package com.spyapptracker.act;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.IntentCompat;

import com.spyapptracker.Utils.Constants;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static java.security.AccessController.getContext;

public class CaptureAct extends AppCompatActivity {


    Context mContext;
    public static  boolean isCapture = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.capturescreen);

        mContext = CaptureAct.this;

        try {
            /*runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
*/
            captureScreen();

        } catch (Exception e) {
            e.printStackTrace();

            Intent intent = new Intent(getApplicationContext(), TermsConditionAct.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);


            ActivityCompat.finishAffinity(this);

            isCapture = true;

        }


    }

    private void captureScreen() {
        View v = getWindow().getDecorView().getRootView();
        v.setDrawingCacheEnabled(true);
        Bitmap bmp = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        try {

            File imagesFolder = new File(Environment.getExternalStorageDirectory(), Constants.FOLDER_SPYGALLERY);
            if (!imagesFolder.exists())
                imagesFolder.mkdirs(); // <----
            File image = new File(imagesFolder, "SpyImg_Screen_Shot"+System.currentTimeMillis()+ ".png");
            FileOutputStream fos = new FileOutputStream(image);
            //FileOutputStream fos = new FileOutputStream(new File(Environment.getExternalStorageDirectory().toString(), "SCREEN" + System.currentTimeMillis() + ".png"));

            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
            Toast.makeText(mContext,"Screen Shot Capture !",Toast.LENGTH_LONG).show();


           /* Intent intent = new Intent(getApplicationContext(), CaptureAct.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();*/

            Intent intent = new Intent(getApplicationContext(), TermsConditionAct.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);


            ActivityCompat.finishAffinity(this);

            isCapture = true;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
