package com.spyapptracker.act;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DeviceAdminDemo extends DeviceAdminReceiver {


    String TAG ="DeviceAdminDemo :-";
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
//        context.stopService(new Intent(context, CallRecordingService.class));
//        Intent myIntent = new Intent(context, CallRecordingService.class);
//        context.startService(myIntent);

        switch (intent.getAction()) {
//            case ACTION_PASSWORD_REQUIREMENTS_CHANGED:
            case Intent.ACTION_BOOT_COMPLETED:
//                updatePasswordConstraintNotification(context);

                Log.d(TAG + "", "ACTION_BOOT_COMPLETED");

                break;
            case DevicePolicyManager.ACTION_PROFILE_OWNER_CHANGED:
//                onProfileOwnerChanged(context);
                Log.d(TAG + "", "ACTION_PROFILE_OWNER_CHANGED");
                break;
            case DevicePolicyManager.ACTION_DEVICE_OWNER_CHANGED:
//                onDeviceOwnerChanged(context);
                Log.d(TAG + "", "ACTION_DEVICE_OWNER_CHANGED");
                break;
            default:
                super.onReceive(context, intent);
                break;
        }

    }

    public void onEnabled(Context context, Intent intent) {
        Log.d(TAG + "", "onEnable :"+intent.getAction());
    };

    public void onDisabled(Context context, Intent intent) {
        Log.d(TAG + "", "onDisabled :"+intent.getAction());
    };

}
