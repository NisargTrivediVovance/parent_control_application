package com.spyapptracker.JobcreatorSheduleService;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobRequest;
import com.spyapptracker.services.ContactWatchService;

public class MySyncJob extends Job {

    public static final String TAG = "my_job_tag";

    Context mContext;


   /* public MySyncJob(Context context){
        mContext = context;
    }*/

    int i=0;

    @Override
    @NonNull
    protected Result onRunJob(Params params) {
        //
        // run your job here
        //
        //

        Log.d(TAG, "Called: " + i);
        i++;


//        Intent intent = new Intent(mContext, ContactWatchService.class);
//        mContext.startService(intent);


//        scheduleJob();

        return Result.SUCCESS;
    }

    public static void scheduleJob() {
        new JobRequest.Builder(MySyncJob.TAG)
                .setExecutionWindow(3_000L, 4_000L) //Every 30 seconds for 40 seconds
                .build()
                .schedule();
    }
}