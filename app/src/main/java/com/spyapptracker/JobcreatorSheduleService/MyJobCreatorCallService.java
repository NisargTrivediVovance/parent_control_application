package com.spyapptracker.JobcreatorSheduleService;

import android.content.Context;

import androidx.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

import org.jetbrains.annotations.Nullable;

public class MyJobCreatorCallService implements JobCreator {


    Context mContext;


   /* public MyJobCreatorCallService(Context context){
        mContext = context;
    }*/


    @Override
    @Nullable
    public Job create(@NonNull String tag) {


        switch (tag) {
            case MySyncJob.TAG:

//                mContext = MyJobCreatorCallService.this.
                return new MySyncJob();
            default:
                return null;
        }
    }
}