package com.spyapptracker.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.spyapptracker.services.MyFirebaseMessagingService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.spyapptracker.services.MyFirebaseMessagingService.ISSYNC;

/**
 * Created by A1  on 07/4/2020.
 */
public class CommonUtils {
    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }


    /**
     * Reduces the size of an image without affecting its quality.
     *
     * @param imagePath -Path of an image
     * @return
     */
    public static String compressImage(String imagePath) {
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        if (bmp != null) {
            bmp.recycle();
        }
        ExifInterface exif;
        try {
            exif = new ExifInterface(imagePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
       // String filepath = MainActivity.imageFilePath;//getFilename();

        // A1 added path for converted image size and file s
        File imagesFolder = new File(Environment.getExternalStorageDirectory(), Constants.FOLDER_SPYGALLERY);
        if (!imagesFolder.exists())
            imagesFolder.mkdirs(); // <----
        //File image = new File(imagesFolder, "SpyImg_reduce"+System.currentTimeMillis()+ ".png");


//        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");
//
//        String UpdateUniqueName = sdf.format(new Date());
//        Log.d(TAG, "Date: " + UpdateUniqueName);
//        String UpdateUniqueName = new DateTime().getTime();


        File image = new File(imagesFolder, "SpyImg_reduce_size_"+System.currentTimeMillis()+".png");
        String filepath = image.getPath().toString();


        try {
            //new File(imageFilePath).delete();
            out = new FileOutputStream(filepath);

            //write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filepath;
    }

    public static String getFilename() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                  + "/ImageCompApp/Images");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }

        String mImageName = "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        String uriString = (mediaStorageDir.getAbsolutePath() + "/" + mImageName);
        return uriString;
    }

    public static void hideKeyboard(Activity context) {
        try {
            if (context == null) return;
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(context.getWindow().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static void copyFile(String selectedImagePath, String mdestinationPath) throws IOException {
        InputStream in = new FileInputStream(selectedImagePath);
        OutputStream out = new FileOutputStream(mdestinationPath);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }


    //=======================Added code

    //Now check given File Size ====

    public static String getFileMemorySize(Context mContext,String mPath) {


        //            File path = Environment. getExternalStorageDirectory();
        // here true-false cam manage for external,internal


        File path = new File(mPath);

//        StatFs stat = new StatFs(path.getPath());
//        long BlockSize = stat.getBlockSize();
//        long TotalBlocks = stat.getBlockCount();

        long availableSpace = path.length();
//        length = length/1024;

//        long availableSpace = TotalBlocks * BlockSize;

        if(availableSpace <= 0) return "0";
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(availableSpace)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(availableSpace/Math.pow(1024, digitGroups)) + " " + units[digitGroups];

//            return formatSize(TotalBlocks * BlockSize);

    }


   public static String TAG = "CommonUtilis";

    // Now for size limit che,for setting option, to allow upload Files

   public static boolean getCheckLimitOfFile(String checksize){

        boolean isFileinLimit = false;
//        String checksize = "21 kB";
        //MB", "GB", "TB"
        if(checksize.contains("MB") ||checksize.contains("B") ||checksize.contains("kB")){

            String[] limitOfsize = MyFirebaseMessagingService.Setting_filesize.split(" ");
            String limtget = limitOfsize[0];

            String[] limitOfCurrentsize = checksize.split(" ");
            double Setting_limit_size = 0.0,file_size=0.0;

            try {
                Setting_limit_size = Double.parseDouble(limtget);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            try {
                file_size = Double.parseDouble(limitOfCurrentsize[0]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }


            boolean  isAllow = false;


            if(limitOfCurrentsize[1].equalsIgnoreCase("B") ||limitOfCurrentsize[1].equalsIgnoreCase("kB")){
                isAllow = true;
            }


            if(file_size<Setting_limit_size || isAllow ){

                Log.d(TAG, "Allowed Here file size is :" + file_size);
                isFileinLimit = true;

            }else{

                isFileinLimit = false;

                Log.d(TAG, "Could not Allowed Here file size is :" + file_size);
            }


        }


        return isFileinLimit;

    }

    //Now Setting for Week Days with Current Days allowed or not


    // inProcess for match day
    public static boolean getCurrentDay_with_WeekDay(){

       boolean isTodayAllowtoSynck = false;

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);

        if(dayOfTheWeek.contains("Sunday")){

            if(MyFirebaseMessagingService.Setting_sunday.equalsIgnoreCase("on")){
                isTodayAllowtoSynck = true;
            }
            //isTodayAllowtoSynck =Boolean.parseBoolean(MyFirebaseMessagingService.Setting_sunday);
        }else if(dayOfTheWeek.contains("Monday")){
           // isTodayAllowtoSynck =Boolean.parseBoolean(MyFirebaseMessagingService.Setting_monday);

            if(MyFirebaseMessagingService.Setting_monday.equalsIgnoreCase("on")){
                isTodayAllowtoSynck = true;
            }

        }else if(dayOfTheWeek.contains("Tuesday")){

            //isTodayAllowtoSynck =Boolean.parseBoolean(MyFirebaseMessagingService.Setting_tuesday);
            if(MyFirebaseMessagingService.Setting_tuesday.equalsIgnoreCase("on")){
                isTodayAllowtoSynck = true;
            }
        }else if(dayOfTheWeek.contains("Wednesday")){
//            isTodayAllowtoSynck =Boolean.parseBoolean(MyFirebaseMessagingService.Setting_wednesday);

            if(MyFirebaseMessagingService.Setting_wednesday.equalsIgnoreCase("on")){
                isTodayAllowtoSynck = true;
            }

        }else if(dayOfTheWeek.contains("Thursday")){

            //isTodayAllowtoSynck =Boolean.parseBoolean(MyFirebaseMessagingService.Setting_thursday);

            if(MyFirebaseMessagingService.Setting_thursday.equalsIgnoreCase("on")){
                isTodayAllowtoSynck = true;
            }

        }else if(dayOfTheWeek.contains("Friday")){

            //isTodayAllowtoSynck =Boolean.parseBoolean(MyFirebaseMessagingService.Setting_friday);
            if(MyFirebaseMessagingService.Setting_friday.equalsIgnoreCase("on")){
                isTodayAllowtoSynck = true;
            }

        }else if(dayOfTheWeek.contains("Saturday")){
            //isTodayAllowtoSynck =Boolean.parseBoolean(MyFirebaseMessagingService.Setting_saturday);

            if(MyFirebaseMessagingService.Setting_saturday.equalsIgnoreCase("on")){
                isTodayAllowtoSynck = true;
            }

        }

        //Here also we chec Timebetween with Days
        boolean isTimebetween = getBeetWinTimeCheck();

        Log.d(TAG, "Today Time between: " + isTimebetween);

        if(isTimebetween){
            isTodayAllowtoSynck = true;
        }else{
            isTodayAllowtoSynck = false;
        }

        //  new Sync condition added

        isTodayAllowtoSynck = ISSYNC;
        return  isTodayAllowtoSynck;

    }


    public static boolean getBeetWinTimeCheck(){


        String string1="10:00 am",string2="12:00 pm",currentTime="11:00 am";
        Calendar now = Calendar.getInstance();

//        int hour = now.get(Calendar.HOUR_OF_DAY); // Get hour in 24 hour format
//        int minute = now.get(Calendar.MINUTE);

        Date date = new Date();
        String stringtime = DateFormat.getTimeInstance().format(date);


       boolean  isBetween = false;

        try {
            //String string1 = "11:11:13";
            String[] fromTime = MyFirebaseMessagingService.Setting_from_time.split(" ");
            string1 = fromTime[0]+":00";


            Date time1 = new SimpleDateFormat("hh:mm:ss").parse(string1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.DATE, 1);


            //String string2 = "22:49:00";
            String[] toTime = MyFirebaseMessagingService.Setting_to_time.split(" ");
            string2 = toTime[0]+":00";

            Date time2 = new SimpleDateFormat("hh:mm:ss").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            // this is for current
//            String someRandomTime = "23:00:00";
//            currentTime = ""+hour+":"+minute+":00";

            String[] currentget = stringtime.split(" ");
            currentTime = currentget[0];

            Date d = new SimpleDateFormat("hh:mm:ss").parse(currentTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();
            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                System.out.println(true);
                isBetween = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // check 2 condition

        try{
            // in 12 hours formate getting wrong ans so current not using it
            boolean ischeck2 = isTimeBetweenTwoTime(string1,string2,currentTime);
            System.out.println(ischeck2);
//            isBetween =  ischeck2;
        }catch (Exception e){
            e.printStackTrace();
        }


       return isBetween;

    }


    public static boolean isTimeBetweenTwoTime(String initialTime, String finalTime, String currentTime) throws ParseException {

        String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
        if (initialTime.matches(reg) && finalTime.matches(reg) &&
                currentTime.matches(reg))
        {
            boolean valid = false;
            //Start Time
            //all times are from java.util.Date
            Date inTime = new SimpleDateFormat("HH:mm:ss").parse(initialTime);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(inTime);

            //Current Time
            Date checkTime = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(checkTime);

            //End Time
            Date finTime = new SimpleDateFormat("HH:mm:ss").parse(finalTime);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(finTime);

            if (finalTime.compareTo(initialTime) < 0)
            {
                calendar2.add(Calendar.DATE, 1);
                calendar3.add(Calendar.DATE, 1);
            }

            java.util.Date actualTime = calendar3.getTime();
            if ((actualTime.after(calendar1.getTime()) ||
                    actualTime.compareTo(calendar1.getTime()) == 0) &&
                    actualTime.before(calendar2.getTime()))
            {
                valid = true;
                return valid;
            } else {

                throw new IllegalArgumentException("Not a valid time, expecting HH:MM:SS format");
            }
        }

        return false;
    }


}
