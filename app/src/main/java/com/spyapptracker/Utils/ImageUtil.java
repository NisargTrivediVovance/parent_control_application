package com.spyapptracker.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class ImageUtil {

    private Context activity;
    private LayoutInflater lf;
    public static final int CAMERA_REQUEST_CODE = 10001;
    public static final int GALLERY_REQUEST_CODE = 10002;

    public ImageUtil(Context activity){
        this.activity=activity;
        lf = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void captureImage(final File file) {
        loadCamera(file);
    }

    public File getImageFile(){
        File file=new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/TimeAttendance/",generateFileName(true));
        return file;
    }
    public void loadCamera(File file) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        if(activity instanceof Activity){
            ((Activity)activity).startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
        }
    }
    private String generateFileName(boolean isImage){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        String dateStr = displayFormat.format(cal.getTime());
        String fileName="";

        if(isImage){
            fileName="IMG_"+dateStr+".jpeg";
        }else{
            fileName="VIDEO_"+dateStr+".MP4";
        }

        return fileName;

    }
    public void loadGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");
        if(activity instanceof Activity){
            ((Activity)activity).startActivityForResult(photoPickerIntent, GALLERY_REQUEST_CODE);
        }

    }
    public static void setImgOrienetation(String path,Activity context) {
        File f = new File(path);
        Bitmap correctBmp=null;
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(f.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);


        int angle = 0;

        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            angle = 90;
        }
        else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
            angle = 180;
        }
        else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
            angle = 270;
        }

        Matrix mat = new Matrix();
        mat.postRotate(angle);
        Bitmap bmp;
        try {
            DisplayMetrics metrics = new DisplayMetrics();
            context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            bmp=decodeScaledBitmapFromSdCard(f.getAbsolutePath(),metrics.widthPixels,metrics.heightPixels);
            correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mat, true);
            FileOutputStream fout=new FileOutputStream(f);
            correctBmp.compress(Bitmap.CompressFormat.JPEG, 100, fout);
        } catch (Exception e) {
            e.printStackTrace() ;
        }finally {
            if(correctBmp!=null){
                correctBmp.recycle();
                correctBmp=null;
                System.gc();
            }
        }
//        return correctBmp;

    }
    public static Bitmap decodeScaledBitmapFromSdCard(String filePath,int reqWidth, int reqHeight){

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public void handleGaleryImage(Intent intent,File destFile){
        try{
            if(intent!=null && intent.getData()!=null){
                String path=getPath(intent.getData());
                File sourceFile=new File(path);
                FileOutputStream fileOutputStream=new FileOutputStream(destFile);
                FileInputStream fileInputStream=new FileInputStream(sourceFile);

                byte buffer[]=new byte[1024*30];
                int count;
                while((count=fileInputStream.read(buffer))!=-1){
                    fileOutputStream.write(buffer,0,count);
                }
                fileOutputStream.flush();
                fileOutputStream.close();
                fileInputStream.close();

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        if(activity instanceof Activity){
            Cursor cursor = ((Activity)activity).managedQuery(uri, projection, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }

        return null;
    }


}
