package com.spyapptracker.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.util.Base64;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.loopj.android.http.RequestParams;
import com.spyapptracker.services.GPSTrackerService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;
import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;
import java.io.ByteArrayInputStream;

public class Utility {

    public static SharedPreferences pref;
    public static String Str_Name_Preference = "SpyApp_Preference";
    public static GPSTrackerService gpsTracker = null;

    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    public static int mkFolder(Activity context, String folderName) { // make a folder under Environment.DIRECTORY_DCIM
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            Log.d("myAppName", "Error: external storage is unavailable");
            return 0;
        }
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            Log.d("myAppName", "Error: external storage is read only.");
            return 0;
        }
        Log.d("myAppName", "External storage is not read only or unavailable");

        if (ContextCompat.checkSelfPermission(context, // request permission when it is not granted.
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d("myAppName", "permission:WRITE_EXTERNAL_STORAGE: NOT granted!");
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // context thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        //File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),folderName);

//        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.getExternalStorageState().toString()),folderName);

        File folder = new File(Environment.getExternalStorageDirectory() + "/" + folderName);
        //Environment.getExternalStorageState()
        int result = 0;
        if (folder.exists()) {
            Log.d("myAppName", "folder exist:" + folder.toString());
            result = 2; // folder exist
        } else {
            try {
                if (folder.mkdir()) {
                    Log.d("myAppName", "folder created:" + folder.toString());
                    result = 1; // folder created
                } else {
                    Log.d("myAppName", "creat folder fails:" + folder.toString());
                    result = 0; // creat folder fails
                }
            } catch (Exception ecp) {
                ecp.printStackTrace();
            }
        }
        return result;
    }


    public static boolean getDeviceWifiEnableDisable(Context mContext) {
        ConnectivityManager connManager =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo current = connManager.getActiveNetworkInfo();
        boolean isWifi = current != null && current.getType() == ConnectivityManager.TYPE_WIFI;
        return isWifi;
    }


    public static void SharedPreferencesWriteData(Context mContext, String PrefKey, String PrefValue) {

        pref = mContext.getApplicationContext().getSharedPreferences(Str_Name_Preference, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(PrefKey, PrefValue);
        editor.commit(); // commit changes

    }

    public static String SharedPreferencesGetData(Context mContext, String PrefKey) {
        String Pref_Result;
        pref = mContext.getApplicationContext().getSharedPreferences(Str_Name_Preference, MODE_PRIVATE);
        Pref_Result = pref.getString(PrefKey, null);
        return Pref_Result;
    }


    public static boolean getDeviceLocation(Context mContext) {

        boolean isEnableGps = false;

        try {


            // check if GPS enabled
            // here as we enable next condition so currently we not checking null here.
            // even its work for all time get new lat& long details
            if (gpsTracker == null) {
                gpsTracker = new GPSTrackerService(mContext);
                if (gpsTracker.getIsGPSTrackingEnabled()) {
                    String STR_CURRENT_LAT = String.valueOf(gpsTracker.latitude);
                    String STR_CURRENT_LNG = String.valueOf(gpsTracker.longitude);
//                    Log.d(TAG + "", "STR_CURRENT_LAT :" + STR_CURRENT_LAT);
//                    Log.d(TAG + "", "Neede STR_CURRENT_LNG :" + STR_CURRENT_LNG);

                    isEnableGps = true;
                } else {

                    isEnableGps = false;
                    // can't get location
                    // GPS or Network is not enabled
                    // Ask user to enable GPS/network in settings
                    gpsTracker.showSettingsAlert();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isEnableGps;
    }


    public static String getCommonCurrentDateTimeFormate() {
        String Str_Current_Date_Time = "";
        Str_Current_Date_Time = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
        return Str_Current_Date_Time;
    }


    static boolean  isSamepass = false;
    public static  boolean  IsPasswordSame(String password,String conf_password){
          if(password.equalsIgnoreCase(conf_password)){
              isSamepass = true;
          }else{
              isSamepass = false;
          }

        return  isSamepass;
    }


    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }








    //16-03-2020  Updated method for get Available Free Space.

    static long sizecheckinmB = 1048576;
    static long sizecheckinGb = 1073741824;
    static long availableSize = 0;
    static long sizecheckinTB = 1099511627776L;

    static String returnSize;

    public static String getAvailableSpaceInGB(){

        final long SIZE_KB = 1024L;

        final long SIZE_MB = SIZE_KB * SIZE_KB;
        final long SIZE_GB = SIZE_KB * SIZE_KB * SIZE_KB;
        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();

        if(availableSpace<SIZE_KB){
            availableSize  =   availableSpace/SIZE_KB ;
            returnSize = availableSize     +" KB";
        }
        else if(availableSpace>sizecheckinmB && availableSpace<sizecheckinGb){
            availableSize  =   availableSpace/SIZE_MB ;
            returnSize = availableSize     +" MB";
        }else  if(availableSpace>sizecheckinGb && availableSpace<sizecheckinTB){
            availableSize  =   availableSpace/SIZE_GB;
            returnSize = availableSize +" GB";
        }else if(availableSpace >sizecheckinTB){
            availableSize  =   availableSpace/sizecheckinTB;
            returnSize = availableSize +" TB";
        }

        //return availableSpace/SIZE_GB;
        return returnSize;
    }

    // or you can use this funcation to check it
    public static String readableAvailableFileSize() {
        long availableSpace = -1L;
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2)
            availableSpace = (long) stat.getBlockSizeLong() * (long) stat.getAvailableBlocksLong();
        else
            availableSpace = (long) stat.getAvailableBlocks() * (long) stat.getBlockSize();

        if(availableSpace <= 0) return "0";
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(availableSpace)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(availableSpace/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }






    // For Total Size check
    public static String getTotalInternalMemorySize() {

        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long BlockSize = stat.getBlockSize();
        long TotalBlocks = stat.getBlockCount();

        long availableSpace = TotalBlocks * BlockSize;

        if(availableSpace <= 0) return "0";
        final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(availableSpace)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(availableSpace/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }


    //=================Now for check TotalSize of Internal and External memory of Sdcard

    public static String getTotalExternalMemorySize(Context mContext,boolean isInternalorExtranal) {
        if (externalMemoryAvailable()) {
//            File path = Environment. getExternalStorageDirectory();
            // here true-false cam manage for external,internal

            String mpath = getExternalStoragePath(mContext, isInternalorExtranal);

            File path = new File(mpath);

            StatFs stat = new StatFs(path.getPath());
            long BlockSize = stat.getBlockSize();
            long TotalBlocks = stat.getBlockCount();

            long availableSpace = TotalBlocks * BlockSize;

            if(availableSpace <= 0) return "0";
            final String[] units = new String[] { "B", "kB", "MB", "GB", "TB" };
            int digitGroups = (int) (Math.log10(availableSpace)/Math.log10(1024));
            return new DecimalFormat("#,##0.#").format(availableSpace/Math.pow(1024, digitGroups)) + " " + units[digitGroups];

//            return formatSize(TotalBlocks * BlockSize);
        } else {
            return "0.0";
        }
    }


    public static boolean externalMemoryAvailable() {
        return android.os.Environment.
                getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }


    private static String getExternalStoragePath(Context mContext, boolean is_removable) {

        StorageManager mStorageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClazz = null;
        try {
            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");
            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");
            Method getPath = storageVolumeClazz.getMethod("getPath");
            Method isRemovable = storageVolumeClazz.getMethod("isRemovable");
            Object result = getVolumeList.invoke(mStorageManager);
            final int length = Array.getLength(result);
            for (int i = 0; i < length; i++) {
                Object storageVolumeElement = Array.get(result, i);
                String path = (String) getPath.invoke(storageVolumeElement);
                boolean removable = (Boolean) isRemovable.invoke(storageVolumeElement);
                if (is_removable == removable) {
                    return path;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }



    public static String getUrlWithQueryString(boolean shouldEncodeUrl, String url, RequestParams params) {
        if (url == null)
            return null;

        if (shouldEncodeUrl) {
            try {
                String decodedURL = URLDecoder.decode(url, "UTF-8");
                URL _url = new URL(decodedURL);
                URI _uri = new URI(_url.getProtocol(), _url.getUserInfo(), _url.getHost(), _url.getPort(), _url.getPath(), _url.getQuery(), _url.getRef());
                url = _uri.toASCIIString();
            } catch (Exception ex) {
                // Should not really happen, added just for sake of validity
                Log.e(LOG_TAG, "getUrlWithQueryString encoding URL", ex);
            }
        }

        if (params != null) {
            // Construct the query string and trim it, in case it
            // includes any excessive white spaces.
            String paramString = params.toString().trim();

            // Only add the query string if it isn't empty and it
            // isn't equal to '?'.
            if (!paramString.equals("") && !paramString.equals("?")) {
                url += url.contains("?") ? "&" : "?";
                url += paramString;
            }
        }

        return url;
    }


  /*  public static String  ConvertBitmap(Context  mContex, Bitmap  bitmap){

        String baseg64 = "";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return baseg64;
    }
*/


    private String getBase64String(Bitmap bitmap)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);


//        byte[] decodedByteArray = Base64.decode(base64String, Base64.NO_WRAP);
//        Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedByteArray, 0, decodedString.length);

        return base64String;
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }


}
