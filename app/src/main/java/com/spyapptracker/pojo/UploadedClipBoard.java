
package com.spyapptracker.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UploadedClipBoard {

    @SerializedName("clipboardtext")
    @Expose
    private String clipboardtext;
    @SerializedName("datetime")
    @Expose
    private String datetime;

    public String getClipboardtext() {
        return clipboardtext;
    }

    public void setClipboardtext(String clipboardtext) {
        this.clipboardtext = clipboardtext;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

}
