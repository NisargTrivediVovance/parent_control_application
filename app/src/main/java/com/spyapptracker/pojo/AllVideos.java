package com.spyapptracker.pojo;

public class AllVideos {
    String id;
    String StrVideofullpath;
    String StrVideoDatetime;
    String StrVideoisonline;


    public AllVideos(String mid,String mStrVideofullpath,String mStrVideoDatetime,String mStrVideoisonline){

        this.id = mid;
        this.StrVideofullpath = mStrVideofullpath;
        this.StrVideoDatetime = mStrVideoDatetime;
        this.StrVideoisonline = mStrVideoisonline;
    }


    public String getStrVideofullpath() {
        return StrVideofullpath;
    }

    public void setStrVideofullpath(String strVideofullpath) {
        StrVideofullpath = strVideofullpath;
    }

    public String getStrVideoisonline() {
        return StrVideoisonline;
    }

    public void setStrVideoisonline(String strVideoisonline) {
        StrVideoisonline = strVideoisonline;
    }


    public String getStrVideoDatetime() {
        return StrVideoDatetime;
    }

    public void setStrVideoDatetime(String strVideoDatetime) {
        StrVideoDatetime = strVideoDatetime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }




}

