package com.spyapptracker.pojo;

public class AllImages {

    String id;
    String StrImagefullpath;
    String StrDatetime;
    String Strisonline;

    public AllImages(String mid,String mStrImagefullpath,String mStrDatetime,String mStrisonline){

        this.id = mid;
        this.StrImagefullpath = mStrImagefullpath;
        this.StrDatetime = mStrDatetime;
        this.Strisonline = mStrisonline;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrImagefullpath() {
        return StrImagefullpath;
    }

    public void setStrImagefullpath(String strImagefullpath) {
        StrImagefullpath = strImagefullpath;
    }

    public String getStrDatetime() {
        return StrDatetime;
    }

    public void setStrDatetime(String strDatetime) {
        StrDatetime = strDatetime;
    }

    public String getStrisonline() {
        return Strisonline;
    }

    public void setStrisonline(String strisonline) {
        Strisonline = strisonline;
    }



}
