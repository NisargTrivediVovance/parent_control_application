package com.spyapptracker.pojo;

public class ClipBoardTextPojo {

    public String getStr_Clipboard_Text() {
        return Str_Clipboard_Text;
    }

    public void setStr_Clipboard_Text(String str_Clipboard_Text) {
        Str_Clipboard_Text = str_Clipboard_Text;
    }

    public String getStr_Date_Time() {
        return Str_Date_Time;
    }

    public void setStr_Date_Time(String str_Date_Time) {
        Str_Date_Time = str_Date_Time;
    }

    String Str_Clipboard_Text;
    String Str_Date_Time;

    public String getStr_Is_online() {
        return Str_Is_online;
    }

    public void setStr_Is_online(String str_Is_online) {
        Str_Is_online = str_Is_online;
    }

    String Str_Is_online;


    public String getStr_ID() {
        return Str_ID;
    }

    public void setStr_ID(String str_ID) {
        Str_ID = str_ID;
    }

    String Str_ID;

    public ClipBoardTextPojo(String mID,String mStr_Clipboard_Text,String mStr_Date_Time,String mStr_Is_Online){

        this.Str_ID = mID;
        this.Str_Clipboard_Text = mStr_Clipboard_Text;
        this.Str_Date_Time = mStr_Date_Time;
        this.Str_Is_online = mStr_Is_Online;
    }

}
