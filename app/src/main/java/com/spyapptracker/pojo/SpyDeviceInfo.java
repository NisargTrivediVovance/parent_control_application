
package com.spyapptracker.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpyDeviceInfo {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("device_id")
    @Expose
    private Integer deviceId;
    @SerializedName("device_imei")
    @Expose
    private String deviceImei;
    @SerializedName("device_operator")
    @Expose
    private String deviceOperator;
    @SerializedName("device_os")
    @Expose
    private String deviceOs;
    @SerializedName("code")
    @Expose
    private Integer code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    public String getDeviceOperator() {
        return deviceOperator;
    }

    public void setDeviceOperator(String deviceOperator) {
        this.deviceOperator = deviceOperator;
    }

    public String getDeviceOs() {
        return deviceOs;
    }

    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

}
