package com.spyapptracker.services;

import android.app.ActivityManager;
import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.spyapptracker.Utils.NotificationCreator;

import java.io.File;

public class DeviceLockUnlockNotificationService  extends Service {

    String TAG = "DeviceLockUnlockNotificationService-";
    Context mContext;


    DevicePolicyManager deviceManger;
    ActivityManager activityManager;
    ComponentName compName;



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Do whatever you want to do here

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
//            createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        return START_STICKY;
    }




    public void doProcess(){

        deviceManger = (DevicePolicyManager)getSystemService(
                Context.DEVICE_POLICY_SERVICE);
        activityManager = (ActivityManager)getSystemService(
                Context.ACTIVITY_SERVICE);
        compName = new ComponentName(this, MyAdmin.class);


    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

}
