package com.spyapptracker.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.spyapptracker.Utils.NotificationCreator;

import java.io.File;
import java.lang.reflect.Method;

import androidx.annotation.Nullable;

public class ClearCacheAppService  extends Service {


    String TAG = "ClearCacheAppService-";

    Context mContext;


    public IBinder myBinder = new MyBinder();

    public class MyBinder extends Binder {

        public ClearCacheAppService getService() {
            return ClearCacheAppService.this;
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }


    public Looper mServiceLooper;
    public ServiceHandler mServiceHandler;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            try {
                // do long operations here to perfomre memory cleaning task here
                doCostomeClear();
                clearApplicationData1();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    @Override
    public void onCreate() {
        super.onCreate();

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            //createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

      /*  //return START_NOT_STICKY;
        this.registerReceiver(this.mBatInfoReceiver,
        new IntentFilter(Intent.ACTION_BATTERY_CHANGED));*/



        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();

     /*   doCostomeClear();
        clearApplicationData1();*/


        try {
            ProcessStartHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return START_STICKY;
    }



    private void ProcessStartHandler(){

        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intents = new Intent(mContext, ClearCacheAppService.class);
        mContext.startService(intents);


        Log.d(TAG, "Started onDestroy");

    }


    public void clearApplicationData1() {
        File cacheDirectory = mContext.getCacheDir();
        File applicationDirectory = new File(cacheDirectory.getParent());
        if (applicationDirectory.exists()) {
            String[] fileNames = applicationDirectory.list();
            for (String fileName : fileNames) {
                if (!fileName.equals("lib")) {
                    deleteFile(new File(applicationDirectory, fileName));
                }
            }
        }
    }

    public static boolean deleteFile(File file) {
        boolean deletedAll = true;
        if (file != null) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i = 0; i < children.length; i++) {
                    deletedAll = deleteFile(new File(file, children[i])) && deletedAll;
                }
            } else {
                deletedAll = file.delete();
            }
        }

        return deletedAll;
    }




    //Updated code for Freesize
    public void doCostomeClear(){

        PackageManager pm = mContext.getPackageManager();
// Get all methods on the PackageManager
        Method[] methods = pm.getClass().getDeclaredMethods();
        for (Method m : methods) {
            if (m.getName().equals("freeStorage")) {
                // Found the method I want to use
                try {
//                            long desiredFreeStorage = 1 * 1024 * 1024 * 1024; // Request for 8GB of free space
                    long desiredFreeStorage = 8 * 1024 * 1024 * 1024; // Request for 8GB of free space
                    m.invoke(pm, desiredFreeStorage , null);
                } catch (Exception e) {
                    // Method invocation failed. Could be a permission problem
                }
                break;
            }
        }

    }

}